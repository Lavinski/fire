﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Fire.Collections;

namespace Ice.Collections
{
	[TestClass]
	public class CollectionsTest
	{
		[TestMethod]
		public void MergeTest()
		{
			var master = new List<int>() { 1, 2, 3, 4 };
			var existing = new List<int>() { 2, 3, 4, 5 };

			Combinators.Merge( master, existing, x => existing.Add(x), x => existing.Remove(x));

			/* Output does not match sort order
			 * for (int listIndex = 0; listIndex < master.Count; listIndex++) {
				Assert.AreEqual( master[listIndex], existing[listIndex] );
			}*/

			foreach (var item in master) {
				Assert.IsTrue(  existing.Contains( item ) );
			}
		}
	}
}
