﻿using Fire.Database.SQL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ice.Database.SQL
{
	
	/// <summary>
	/// This is a test class for QueryTest and is intended
	/// to contain all QueryTest Unit Tests
	/// </summary>
	[TestClass()]
	public class QueryTest
	{

		/// <summary>
		/// Gets or sets the test context which provides
		/// information about and functionality for the current test run.
		/// </summary>
		public TestContext TestContext { get; set; }

		/// <summary>
		/// A join condition must reference the new table if it is fully specified
		/// </summary>
		[TestMethod()]
		public void JoinAssertTest()
		{
			bool exceptionThrown = false;
			try {
				new Join() {
					Table = new Table( "1" ),
					LeftField = new Field( new Table( "2" ), "F1" ),
					RightField = new Field( new Table( "3" ), "F2" )
				}.Assert();
			} catch (Exception) {
				exceptionThrown = true;
			}
			Assert.IsTrue( exceptionThrown );
		}

		/// <summary>
		/// SQL Generation Test using all the options
		/// </summary>
		[TestMethod()]
		public void SQLGenTest()
		{

			var query = Query.From( "tableA" );

			query.Join( new Table( "tableB", "B" ), new Field( "FieldA" ), new Field( "FieldB" ) );
			query.Where( Predicate.EqualTo( new Field( "Field1" ), new Field( new Table("B"), "Field2" ) ) );
			query.OrderBy( new Field( "FieldA" ).Order() );
			query.GroupBy( new Field( "FieldA" ) );
			query.Select( new Table( "tableB" ) );

			var result = query.ToString();

			Assert.AreEqual(
@"Select tableB.*
From tableA
Left Inner Join tableB B On FieldA = FieldB
Where (Field1 = B.Field2)
Order By FieldA Asc
Group By FieldA
", result );

		}
	}
}
