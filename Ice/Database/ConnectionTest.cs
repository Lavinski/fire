﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Fire.Database;

namespace Ice.Database
{
	[TestClass]
	public class ConnectionTest
	{
		[TestMethod]
		public void BuildStringTest()
		{
			var connection = new Connection();
			connection.Location = "C:\\database.file";
			connection.Password = "Password";
			connection.Username = "Username";
			connection["param1"] = "Value1";

			Assert.AreEqual( "database=C:\\database.file;password=Password;user=Username;param1=Value1;", connection.Value );
		}

		[TestMethod]
		public void ParseStringTest()
		{
			var connection = new Connection();
			connection.Value = "database=C:\\database.file;password=Password;user=Username;paRam1=Value1;";

			Assert.AreEqual( "C:\\database.file", connection.Location  );
			Assert.AreEqual( "Password",          connection.Password  );
			Assert.AreEqual( "Username",          connection.Username  );
			Assert.AreEqual( "Value1",            connection["param1"] ); // Note: keys are always lower case
		}
	}
}
