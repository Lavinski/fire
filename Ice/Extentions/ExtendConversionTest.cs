﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Fire.Extensions;

namespace Ice.Extensions
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class ExtendConversionTests
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		[TestMethod]
		public void ToBool()
		{
			string[] fail = new[] { "", "-", "1234", "asdf", "FGJH", "^%$@", "T", "F", "0", "1", "Y", "N" };
			string[] succ = new[] { "true", "false", "True", "False" };

			foreach (var item in fail) {
				Assert.AreEqual( default(bool), item.ToBool() );
			}

			foreach (var item in succ) {
				Assert.AreEqual( bool.Parse(item), item.ToBool() );
			}
		}

		[TestMethod]
		public void ToInt()
		{
			string[] fail = new[] { "", "-", "	", "asdf", "FGJH", "^%$@", "T", "F", "0.0", "1.0", "Y", "N" };
			string[] succ = new[] { "0","1", "2", "3", "4", "5", "6", "7", "8", "9" };

			foreach (var item in fail) {
				Assert.AreEqual( default(int), item.ToInt() );
			}

			foreach (var item in succ) {
				Assert.AreEqual( int.Parse( item ), item.ToInt() );
			}
		}

		[TestMethod]
		public void ToDateTime()
		{
			string[] fail = new[] { "", "-", "	", "asdf", "FGJH", "^%$@", "T", "F", "0.0", "1.0", "Y", "N", "0/0/0000", "99/99/9999" };
			string[] succ = new[] { "01/1/0001", "30/12/9999" };

			foreach (var item in fail) {
				Assert.AreEqual( default( DateTime ), item.ToDateTime() );
			}

			foreach (var item in succ) {
				Assert.AreEqual( DateTime.Parse( item ), item.ToDateTime() );
			}
		}

		[TestMethod]
		public void ToEnum()
		{
			string[] fail = new[] { "", "-", "	", "asdf", "FGJH", "^%$@", "T", "F", "0.0", "1.0", "Y", "N", "0/0/0000", "99/99/9999" };
			string[] succ = new[] { "Authority", "Path", "Query", "Scheme" };

			foreach (var item in fail) {
				Assert.AreEqual( null, item.ToEnum<UriPartial>() );
			}

			foreach (var item in succ) {
				Assert.AreEqual( (UriPartial)Enum.Parse(typeof(UriPartial), item), item.ToEnum<UriPartial>() );
			}
		}

		[TestMethod]
		public void ConvertTo()
		{
			string[] fail = new[] { "", "-", "	", "asdf", "FGJH", "^%$@", "T", "F", "0.0", "1.0", "Y", "N", "0/0/0000", "99/99/9999" };
			string[] succ = new[] { "01/1/0001", "30/12/9999" };

			foreach (var item in fail) {
				Assert.AreEqual( default( DateTime ), item.ConvertTo<DateTime>() );
			}

			foreach (var item in succ) {
				Assert.AreEqual( DateTime.Parse( item ), item.ConvertTo<DateTime>() );
			}
		}

	}
}
