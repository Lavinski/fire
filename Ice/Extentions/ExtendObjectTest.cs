﻿using System;
using Fire.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ice.Extensions
{
    
    
    /// <summary>
    ///This is a test class for ExtendObjectTest and is intended
    ///to contain all ExtendObjectTest Unit Tests
    ///</summary>
	[TestClass()]
	public class ExtendObjectTest
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }


		/// <summary>
		///A test for FromCamelCase
		///</summary>
		[TestMethod()]
		public void FromCamelCaseTest()
		{
			object value = "SomeValue 12/$%ABCD"; // TODO: Initialize to an appropriate value
			string expected = "Some Value 12/$%ABCD"; // TODO: Initialize to an appropriate value
			string actual;

			actual = ExtendObject.FromCamelCase( value );
			Assert.AreEqual( expected, actual );
		}


		public class Classy
		{
			public Int64 BigNumber { get; set; }
		}

		[TestMethod()]
		public void PropertyNameTest()
		{
			var instance = new Classy();

			Assert.AreEqual( instance.GetPropertyName( x => x.BigNumber ), "BigNumber");
			Assert.AreEqual( instance.GetPropertyDetail( x => x.BigNumber ).Type , typeof(Int64) );
		}
	}
}
