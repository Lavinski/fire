﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Fire.Extensions.Mvc;

namespace Ice.Extensions.Mvc
{


	/// <summary>
	///This is a test class for ExtendStringTest and is intended
	///to contain all ExtendStringTest Unit Tests
	///</summary>
	[TestClass()]
	public class ExtendStringTest
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		/// <summary>
		///A test for FirstQuery
		///</summary>
		[TestMethod()]
		public void FirstQueryTest()
		{
			string target = "abcd!@#,abcd!@#";
			string expected = "abcd!@#"; 
			string actual;

			actual = ExtendString.FirstQuery( target );
			Assert.AreEqual( expected, actual );

			actual = ExtendString.FirstQuery( string.Empty );
			Assert.AreEqual( string.Empty, actual );
		}


		/// <summary>
		///A test for FirstQuery
		///</summary>
		[TestMethod()]
		public void RemoveCharsTest()
		{
			string target = "''''''''\"\"\"\"\"\"\"\"\"\"<This is a removal test>";
			string expected = "<Thisisaremovaltest>";
			string actual;

			actual = ExtendString.RemoveCharacters( target, '\'', '"', ' ' );
			Assert.AreEqual( expected, actual );


			Assert.AreEqual( ExtendString.RemoveCharacters( "\"", '\'', '"' ), "" );

		}
	}
}