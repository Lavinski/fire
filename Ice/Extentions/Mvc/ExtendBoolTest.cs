﻿using Fire.Extensions.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ice.Extensions
{
    
    
    /// <summary>
    ///This is a test class for ExtendBoolTest and is intended
    ///to contain all ExtendBoolTest Unit Tests
    ///</summary>
	[TestClass()]
	public class ExtendBoolTest
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }


		/// <summary>
		///A test for AsAttribute
		///</summary>
		[TestMethod()]
		public void AsAttributeTest()
		{
			bool disabled = false;
			object attribs = new { @class="class" };
			IDictionary<string, object> expected = new Dictionary<string, object>();
			IDictionary<string, object> actual;

			expected["class"] = "class";
			actual = ExtendBool.AsAttribute( disabled, attribs );

			foreach (var key in expected.Keys) {
				Assert.AreEqual( expected[key], actual[key] );
			}


			disabled = true;
			expected["disabled"] = disabled;
			actual = ExtendBool.AsAttribute( disabled, attribs );

			foreach (var key in expected.Keys) {
				Assert.AreEqual( expected[key], actual[key] );
			}
			
		}

		/// <summary>
		///A test for AsAttribute
		///</summary>
		[TestMethod()]
		public void AsAttributeTest1()
		{
			bool disabled = false; 
			string cssClass = "class"; 
			IDictionary<string, object> expected = new Dictionary<string, object>();
			IDictionary<string, object> actual;
			
			expected["class"] = "class";
			actual = ExtendBool.AsAttribute( false, ExtendBool.AsAttribute( disabled, cssClass ) );

			foreach (var key in expected.Keys) {
				Assert.AreEqual( expected[key], actual[key] );
			}


			disabled = true;
			expected["disabled"] = disabled;
			actual = ExtendBool.AsAttribute( false, ExtendBool.AsAttribute( disabled, cssClass ) );

			foreach (var key in expected.Keys) {
				Assert.AreEqual( expected[key], actual[key] );
			}
		}

		/// <summary>
		///A test for AsAttribute
		///</summary>
		[TestMethod()]
		public void AsAttributeTest2()
		{
			bool disabled = false;
			IDictionary<string, object> expected = new Dictionary<string, object>();
			IDictionary<string, object> actual;

			actual = ExtendBool.AsAttribute( false, ExtendBool.AsAttribute( disabled ) );

			foreach (var key in expected.Keys) {
				Assert.AreEqual( expected[key], actual[key] );
			}

			disabled = true;
			expected["disabled"] = true;
			actual = ExtendBool.AsAttribute( false, ExtendBool.AsAttribute( disabled ) );

			foreach (var key in expected.Keys) {
				Assert.AreEqual( expected[key], actual[key] );
			}

		}
	}
}
