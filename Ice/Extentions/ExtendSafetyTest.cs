﻿using Fire.Extensions.Safety;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ice.Extensions
{
    
    
    /// <summary>
    ///This is a test class for ExtendSafetyTest and is intended
    ///to contain all ExtendSafetyTest Unit Tests
    ///</summary>
	[TestClass()]
	public class ExtendSafetyTest
	{

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }


		class TypeA : TypeB { }
		class TypeB : TypeC { }
		class TypeC  { }


		[TestMethod()]
		public void AsTest()
		{
			Func<TypeA, string> funcT = x => "A";
			Func<TypeB, string> funcY = x => "B";
			string actual;

			TypeC typeC = new TypeA();

			actual = ExtendSafety.As<TypeA, TypeB>( typeC, funcT, funcY );
			Assert.AreEqual( "A", actual );

			typeC = new TypeB();

			actual = ExtendSafety.As<TypeA, TypeB>( typeC, funcT, funcY );
			Assert.AreEqual( "B", actual );
		}

		[TestMethod()]
		public void AsTest1()
		{
			Func<TypeA, string> funcT = x => "A";
			string defaultVal = "B";
			string actual;

			TypeC typeC = new TypeA();

			actual = ExtendSafety.As<TypeA, string>( typeC, funcT, defaultVal );
			Assert.AreEqual( "A", actual );

			typeC = new TypeB();

			actual = ExtendSafety.As<TypeA, string>( typeC, funcT, defaultVal );
			Assert.AreEqual( "B", actual );
		}

		[TestMethod()]
		public void AsTest2()
		{
			Func<TypeA, string> funcT = x => string.Empty;
			string actual;

			TypeC typeC = new TypeA();

			actual = ExtendSafety.As<TypeA, string>( typeC, funcT );
			Assert.AreEqual( string.Empty, actual );

			typeC = new TypeB();

			actual = ExtendSafety.As<TypeA, string>( typeC, funcT );
			Assert.AreEqual( null, actual );
		}


		[TestMethod()]
		public void IsSafeTest()
		{
			Nullable<bool> target = new Nullable<bool>(); 
			string actual;
			
			actual = ExtendSafety.IsSafe<bool, string>( target, x => "override", "default" );
			Assert.AreEqual( "default", actual );

			target = new Nullable<bool>( false); 

			actual = ExtendSafety.IsSafe<bool, string>( target, x => "override", "default" );
			Assert.AreEqual( "override", actual );

			target = new Nullable<bool>( true ); 

			actual = ExtendSafety.IsSafe<bool, string>( target, x => "override", "default" );
			Assert.AreEqual( "override", actual );

		}

		[TestMethod()]
		public void IsSafeTest1()
		{
			Nullable<bool> target = new Nullable<bool>();
			string actual;

			actual = ExtendSafety.IsSafe<bool, string>( target, x => "override" );
			Assert.AreEqual( null, actual );

			target = new Nullable<bool>( false );

			actual = ExtendSafety.IsSafe<bool, string>( target, x => "override" );
			Assert.AreEqual( "override", actual );

			target = new Nullable<bool>( true );

			actual = ExtendSafety.IsSafe<bool, string>( target, x => "override" );
			Assert.AreEqual( "override", actual );
		}

		[TestMethod()]
		public void IsSafeTest2()
		{
			Nullable<bool> target = new Nullable<bool>();
			string actual;

			actual = ExtendSafety.IsSafe<bool, string>( target, x => "override", () => "default" );
			Assert.AreEqual( "default", actual );

			target = new Nullable<bool>( false );

			actual = ExtendSafety.IsSafe<bool, string>( target, x => "override", () => "default" );
			Assert.AreEqual( "override", actual );

			target = new Nullable<bool>( true );

			actual = ExtendSafety.IsSafe<bool, string>( target, x => "override", () => "default" );
			Assert.AreEqual( "override", actual );
		}

		[TestMethod()]
		public void IsSafeTest3()
		{
			ExtendSafety.IsSafe<bool>( new Nullable<bool>(), x => Assert.Fail() );
			ExtendSafety.IsSafe<bool>( new Nullable<bool>( false ), x => { } );
			ExtendSafety.IsSafe<bool>( new Nullable<bool>( true ), x => { } );
		}

		[TestMethod()]
		public void IsSafeTest4()
		{
			ExtendSafety.IsSafe<bool>( new Nullable<bool>(), x => Assert.Fail(), () => { } );
			ExtendSafety.IsSafe<bool>( new Nullable<bool>( false ), x => { }, () => Assert.Fail() );
			ExtendSafety.IsSafe<bool>( new Nullable<bool>( true ), x => { }, () => Assert.Fail() );
		}

		[TestMethod()]
		public void IsSafeTest5()
		{
			Nullable<bool> target = new Nullable<bool>(); 
			bool expected = false; 
			bool actual;
			
			actual = ExtendSafety.IsSafe<bool>( target );
			Assert.AreEqual( expected, actual );


			target = new Nullable<bool>( false );
			expected = true;

			actual = ExtendSafety.IsSafe<bool>( target );
			Assert.AreEqual( expected, actual );


			target = new Nullable<bool>( true );
			expected = true;

			actual = ExtendSafety.IsSafe<bool>( target );
			Assert.AreEqual( expected, actual );
		}


		[TestMethod()]
		public void NotNullTest()
		{
			object target = null;
			bool expected = false;
			bool actual;

			actual = ExtendSafety.NotNull<object>( target );
			Assert.AreEqual( expected, actual );

			target = new object();
			expected = true;

			actual = ExtendSafety.NotNull<object>( target );
			Assert.AreEqual( expected, actual );
		}

		[TestMethod()]
		public void NotNullTest1()
		{
			object target = null;
			Func<object, bool> func = (x => true);
			Func<bool> elseFunc = (() => false);
			bool expected = false;
			bool actual;

			actual = ExtendSafety.NotNull<object, bool>( target, func, elseFunc );
			Assert.AreEqual( expected, actual );

			target = new object();
			expected = true;

			actual = ExtendSafety.NotNull<object, bool>( target, func, elseFunc );
			Assert.AreEqual( expected, actual );
		}

		[TestMethod()]
		public void NotNullTest2()
		{
			bool wasRun = false;
			object target = null;
			Action<object> action = (x => wasRun = true); 
			
			ExtendSafety.NotNull<object>( target, action );
			Assert.AreEqual( false, wasRun );


			target = new object();
			
			ExtendSafety.NotNull<object>( target, action );
			Assert.AreEqual( true, wasRun );
		}

		[TestMethod()]
		public void NotNullTest3()
		{
			string actual;
			object target = null;
			Func<object, string> action = (x => string.Empty );

			actual = ExtendSafety.NotNull<object, string>( target, action, "default" );
			Assert.AreEqual( "default", actual );


			target = new object();

			actual = ExtendSafety.NotNull<object, string>( target, action, "default" );
			Assert.AreEqual( string.Empty, actual );
		}

		[TestMethod()]
		public void NotNullTest4()
		{
			string actual;
			object target = null;
			Func<object, string> action = (x => string.Empty);

			actual = ExtendSafety.NotNull<object, string>( target, action );
			Assert.AreEqual( null, actual );


			target = new object();

			actual = ExtendSafety.NotNull<object, string>( target, action );
			Assert.AreEqual( string.Empty, actual );
		}

		/// <summary>
		///A test for NotZero
		///</summary>
		[TestMethod()]
		public void NotZeroTest()
		{
			var done = true;
			string target = string.Empty;
			Action<string> action = (x => done = true);
			Action elseAction = (() => done = false);

			ExtendSafety.NotZero( target, action, elseAction );
			Assert.AreEqual( false, done );

			target = "1";

			ExtendSafety.NotZero( target, action, elseAction );
			Assert.AreEqual( true, done );
		}

		/// <summary>
		///A test for NotZero
		///</summary>
		[TestMethod()]
		public void NotZeroTest1()
		{
			var done = false;
			string target = string.Empty;
			Action<string> work = (x => done = true); 
			
			ExtendSafety.NotZero( target, work );
			Assert.AreEqual( false, done );

			target = "1";

			ExtendSafety.NotZero( target, work );
			Assert.AreEqual( true, done );
		}

		/// <summary>
		///A test for NotZero
		///</summary>
		[TestMethod()]
		public void NotZeroTest2()
		{
			bool actual;

			actual = ExtendSafety.NotZero( string.Empty );
			Assert.AreEqual( false, actual );

			actual = ExtendSafety.NotZero( "1" );
			Assert.AreEqual( true, actual );

			// Bug found
		}


		[TestMethod()]
		public void NotZeroTest3()
		{
			Func<string, string> action = (x => "override");
			string elseVal = "default";
			string actual;

			actual = ExtendSafety.NotZero<string>( string.Empty, action, elseVal );
			Assert.AreEqual( "default", actual );

			actual = ExtendSafety.NotZero<string>( null, action, elseVal );
			Assert.AreEqual( "default", actual );

			actual = ExtendSafety.NotZero<string>( "value", action, elseVal );
			Assert.AreEqual( "override", actual );
		}


		[TestMethod()]
		public void NotZeroTest4()
		{
			Func<string, string> action = (x => "override");
			Func<string> elseAction = (() => "default");
			string actual;

			actual = ExtendSafety.NotZero<string>( string.Empty, action, elseAction );
			Assert.AreEqual( "default", actual );

			actual = ExtendSafety.NotZero<string>( null, action, elseAction );
			Assert.AreEqual( "default", actual );

			actual = ExtendSafety.NotZero<string>( "value", action, elseAction );
			Assert.AreEqual( "override", actual );
		}

		[TestMethod()]
		public void NotZeroTest5()
		{
			string target = string.Empty;
			Func<string, string> work = x => "override";
			string actual;

			actual = ExtendSafety.NotZero<string>( target, work );
			Assert.AreEqual( null, actual );

			target = null;

			actual = ExtendSafety.NotZero<string>( target, work );
			Assert.AreEqual( null, actual );

			target = "1";

			actual = ExtendSafety.NotZero<string>( target, work );
			Assert.AreEqual( "override", actual );
		}

		/// <summary>
		///A test for Nullable
		///</summary>
		public void NullableTestHelper<T>()
			where T : struct
		{
			T target = new T(); 
			T expected = default(T);
			Nullable<T> actual;
			actual = ExtendSafety.Nullable<T>( target );
			Assert.AreEqual( expected, actual );
		}

		[TestMethod()]
		public void NullableTest()
		{
			NullableTestHelper<int>();
			NullableTestHelper<Guid>();
			NullableTestHelper<bool>();
		}
	}
}
