﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Fire.Extensions;

namespace Ice.Extensions
{
	
	
	/// <summary>
	///This is a test class for ExtendStringTest and is intended
	///to contain all ExtendStringTest Unit Tests
	///</summary>
	[TestClass()]
	public class ExtendIntegerTest
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		/// <summary>
		///A test for Capitalise
		///</summary>
		[TestMethod()]
		public void OrdinalTest()
		{
			Assert.AreEqual( "st", 1.Ordinal() );
			Assert.AreEqual( "nd", 2.Ordinal() );
			Assert.AreEqual( "rd", 3.Ordinal() );
			Assert.AreEqual( "th", 4.Ordinal() );
			Assert.AreEqual( "th", 10.Ordinal() );
			Assert.AreEqual( "th", 11.Ordinal() );
			Assert.AreEqual( "st", 21.Ordinal() );
		}

	}
}
