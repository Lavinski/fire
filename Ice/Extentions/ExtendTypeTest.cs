﻿using Fire.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ice.Extensions
{
    
    
    /// <summary>
    ///This is a test class for ExtendTypeTest and is intended
    ///to contain all ExtendTypeTest Unit Tests
    ///</summary>
	[TestClass()]
	public class ExtendTypeTest
	{
		class TypeA : TypeB { }
		class TypeB { }

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		/// <summary>
		///A test for RootType
		///</summary>
		[TestMethod()]
		public void RootTypeTest()
		{
			Type target = typeof( TypeA ); 
			Assert.AreEqual( typeof( TypeB ), ExtendType.RootType( target ) );
		}
	}
}
