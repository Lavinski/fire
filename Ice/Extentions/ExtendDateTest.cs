﻿using Fire.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ice.Extensions
{
	
	
	/// <summary>
	///This is a test class for ExtendDateTest and is intended
	///to contain all ExtendDateTest Unit Tests
	///</summary>
	[TestClass()]
	public class ExtendDateTest
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		///<summary>
		///A test for DaysOfAMonth
		///</summary>
		[TestMethod()]
		public void DaysOfAMonthTest()
		{
			// This proves that there are at least 4 of each DayOfTheWeek in each month
			// Tested from 2008 to 2019

			for (int y = 2009; y < 2012; y++) {
				for (int i = 1; i < 13; i++) {
					var days = new DateTime( y, i, 1 ).DaysOfAMonth();

					foreach (var day in days) {
						Assert.IsTrue( day.Value >= 4 );
					}

				}
			}
		}


		/// <summary>
		///A test for ReachedDate
		///</summary>
		[TestMethod()]
		public void ReachedDateTest()
		{
			Assert.IsTrue( DateTime.Today.ReachedDate( DateTime.Today ) );
			Assert.IsFalse( DateTime.Today.ReachedDate( DateTime.Today.AddDays( 1 ) ) );
			Assert.IsTrue( DateTime.Today.ReachedDate( DateTime.Today.AddDays( -1 ) ) );
		}

		
		/// <summary>
		///A test for IsBefore
		///</summary>
		[TestMethod()]
		public void IsBeforeTest()
		{
			Assert.IsFalse( DateTime.Today.IsBeforeDate( DateTime.Today ) );
			Assert.IsTrue( DateTime.Today.IsBeforeDate( DateTime.Today.AddDays( 1 ) ) );
			Assert.IsFalse( DateTime.Today.IsBeforeDate( DateTime.Today.AddDays( -1 ) ) );
		}

		/// <summary>
		///A test for IsAfter
		///</summary>
		[TestMethod()]
		public void IsAfterTest()
		{
			var dateA = new DateTime( 2011, 1, 1, 1, 1, 1 );
			var dateB = new DateTime( 2011, 1, 2, 0, 0, 0 );
			var dateC = new DateTime( 2011, 1, 2, 23, 59, 59 );
			var dateD = new DateTime( 2011, 1, 3, 1, 1, 1 );


			Assert.IsFalse( dateA.IsAfterDate( dateA ) );
			Assert.IsFalse( dateA.IsAfterDate( dateB ) );
			Assert.IsFalse( dateA.IsAfterDate( dateC ) );
			Assert.IsFalse( dateA.IsAfterDate( dateD ) );

			Assert.IsTrue( dateD.IsAfterDate( dateC ) );
			Assert.IsTrue( dateD.IsAfterDate( dateB ) );
			Assert.IsTrue( dateD.IsAfterDate( dateA ) );
		}

		/// <summary>
		///A test for GetClosestWeekday
		///</summary>
		[TestMethod()]
		public void GetClosestWeekdayTest()
		{
			DateTime time = new DateTime(2010, 7, 17); // Saturday
			DateTime expected = new DateTime( 2010, 7, 16 ); // Friday
			DateTime actual;

			actual = ExtendDate.GetClosestWeekday( time );
			Assert.AreEqual( expected, actual );


			time = new DateTime( 2010, 7, 18 ); // Sunday
			expected = new DateTime( 2010, 7, 19 ); // Monday

			actual = ExtendDate.GetClosestWeekday( time );
			Assert.AreEqual( expected, actual );
		}

		/// <summary>
		///A test for GetClosestWeekdayInMonth
		///</summary>
		[TestMethod()]
		public void GetClosestWeekdayInMonthTest()
		{
			var rangeA = new DateTime( 2010, 1, 1 );
			var rangeB = new DateTime( 2011, 1, 1 );

			var firstSaturdays = ExtendDate.GetDateWhereEqual( Fire.Place.First, DayOfWeek.Saturday, rangeA, rangeB );
			var firstSundays = ExtendDate.GetDateWhereEqual( Fire.Place.First, DayOfWeek.Sunday, rangeA, rangeB );

			var secondSaturdays = ExtendDate.GetDateWhereEqual( Fire.Place.Second, DayOfWeek.Saturday, rangeA, rangeB );
			var secondSundays = ExtendDate.GetDateWhereEqual( Fire.Place.Second, DayOfWeek.Sunday, rangeA, rangeB );

			var lastSaturdays = ExtendDate.GetDateWhereEqual( Fire.Place.Last, DayOfWeek.Saturday, rangeA, rangeB );
			var lastSundays = ExtendDate.GetDateWhereEqual( Fire.Place.Last, DayOfWeek.Sunday, rangeA, rangeB );


			DateTime time, actual;

			Assert.IsTrue( firstSaturdays.Count > 0 );
			for (int i = 0; i < firstSaturdays.Count; i++) {
				time = firstSaturdays[i];

				actual = ExtendDate.GetClosestWeekdayInMonth( time );
				Assert.IsTrue( ExtendDate.IsWeekday( actual ) );
				Assert.AreEqual( time.Month, actual.Month );
				Assert.AreEqual( time.Year, actual.Year );
				Assert.IsTrue( time < actual );
			}

			Assert.IsTrue( firstSundays.Count > 0 );
			for (int i = 0; i < firstSundays.Count; i++) {
				time = firstSundays[i];

				actual = ExtendDate.GetClosestWeekdayInMonth( time );
				Assert.IsTrue( ExtendDate.IsWeekday( actual ) );
				Assert.AreEqual( time.Month, actual.Month );
				Assert.AreEqual( time.Year, actual.Year );
				Assert.IsTrue( time < actual );
			}


			Assert.IsTrue( secondSaturdays.Count > 0 );
			for (int i = 0; i < secondSaturdays.Count; i++) {
				time = secondSaturdays[i];

				actual = ExtendDate.GetClosestWeekdayInMonth( time );
				Assert.IsTrue( ExtendDate.IsWeekday( actual ) );
				Assert.AreEqual( time.Month, actual.Month );
				Assert.AreEqual( time.Year, actual.Year );
				Assert.IsTrue( time > actual );
			}

			Assert.IsTrue( secondSundays.Count > 0 );
			for (int i = 0; i < secondSundays.Count; i++) {
				time = secondSundays[i];

				actual = ExtendDate.GetClosestWeekdayInMonth( time );
				Assert.IsTrue( ExtendDate.IsWeekday( actual ) );
				Assert.AreEqual( time.Month, actual.Month );
				Assert.AreEqual( time.Year, actual.Year );
				Assert.IsTrue( time < actual );
			}


			Assert.IsTrue( lastSaturdays.Count > 0 );
			for (int i = 0; i < lastSaturdays.Count; i++) {
				time = lastSaturdays[i];

				actual = ExtendDate.GetClosestWeekdayInMonth( time );
				Assert.IsTrue( ExtendDate.IsWeekday( actual ) );
				Assert.AreEqual( time.Month, actual.Month );
				Assert.AreEqual( time.Year, actual.Year );
				Assert.IsTrue( time > actual );
			}

			Assert.IsTrue( lastSundays.Count > 0 );
			for (int i = 0; i < lastSundays.Count; i++) {
				time = lastSundays[i];

				actual = ExtendDate.GetClosestWeekdayInMonth( time );
				Assert.IsTrue( ExtendDate.IsWeekday( actual ) );
				Assert.AreEqual( time.Month, actual.Month );
				Assert.AreEqual( time.Year, actual.Year );
				Assert.IsTrue( time > actual );
			}
		}

		/// <summary>
		///A test for GetPlaceInMonth
		///</summary>
		[TestMethod()]
		public void GetPlaceInMonth()
		{
			Assert.AreEqual( new DateTime( 2011, 1, 1 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.First, Fire.Weekday.Day ));
			Assert.AreEqual( new DateTime( 2011, 1, 2 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.Second, Fire.Weekday.Day ) );
			Assert.AreEqual( new DateTime( 2011, 1, 3 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.Third, Fire.Weekday.Day ) );
			Assert.AreEqual( new DateTime( 2011, 1, 31 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.Last, Fire.Weekday.Day ) );

			Assert.AreEqual( new DateTime( 2011, 1, 3 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.First, Fire.Weekday.Weekday ) );
			Assert.AreEqual( new DateTime( 2011, 1, 4 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.Second, Fire.Weekday.Weekday ) );
			Assert.AreEqual( new DateTime( 2011, 1, 5 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.Third, Fire.Weekday.Weekday ) );
			Assert.AreEqual( new DateTime( 2011, 1, 31 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.Last, Fire.Weekday.Weekday ) );

			Assert.AreEqual( new DateTime( 2011, 1, 1 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.First, Fire.Weekday.WeekendDay ) );
			Assert.AreEqual( new DateTime( 2011, 1, 2 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.Second, Fire.Weekday.WeekendDay ) );
			Assert.AreEqual( new DateTime( 2011, 1, 8 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.Third, Fire.Weekday.WeekendDay ) );
			Assert.AreEqual( new DateTime( 2011, 1, 30 ), new DateTime( 2011, 1, 1 ).GetPlaceInMonth( Fire.Place.Last, Fire.Weekday.WeekendDay ) );
		}

		/// <summary>
		///A test for GetDayStart
		///</summary>
		[TestMethod()]
		public void GetDayStartTest()
		{
			DateTime time = new DateTime( 2010, 7, 21, 11, 11, 11); 
			DateTime expected = new DateTime(2010, 7, 21, 0, 0, 0);
			DateTime actual;
			actual = ExtendDate.GetDayStart( time );
			Assert.AreEqual( expected, actual );
		}

		/// <summary>
		///A test for GetDayEnd
		///</summary>
		[TestMethod()]
		public void GetDayEndTest()
		{
			DateTime time = new DateTime( 2010, 7, 21, 11, 11, 11); 
			DateTime expected = new DateTime(2010, 7, 21, 23, 59, 59);
			DateTime actual;
			actual = ExtendDate.GetDayEnd( time );
			Assert.AreEqual( expected, actual );
		}

		/// <summary>
		///A test for GetFirstDayOfMonth
		///</summary>
		[TestMethod()]
		public void GetFirstDayOfMonthTest()
		{
			DateTime time = new DateTime(2010, 7, 30); // TODO: Initialize to an appropriate value
			DateTime expected = new DateTime(2010, 7, 1); // TODO: Initialize to an appropriate value
			DateTime actual;
			
			actual = ExtendDate.GetFirstDayOfMonth( time );
			Assert.AreEqual( expected, actual );
		}

		/// <summary>
		///A test for GetNextWeekday
		///</summary>
		[TestMethod()]
		public void GetNextWeekdayTest()
		{
			DateTime time = new DateTime(2010, 7, 17); // Saturday
			DateTime expected = new DateTime(2010, 7, 19); // Monday
			DateTime actual;

			actual = ExtendDate.GetNextWeekday( time );
			Assert.AreEqual( expected, actual );
			

			time = new DateTime( 2010, 7, 18 ); // Sunday
			expected = new DateTime( 2010, 7, 19 ); // Monday

			actual = ExtendDate.GetNextWeekday( time );
			Assert.AreEqual( expected, actual );
		}

		/// <summary>
		///A test for GetWeekdayInMonth
		///</summary>
		[TestMethod()]
		public void GetWeekdayInMonthTest()
		{
			DateTime time;

			// First Xday is in the second week
			time = new DateTime( 2010, 07, 1 ).GetWeekdayInMonth( new DateTime( 2010, 7, 1 ) );
			Assert.AreEqual( 7, time.Month );
			Assert.AreEqual( DayOfWeek.Thursday, time.DayOfWeek );
			Assert.AreEqual( 1, time.Day );

			time = new DateTime( 2010, 08, 20 ).GetWeekdayInMonth( new DateTime( 2010, 7, 1 ) );
			Assert.AreEqual( 8, time.Month );
			Assert.AreEqual( DayOfWeek.Thursday, time.DayOfWeek );
			Assert.AreEqual( 5, time.Day );

			time = new DateTime( 2010, 09, 11 ).GetWeekdayInMonth( new DateTime( 2010, 7, 1 ) );
			Assert.AreEqual( 9, time.Month );
			Assert.AreEqual( DayOfWeek.Thursday, time.DayOfWeek );
			Assert.AreEqual( 2, time.Day );

			// Last Xday is outside the month
			time = new DateTime( 2010, 07, 15 ).GetWeekdayInMonth( new DateTime( 2010, 7, 31 ) );
			Assert.AreEqual( 7, time.Month );
			Assert.AreEqual( DayOfWeek.Saturday, time.DayOfWeek );
			Assert.AreEqual( 31, time.Day );

			time = new DateTime( 2010, 08, 21 ).GetWeekdayInMonth( new DateTime( 2010, 7, 31 ) );
			Assert.AreEqual( 8, time.Month );
			Assert.AreEqual( DayOfWeek.Saturday, time.DayOfWeek );
			Assert.AreEqual( 28, time.Day );

			time = new DateTime( 2010, 09, 11 ).GetWeekdayInMonth( new DateTime( 2010, 7, 31 ) );
			Assert.AreEqual( 9, time.Month );
			Assert.AreEqual( DayOfWeek.Saturday, time.DayOfWeek );
			Assert.AreEqual( 25, time.Day );

			// Normal Case
			time = new DateTime( 2010, 07, 30 ).GetWeekdayInMonth( new DateTime( 2010, 7, 16 ) );
			Assert.AreEqual( 7, time.Month );
			Assert.AreEqual( DayOfWeek.Friday, time.DayOfWeek );
			Assert.AreEqual( 16, time.Day );

			time = new DateTime( 2010, 06, 11 ).GetWeekdayInMonth( new DateTime( 2010, 7, 16 ) );
			Assert.AreEqual( 6, time.Month );
			Assert.AreEqual( DayOfWeek.Friday, time.DayOfWeek );
			Assert.AreEqual( 18, time.Day );

			time = new DateTime( 2010, 08, 12 ).GetWeekdayInMonth( new DateTime( 2010, 7, 16 ) );
			Assert.AreEqual( 8, time.Month );
			Assert.AreEqual( DayOfWeek.Friday, time.DayOfWeek );
			Assert.AreEqual( 20, time.Day );


			// First Xday is in the second week
			time = new DateTime( 2010, 07, 17 ).GetWeekdayInMonth( new DateTime( 2010, 7, 17 ) );
			Assert.AreEqual( 7, time.Month );
			Assert.AreEqual( DayOfWeek.Saturday, time.DayOfWeek );
			Assert.AreEqual( 17, time.Day );

			int month = 7;
			int year = 2010;
			for (int i = 1; i < DateTime.DaysInMonth( year, month ); i++) {

				// They are the same date
				DateTime start = new DateTime( 2010, month, i );
				DateTime current = new DateTime( 2010, month, i );

				time = current.GetWeekdayInMonth( start );

				Assert.AreEqual( month, time.Month );
				Assert.AreEqual( start.DayOfWeek, time.DayOfWeek );
				Assert.AreEqual( i, time.Day );
			}
		}

		/// <summary>
		///A test for MonthsDifference
		///</summary>
		[TestMethod()]
		public void MonthsDifferenceTest()
		{
			DateTime startDate = new DateTime(2009, 1, 1); 
			DateTime endDate = new DateTime(2010, 12, 1); 
			int expected = 23;
			int actual;
			actual = ExtendDate.MonthsDifference( startDate, endDate );
			Assert.AreEqual( expected, actual );
		}

		/// <summary>
		///A test for YearsDifference
		///</summary>
		[TestMethod()]
		public void YearsDifferenceTest()
		{
			DateTime startDate = new DateTime(2000, 1, 1);
			DateTime endDate = new DateTime(2010, 1, 1); 
			int expected = 10; 
			int actual;
			actual = ExtendDate.YearsDifference( startDate, endDate );
			Assert.AreEqual( expected, actual );
		}
	}
}
