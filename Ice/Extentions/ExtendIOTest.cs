﻿
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Fire.Extensions.IO;
using System.IO;

namespace Ice.Extensions
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class TestExtendIO
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		
		[TestMethod]
		public void TestResolve()
		{

			var basePath = "C:\\Windows\\System32";
			
			Assert.AreEqual( "C:\\Windows\\temp", "seek(Windows)\\Temp".ResolvePath(basePath) );
			Assert.AreEqual( "C:\\Windows", "seek(windows)".ResolvePath(basePath) );
			Assert.AreEqual( "C:\\Windows\\System32", ".".ResolvePath(basePath) );
			Assert.AreEqual( "C:\\Windows\\System32\\Bin", "Bin".ResolvePath(basePath) );
		}

	}
}
