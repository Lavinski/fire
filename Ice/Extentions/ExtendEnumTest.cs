﻿using Fire.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Ice.Extensions
{
    
    
    /// <summary>
    ///This is a test class for ExtendEnumTest and is intended
    ///to contain all ExtendEnumTest Unit Tests
    ///</summary>
	[TestClass()]
	public class ExtendEnumTest
	{
		enum Values {
			CammelCaseEnum,
			UPPERCASE,
			lowercase
		}

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }


		/// <summary>
		///A test for FromCamelCase
		///</summary>
		[TestMethod()]
		public void FromCamelCaseTest()
		{
			Enum value = Values.CammelCaseEnum; 
			string expected = "Cammel Case Enum";
			string actual;

			actual = ExtendEnum.FromCamelCase( value );
			Assert.AreEqual( expected, actual );
		}

		/// <summary>
		///A test for FromCamelCase
		///</summary>
		[TestMethod()]
		public void GetValuesTest()
		{
			var actual = (IEnumerable<Values>)null;

			actual = ExtendEnum.GetValues<Values>();
			Assert.AreEqual( 3, actual.Count() );
		}
	}
}
