﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Fire.Extensions.Crytography;

namespace Ice.Extensions
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class TestExtendCrytography
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		
		[TestMethod]
		public void TestCrypto()
		{
			var key = "MyPa33w0rd";
			var data = "String data";
			var message = data.Encrypt( key );

			Assert.AreNotEqual( data, message );
			Assert.AreEqual( data, message.Decrypt( key ) );
		}

	}
}
