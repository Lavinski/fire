﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Fire.Extensions.Database;

namespace Ice.Extensions.Database
{


	/// <summary>
	///This is a test class for ExtendStringTest and is intended
	///to contain all ExtendStringTest Unit Tests
	///</summary>
	[TestClass()]
	public class ExtendStringTest
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		/// <summary>
		///A test for Limit
		///</summary>
		[TestMethod()]
		public void LimitTest()
		{
			string value = "1234567890"; 
			int maxLength = 5;
			string expected = "12345";
			string actual;

			actual = ExtendString.Limit( value, maxLength );
			Assert.AreEqual( expected, actual );

			actual = ExtendString.Limit( null, maxLength );
			Assert.AreEqual( null, actual );

			try 
			{	        
				actual = ExtendString.Limit( value, -1 );
				Assert.Fail();
			}
			catch (Exception) {}

		}
	}
}