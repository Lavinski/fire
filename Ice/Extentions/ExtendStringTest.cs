﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Fire.Extensions;

namespace Ice.Extensions
{
	
	
	/// <summary>
	///This is a test class for ExtendStringTest and is intended
	///to contain all ExtendStringTest Unit Tests
	///</summary>
	[TestClass()]
	public class ExtendStringTest
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		/// <summary>
		///A test for Capitalise
		///</summary>
		[TestMethod()]
		public void CapitaliseTest()
		{
			string value = "capitalise test";
			string expected = "Capitalise test";
			string actual;

			actual = ExtendString.Capitalise( value );
			Assert.AreEqual( expected, actual );
		}

		/// <summary>
		///A test for Parse
		///</summary>
		[TestMethod()]
		public void ParseTest()
		{
			object boolean;
			ExtendConversion.ConvertTo( "true", typeof(bool), out boolean );

			Assert.AreEqual( true, boolean );
		}

		/// <summary>
		///A test for FromCamelCase
		///</summary>
		[TestMethod()]
		public void FromCamelCaseTest()
		{
			string value, expected, actual;

			value = "CammelCaseTest 1/ABC";
			expected = "Cammel Case Test 1/ABC"; 

			actual = ExtendString.FromCamelCase( value );
			Assert.AreEqual( expected, actual );


			value = "*&$@%#$DTY123123FWFVDTRF^R@#%^E$^VC@";
			expected = "*&$@%#$DTY 123123FWFVDTRF^R@#%^E$^VC@";

			actual = ExtendString.FromCamelCase( value );
			Assert.AreEqual( expected, actual );


			value = "UPPERLowerRR";
			expected = "UPPER LowerRR";

			actual = ExtendString.FromCamelCase( value );
			Assert.AreEqual( expected, actual );


			value = "NumbersAfterWords1111@";
			expected = "Numbers After Words 1111@";

			actual = ExtendString.FromCamelCase( value );
			Assert.AreEqual( expected, actual );
		}

		/// <summary>
		///A test for IsWhiteSpace
		///</summary>
		[TestMethod()]
		public void IsWhiteSpaceTest()
		{
			bool actual;

			actual = ExtendString.IsWhiteSpace( "\t\r\n " );
			Assert.AreEqual( true, actual );

			actual = ExtendString.IsWhiteSpace( "  a  " );
			Assert.AreEqual( false, actual );

			actual = ExtendString.IsWhiteSpace( "  a   " );
			Assert.AreEqual( false, actual );

			actual = ExtendString.IsWhiteSpace( "   a  " );
			Assert.AreEqual( false, actual );

			actual = ExtendString.IsWhiteSpace( "     a" );
			Assert.AreEqual( false, actual );

			actual = ExtendString.IsWhiteSpace( " " );
			Assert.AreEqual( true, actual );
		}

		/// <summary>
		///A test for StripExcessWhitespace
		///</summary>
		[TestMethod()]
		public void StripExcessWhitespaceTest()
		{
			string target = "\t\r\n		       ";
			string expected = " ";
			string actual;

			actual = ExtendString.StripExcessWhitespace( target );
			Assert.AreEqual( expected, actual );
		}

		
	}
}
