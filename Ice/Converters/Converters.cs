﻿using Fire.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ComponentModel;
using Description = System.ComponentModel.DescriptionAttribute;

namespace Ice.Converters
{
	
	/// <summary>
	/// This is a test class for QueryTest and is intended
	/// to contain all QueryTest Unit Tests
	/// </summary>
	[TestClass()]
	public class ConvertersTest
	{

		/// <summary>
		/// Gets or sets the test context which provides
		/// information about and functionality for the current test run.
		/// </summary>
		public TestContext TestContext { get; set; }

		[TypeConverter(typeof(EnumDescriptions))]
		enum Values
		{
			[Description( "AAAA" )] ValueA,
			[Description( "BBBB" )] ValueB
		}

		/// <summary>
		/// SQL Generation Test using all the options
		/// </summary>
		[TestMethod()]
		public void ToStringTest()
		{
			Assert.AreEqual( "AAAA", TypeDescriptor.GetConverter(Values.ValueA.GetType()).ConvertTo( Values.ValueA, typeof( string ) ) );
			Assert.AreEqual( "ValueA", Values.ValueA.ToString() );
		}

		/// <summary>
		/// SQL Generation Test using all the options
		/// </summary>
		[TestMethod()]
		public void BytesToHex()
		{
			byte[] bytes = new byte[] { 0, 1, 2, 4, 8, 16, 32, 64, 128, 255 };
			string hex = HexConverter.BytesToHexString( bytes, 0, 10 );

			Assert.AreEqual( 20, hex.Length );
			Assert.AreEqual( "000102040810204080FF", hex );
		}
	}
}
