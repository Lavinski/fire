﻿using Fire.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using Fire.Configuration;

namespace Ice.Configuration
{
	
	/// <summary>
	/// This is a test class for QueryTest and is intended
	/// to contain all QueryTest Unit Tests
	/// </summary>
	[TestClass()]
	public class ConfigurationTest
	{

		/// <summary>
		/// Gets or sets the test context which provides
		/// information about and functionality for the current test run.
		/// </summary>
		public TestContext TestContext { get; set; }


		Dictionary<string, string> vals = new Dictionary<string, string>() {
			{"One", "1"},
			{"Two", "2"},
			{"Three", "3"}
		};

		public class Values
		{
			public int One { get; set; }
			public int Two { get; set; }
			public int Three { get; set; }
		}

		/// <summary>
		/// Fill a test object
		/// </summary>
		[TestMethod()]
		public void FillTest()
		{
			var settings = new Values();
			Inflate.Instance( settings, x => vals[x] );

			Assert.AreEqual( 1, settings.One);
			Assert.AreEqual( 2, settings.Two );
			Assert.AreEqual( 3, settings.Three );
		}
	}
}
