﻿using Fire.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Fire;

namespace Ice.Extensions
{
	
	/// <summary>
	///This is a test class for ExtendSafetyTest and is intended
	///to contain all ExtendSafetyTest Unit Tests
	///</summary>
	[TestClass()]
	public class FluentTest
	{

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }


		class TypeA : TypeB { }
		class TypeB : TypeC { }
		class TypeC  { }



		[TestMethod()]
		public void NullSafe()
		{
			var typed = new TypeA().AsFluent().NotNull( x => "It wasn't null" ).Default( "It was null" );
			Assert.AreEqual( "It wasn't null", typed );
		}

		[TestMethod()]
		public void TypeSwitchTest()
		{
			var typed = new TypeA().AsFluent().NotNull( x => "It wasn't null" ).Default( "It was null" );
			
			var switched = new TypeB().AsFluent()
										.Switch<string>()
											.Case<TypeA>(x => "TypeA" )
											.Case<TypeC>(x => "TypeC" )
											.Default( "default case" );

			
			Assert.AreEqual( "TypeC", switched );
		}

		[TestMethod()]
		public void TypeConditionSwitchTest()
		{

			var switched = new TypeA().AsFluent()
										.Switch<string>()
											.Case<TypeB>( x => { return true; }, x => "TypeA" )
											.Case<TypeC>( x => { return true; }, x => "TypeC" )
											.Default();

			Assert.IsNotNull( switched );
		}

		[TestMethod()]
		public void BasicSwitchTest()
		{
			var switched = 1.AsFluent()
								.Switch<string>()
									.Case( y => (y == 2), x => x.ToString() )
									.Case( y => (y == 1), x => x.ToString() )
									.Default( "default case" );

			Assert.AreEqual( "1", switched );
		}

		[TestMethod()]
		public void PredicateSwitchTest()
		{
			var switched = "1".AsFluent()
								.Switch<string>()
									.Case("2", "Two" )
									.Case("1", "One" )
									.Default( "default case" );

			Assert.AreEqual( "One", switched );
		}

		[TestMethod()]
		public void SwitchDefaultTest()
		{
			var switched = "3".AsFluent()
								.Switch<string>()
									.Case( "2", "Two" )
									.Case( "1", "One" )
									.Default( "default case" );

			Assert.AreEqual( "default case", switched );

			switched = "3".AsFluent()
								.Switch<string>()
								.Default();

			Assert.AreEqual( default(string) , switched );
		}
	}
}
