﻿using Fire.Regex;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ice.Regex
{
    
    
    /// <summary>
    ///This is a test class for ValidatiorsTest and is intended
    ///to contain all ValidatiorsTest Unit Tests
    ///</summary>
	[TestClass()]
	public class ValidatiorsTest
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		/// <summary>
		///A test for Validate
		///</summary>
		[TestMethod()]
		public void ValidateTest()
		{
			string[] fail = new[] { "", "-", "1234", "asdf", "FGJH", "^%$@", "T", "F", "0", "1", "Y", "N" };
			string[] succ = new[] { "E26DA473-85BD-4C0C-9C05-D7E043B36CCE" };

			foreach (var item in fail) {
				Assert.AreEqual( false, Validatiors.Validate( item, Validatiors.isGuid ) );
			}

			foreach (var item in succ) {
				Assert.AreEqual( true, Validatiors.Validate( item, Validatiors.isGuid ) );
			}
		}
	}
}
