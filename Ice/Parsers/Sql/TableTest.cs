﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Fire.Converters;
using Fire.Parsers.Sql;
using Fire.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ice.Parsers.Sql
{
	[TestClass]
	public class TableTest
	{
		class TestParser : SqlParser
		{
			public TestParser(Stream textStream) 
				: base(textStream) { }

			public new Table ParseTable() { return base.ParseTable(); }
		}

		[TestMethod]
		public void Control()
		{
			var parser = new TestParser("[This is a name].[yes] ( )".ToStream());

			var table =  parser.ParseTable();
			Assert.AreEqual("[This is a name].[yes]", table.Identifier.Text);
		}

		[TestMethod]
		public void InvalidStart()
		{
			var parser = new TestParser("[name] End".ToStream());

			try {
				parser.ParseTable();
			} catch (ValidationException ex) {

				Assert.IsNotNull( ex.Result );
			}
			
		}

		[TestMethod]
		public void InvalidEnd()
		{
			var parser = new TestParser("[name] ( End".ToStream());

			try {
				parser.ParseTable();
			} catch (ValidationException ex) {

				Assert.IsNotNull( ex.Result );
			}
		}

		[TestMethod]
		public void Columns()
		{
			var parser = new TestParser("[name] ( One varchar(10), Two int, Three datetime);".ToStream());

			var table = parser.ParseTable();

			Assert.AreEqual(3, table.Columns.Count);
			Assert.AreEqual("One", table.Columns[0].Name);
			Assert.AreEqual("Two", table.Columns[1].Name);
			Assert.AreEqual("Three", table.Columns[2].Name);
		}
	}
}
