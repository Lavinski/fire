﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Fire.Converters;
using Fire.Parsers.Sql;
using Fire.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ice.Parsers.Sql
{
	[TestClass]
	public class NameTest
	{
		class TestParser : SqlParser
		{
			public TestParser(Stream textStream) 
				: base(textStream) { }

			public new string ParseName() { return base.ParseName(); }
		}

		[TestMethod]
		public void ParseNameSquare()
		{
			var parser = new TestParser("[This is a name]".ToStream());

			Assert.AreEqual("[This is a name]", parser.ParseName());
		}

		[TestMethod]
		public void ParseNameNormal()
		{
			var parser = new TestParser("Table (".ToStream());

			Assert.AreEqual("Table", parser.ParseName());
		}

		[TestMethod]
		public void ParseNameInvalid()
		{
			var parser = new TestParser("]Table[".ToStream());

			try {
				parser.ParseName();
			} catch (ValidationException ex) {

				Assert.IsNotNull( ex.Result );
			}
		}

		[TestMethod]
		public void ParseNameEmpty()
		{
			var parser = new TestParser(" ".ToStream());

			try {
				parser.ParseName();
			} catch (ValidationException ex) {

				Assert.IsNotNull( ex.Result );
			}
		}

		[TestMethod]
		public void ParseNameShort()
		{
			var parser = new TestParser("[]".ToStream());

			try {
				parser.ParseName();
			} catch (ValidationException ex) {

				Assert.IsNotNull( ex.Result );
			}
		}
	}
}
