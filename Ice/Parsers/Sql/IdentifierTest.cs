﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Fire.Converters;
using Fire.Parsers.Sql;
using Fire.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ice.Parsers.Sql
{
	[TestClass]
	public class IdentifierTest
	{
		class TestParser : SqlParser
		{
			public TestParser(Stream textStream) 
				: base(textStream) { }

			public new Identifier ParseIdentifier() { return base.ParseIdentifier(); }
		}

		[TestMethod]
		public void ParseNameNormal()
		{
			var parser = new TestParser("[This is a name].[yes]".ToStream());

			Assert.AreEqual("[This is a name].[yes]", parser.ParseIdentifier().Text);
		}

		[TestMethod]
		public void ParseNameError()
		{
			var parser = new TestParser("[This is a name].".ToStream());

			try {
				parser.ParseIdentifier();
			} catch (ValidationException ex) {

				Assert.IsNotNull( ex.Result );
			}
			
		}
	}
}
