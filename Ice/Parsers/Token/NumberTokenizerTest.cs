﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Fire.Parsers.Token;

namespace Ice.Parsers.Token
{
	[TestClass]
	public class NumberTokenizerTest
	{
		[TestMethod]
		public void FloatTokenTest()
		{
			var prase = @"
        0 1  1 2  2 3
        3 4  4 5  5 6
        6 7  7 8  8 0";

			var words = new[] { 
				0, 1, 1, 2, 2, 3,
				3, 4, 4, 5, 5, 6,
				6, 7, 7, 8, 8, 0
			};

			int index = 0;
			NumberTokenizer tokens = new NumberTokenizer( prase );

			while (!tokens.Done) {

				Assert.AreEqual( (int)words[index++], (int)tokens.NextFloat());
			
			}

			Assert.AreEqual(index, words.Length);
		}

		[TestMethod]
		public void IntTokenTest()
		{
			var prase = @"0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 0 ";

			var words = new[] { 
				0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 0 
			};

			int index = 0;
			NumberTokenizer tokens = new NumberTokenizer(prase);

			while (!tokens.Done) {

				Assert.AreEqual((int)words[index++], int.Parse(tokens.Next()));

			}

			Assert.AreEqual(index, words.Length);
		}
	}
}
