﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Fire.Parsers.Token;

namespace Ice.Parsers.Token
{
	[TestClass]
	public class WordTokenizerTest
	{
		[TestMethod]
		public void WordTokenTest()
		{
			var prase = "This is a bunch of words, how many? ehzz";
			var words = new[] { "This", "is", "a", "bunch", "of", "words", "how", "many", "ehzz" };

			int index = 0;
			WordTokenizer tokens = new WordTokenizer( prase );

			while (!tokens.Done) {

				Assert.AreEqual(words[index++], tokens.Next());
			
			}
		}
	}
}
