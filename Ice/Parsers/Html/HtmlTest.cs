﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fire.Parsers.Html;
using System.IO;
using System.Diagnostics;

namespace Ice.Parsers.Csv
{
	/// <summary>
	/// Summary description for HtmlTokenizer
	/// </summary>
	[TestClass]
	public class HtmlTokenizerTest
	{

		public TestContext TestContext { get; set; }

		[TestMethod]
		public void ParseTest()
		{

			IHtmlTokenizer parser = new PartHtmlTokenizer();

			byte[] byteArray = Encoding.ASCII.GetBytes( Ice.Properties.Resources.FullHtml );
			MemoryStream stream = new MemoryStream( byteArray );

			var tags = parser.Parse( stream );


			StringBuilder result = new StringBuilder();
			foreach (var tag in tags) {
				if (tag.Type != TagType.Text && tag.Type != TagType.CData && tag.Type != TagType.Comment) {
					result.AppendLine( tag.ToString() );
				}
			}

			string r = result.ToString();
			
			// The following is a tag list with the number of attributes
			Assert.AreEqual(
@"!doctype
<html> (1)
<head> (0)
<title> (0)
</title>
<meta> (2)
<link> (3)
<link> (3)
<link> (3)
<link> (6)
<script> (2)
</script>
<script> (1)
</script>
<script> (2)
</script>
<script> (2)
</script>
</head>
<body> (1)
<form> (4)
<div> (0)
<input> (4)
<input> (4)
<input> (4)
<input> (4)
</div>
<script> (1)
</script>
<div> (0)
<input> (4)
</div>
<div> (1)
<div> (1)
<a> (1)
<img> (4)
</a>
</div>
<div> (2)
<span> (1)
</span>
<a> (1)
</a>
<a> (1)
</a>
</div>
</div>
<div> (1)
</div>
<br> (0)
<div> (1)
<div> (1)
<h1> (0)
<span> (1)
</span>
</h1>
<ul> (0)
<li> (0)
<a> (1)
</a>
</li>
</ul>
</div>
<div> (1)
<h1> (0)
</h1>
<ul> (0)
<li> (0)
<a> (1)
</a>
<li> (0)
<a> (1)
</a>
<li> (0)
<a> (1)
</a>
<li> (0)
<a> (1)
</a>
<li> (0)
<a> (1)
</a>
<li> (0)
<a> (1)
</a>
<li> (0)
<a> (1)
</a>
</li>
</ul>
</div>
<div> (1)
<h1> (0)
</h1>
<ul> (0)
<li> (0)
<a> (1)
</a>
<li> (0)
<a> (1)
</a>
</li>
</ul>
</div>
</div>
<div> (2)
</div>
<div> (1)
<div> (1)
<div> (1)
<div> (1)
<h1> (0)
</h1>
<div> (1)
</div>
<br> (0)
</div>
<br> (1)
<div> (1)
<div> (1)
<a> (1)
</a>
</div>
<div> (1)
<a> (1)
</a>
</div>
<div> (1)
<a> (1)
</a>
</div>
<div> (1)
<a> (1)
</a>
</div>
<div> (1)
<a> (1)
</a>
</div>
</div>
</div>
</div>
<div> (2)
<script> (0)
<div> (1)
</div>
<div> (1)
<div> (1)
<h1> (0)
</h1>
<div> (2)
<table> (5)
<tr> (0)
<td> (3)
</td>
<td> (0)
<div> (1)
<table> (2)
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
</table>
</div>
</td>
<td> (3)
</td>
<td> (0)
<div> (1)
<table> (2)
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
</table>
</div>
<br> (0)
<input> (4)
<label> (1)
</label>
<br> (0)
<input> (3)
<label> (1)
</label>
</td>
</tr>
<tr> (0)
<td> (1)
</td>
<td> (0)
</td>
<td> (0)
</td>
<td> (1)
<input> (5)
<input> (5)
</td>
</tr>
</table>
</div>
</div>
<br> (0)
<div> (1)
<div> (1)
<span> (1)
</span>
</div>
<table> (3)
</tbody>
</table>
</div>
<div> (0)
</div>
</div>
</div>
</div>
<div> (1)
<a> (1)
</a>
<a> (2)
</a>
<a> (2)
</a>
<a> (2)
</a>
</div>
</form>
</body>
</html>
", r );
			
		}

	}
}
