﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fire.Parsers.Csv;
using System.IO;

namespace Ice.Parsers.Csv
{
	/// <summary>
	/// Summary description for CsvParserTest
	/// </summary>
	[TestClass]
	public class CsvParserTest
	{

		public TestContext TestContext { get; set; }

		[TestMethod]
		public void ParseTest()
		{
			var text =
@"heading, heading, heading, heading,
  value  , value  , value  , value  ,";
			
			ICsvParser parser = new CsvParser();

			byte[] byteArray = Encoding.ASCII.GetBytes( text );
			MemoryStream stream = new MemoryStream( byteArray );

			parser.LineParsed += Handle;

			parser.Parse( stream );
		}

		private void Handle( IList<string> values, int row )
		{
			if (row == 1) {
				Assert.AreEqual( 5, values.Count );
				Assert.AreEqual( "heading", values[0] );
				Assert.AreEqual( " heading", values[1] );
				Assert.AreEqual( " heading", values[2] );
				Assert.AreEqual( " heading", values[3] );
				Assert.AreEqual( String.Empty, values[4] );

			} else if (row == 2) {
				Assert.AreEqual( 5, values.Count );

				Assert.AreEqual( "  value  ", values[0] );
				Assert.AreEqual( " value  ", values[1] );
				Assert.AreEqual( " value  ", values[2] );
				Assert.AreEqual( " value  ", values[3] );
				Assert.AreEqual( String.Empty, values[4] );
			}
		}


		[TestMethod]
		public void ParseQuotedTest()
		{
			var text = "\"heading\", \"heading\", \"heading\", \"heading\", \" \"" + Environment.NewLine +
			            "\"value\"  , \"value\"  , \"value\"  , \"value\"  , \" \"";

			ICsvParser parser = new CsvParser();

			byte[] byteArray = Encoding.ASCII.GetBytes( text );
			MemoryStream stream = new MemoryStream( byteArray );

			parser.LineParsed += HandleQuoted;

			parser.Parse( stream, ',', false, true, true );

		}

		private void HandleQuoted( IList<string> values, int row )
		{
			if (row == 1) {
				Assert.AreEqual( 5, values.Count );
				Assert.AreEqual( "heading", values[0] );
				Assert.AreEqual( "heading", values[1] );
				Assert.AreEqual( "heading", values[2] );
				Assert.AreEqual( "heading", values[3] );
				Assert.AreEqual( " ", values[4] );

			} else if (row == 2) {
				Assert.AreEqual( 5, values.Count );

				Assert.AreEqual( "value", values[0] );
				Assert.AreEqual( "value", values[1] );
				Assert.AreEqual( "value", values[2] );
				Assert.AreEqual( "value", values[3] );
				Assert.AreEqual( " ", values[4] );
			}
		}

	}
}
