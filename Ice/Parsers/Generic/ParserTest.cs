﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fire.Parsers;
using Fire.Parsers.Locators;

namespace Ice.Parsers.Generic
{
	[TestClass]
	public class ParserTest
	{
		[TestMethod]
		public void Read()
		{
			var readingText = "ThisIsSomeText. 463v07365";
			var expected = "ThisIsSomeText";

			var stream = new MemoryStream( UnicodeEncoding.Default.GetBytes( readingText ) );
			var reader = new StreamReader( stream );
			var parser = new Parser(reader);

			parser.Read( new AlphaLocator() );

			Assert.AreEqual(expected, parser.Text);
		}

		[TestMethod]
		public void ReadTo()
		{
			var readingText = "46307365_t";
			var expected = "46307365_";

			var stream = new MemoryStream( UnicodeEncoding.Default.GetBytes( readingText ) );
			var reader = new StreamReader( stream );
			var parser = new Parser(reader);

			parser.ReadTo( new AlphaLocator() );
			parser.Back();

			Assert.AreEqual(expected, parser.Text);
		}

		[TestMethod]
		public void Seek()
		{
			var readingText = "ThisIsSomeText. 463v07365";
			var expected = ". 463v07365";

			var stream = new MemoryStream( UnicodeEncoding.Default.GetBytes( readingText ) );
			var reader = new StreamReader( stream );
			var parser = new Parser(reader);

			parser.Seek( new AlphaLocator() );
			parser.Back();
			parser.Read( new EndOfFile() );

			Assert.AreEqual(expected, parser.Text);
		}

		[TestMethod]
		public void SeekTo()
		{
			var readingText = "46307365_t";
			var expected = "t";

			var stream = new MemoryStream( UnicodeEncoding.Default.GetBytes( readingText ) );
			var reader = new StreamReader( stream );
			var parser = new Parser(reader);

			parser.SeekTo( new Word("t") );
			parser.Back();
			parser.Read( new EndOfFile() );

			Assert.AreEqual(expected, parser.Text);
		}
	}
}
