﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Fire.Trees;
using System.Collections;

namespace Ice.Extensions
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class TestTrees
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		
		[TestMethod]
		public void TestTreeWalk()
		{
			var listTree = new List<IList>() {
				new List<IList>() {
					new List<IList>(),
					new List<IList>()
				},
				new List<IList>() {
					new List<IList>(),
					new List<IList>()
				}
			};

			// Demo Tree
			Node<int> expected = new Node<int>() {
				Children = new Node<int>[] {
					new Node<int>() {
						Children = new Node<int>[] {
							new Node<int>(),
							new Node<int>()
						}
					},
					new Node<int>() {
						Children = new Node<int>[] {
							new Node<int>(),
							new Node<int>()
						}
					}
				}
			};

			var helper = new TreeTraversal();

			int counter = 0;
			var output = helper.TransformTree( listTree, x => new Node<int>() { Content = (counter++) } );

			Assert.AreEqual( 0, output.Content );
			Assert.AreEqual( expected.Children.Count, output.Children.Count );
			Assert.AreEqual( 1, output.Children[0].Content );
			Assert.AreEqual( expected.Children[0].Children.Count, output.Children[0].Children.Count );
			Assert.AreEqual( 2, output.Children[1].Content );
			Assert.AreEqual( expected.Children[1].Children.Count, output.Children[1].Children.Count );
			Assert.AreEqual( expected.Children[0].Children[0].Children.Count, output.Children[0].Children[0].Children.Count );
			Assert.AreEqual( expected.Children[0].Children[1].Children.Count, output.Children[0].Children[1].Children.Count );
			Assert.AreEqual( expected.Children[1].Children[0].Children.Count, output.Children[1].Children[0].Children.Count );
			Assert.AreEqual( expected.Children[1].Children[1].Children.Count, output.Children[1].Children[1].Children.Count );

		}

	}
}
