﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Fire.Build;

namespace Ice.Extentions
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class TestBuild
	{
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext { get; set; }

		
		[TestMethod]
		public void TestDBBuild()
		{
			var commandArds = new[] {

				// MSSQL or FireBird
				"t", "Firebird", 
				
				// Create or Update
				"b", "create", 

				//MSSQL: Data Source=Daniel-Laptop\\SQLEXPRESS;Initial Catalog=Scratch;User Id=test;Password=test;
				//FireB: Data Source=localhost; User=SYSDBA; Password=masterkey; ServerType=1; Database=..\app_data\db\twenty.fdb; ClientLibrary=..\lib\firebird;
				"c", @"Data Source=localhost; User=SYSDBA; Password=masterkey; ServerType=1; Database=seek(fire)\Fire.Build\app_data\twenty.fdb; ClientLibrary=seek(fire)\Fire.Build\lib\firebird;", 

				"d", "seek(fire)\\Ice\\Build" 
			};

			Program.Main(commandArds);
		}

	}
}
