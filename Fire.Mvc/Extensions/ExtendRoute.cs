﻿namespace Fire.Mvc.Extensions
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using System.Web.Routing;
	using System.Reflection;
	using System.Linq;
	using Fire.Mvc.Routing.Parsing;


	public static class ExtendRoute
	{
		private static Dictionary<string, ParsedRoute> parsedRoute = new Dictionary<string,ParsedRoute>();

		private static ParsedRoute ParseRoute( Route route )
		{
			if (parsedRoute.ContainsKey( route.Url )) {
				return parsedRoute[route.Url];
			}
			return RouteParser.Parse( route.Url );
		}

		public static RouteValueDictionary GetRouteData( this RouteCollection routes, string url )
		{
			try {
				var coreUrl = new Uri( url );
				var routeUrlPart = coreUrl.AbsolutePath;
				if (routeUrlPart.StartsWith("/") || routeUrlPart.StartsWith("\\")) {
					routeUrlPart = routeUrlPart.Substring(1);
				}

				using (var rtLock = routes.GetReadLock()) {

					foreach (var routeBase in routes) {
						var route = routeBase as Route;
						if (route != null) {

							var paredRoute = ParseRoute( route );
							var values = paredRoute.Match( routeUrlPart, null );

							return values;
						}
					}
				}

			} catch (Exception) { }

			return null;
		}
	}
}
