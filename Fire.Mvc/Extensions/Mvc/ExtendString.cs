﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Extensions.Mvc
{
	public static class ExtendString
	{
		/// <summary> Gets the first value from a comma delimited string </summary>
		public static string FirstQuery( this string target )
		{
			if (target != null && target.Length != 0) {

				var index = target.IndexOf( ',' );
				return target.Substring( 0, (index > 0) ? index : target.Length );
			}

			return target;
		}

		/* TODO: Determine the best XSS method for various cases
		 * public static string Sanitise( this string target )
		{ 
			
		}*/

		public static string RemoveCharacters( this string target, params char[] characters )
		{
			var add = true;
			var cchar = '\0';
			var result = new StringBuilder();

			for (int i = 0; i < target.Length; i++) {
				
				add = true;
				cchar = target[i];

				for (int j = 0; j < characters.Length; j++) {
					if (cchar == characters[j]) {
						add = false;
						break;
					}
				}

				if (add) {
					result.Append(cchar);
				}
				
			}
			return result.ToString();
		}
	}
}
