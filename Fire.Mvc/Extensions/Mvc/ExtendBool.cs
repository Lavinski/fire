﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;

namespace Fire.Extensions.Mvc
{
	public static class ExtendBool
	{
		/// <summary> Creates an object used for Mvc attribute declaration </summary>
		public static object AsAttribute( this bool disabled )
		{
			return (disabled ? (object)new { disabled = true } : new object());
		}

		/// <summary> Creates an object used for Mvc attribute declaration </summary>
		public static object AsAttribute( this bool disabled, string cssClass )
		{
			return (disabled ?
				(object)new { disabled = true, @class = cssClass } :
				(object)new { @class = cssClass });
		}
		
		/// <summary> Creates a dictionary used for Mvc attribute declaration </summary>
		public static IDictionary<string, Object> AsAttribute( this bool disabled, object attribs )
		{
			var attributes = new RouteValueDictionary( attribs );
			if (disabled) {
				attributes.Add( "disabled", true );
			}
			return attributes;
		}
		
	}
}
