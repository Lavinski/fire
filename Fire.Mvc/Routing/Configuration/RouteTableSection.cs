﻿using System;
using System.Configuration;
using System.Collections;
using System.Xml;


namespace Fire.Web.Mvc.Routing.Configuration
{
    public class RouteTableSection : ConfigurationSection
    {
        RouteConfigElement url;

        public RouteTableSection()
        {
            url = new RouteConfigElement();
        }

        [ConfigurationProperty( "routes", IsDefaultCollection = false )]
        public RouteCollection Routes
        {
            get
            {
                return base["routes"] as RouteCollection;
            }
        }

        protected override void DeserializeSection( XmlReader reader )
        {
            base.DeserializeSection(reader);
        }

        protected override string SerializeSection( ConfigurationElement parentElement, string name, ConfigurationSaveMode saveMode )
        {
            return base.SerializeSection(parentElement, name, saveMode);
        }

    }
}
