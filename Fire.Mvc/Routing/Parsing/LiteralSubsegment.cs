﻿namespace Fire.Mvc.Routing.Parsing
{
	public class LiteralSubsegment : PathSubsegment
	{
		// Methods
		public LiteralSubsegment( string literal )
		{
			this.Literal = literal;
		}

		// Properties
		public string Literal { get; private set; }

	}

}