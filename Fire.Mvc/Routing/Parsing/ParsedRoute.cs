﻿namespace Fire.Mvc.Routing.Parsing
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Web.Routing;


	public class ParsedRoute
	{
		public ParsedRoute( IList<PathSegment> pathSegments )
		{
			PathSegments = pathSegments;
		}

		private static bool ForEachParameter( IList<PathSegment> pathSegments, Func<ParameterSubsegment, bool> action )
		{
			for (int i = 0; i < pathSegments.Count; i++) {
				PathSegment segment = pathSegments[i];
				if (!(segment is SeparatorPathSegment)) {
					ContentPathSegment segment2 = segment as ContentPathSegment;
					if (segment2 != null) {
						foreach (PathSubsegment subsegment in segment2.Subsegments) {
							if (!(subsegment is LiteralSubsegment)) {
								ParameterSubsegment arg = subsegment as ParameterSubsegment;
								if ((arg != null) && !action( arg )) {
									return false;
								}
							}
						}
					}
				}
			}
			return true;
		}

		private static ParameterSubsegment GetParameterSubsegment( IList<PathSegment> pathSegments, string parameterName )
		{
			ParameterSubsegment foundParameterSubsegment = null;
			ForEachParameter( pathSegments, delegate( ParameterSubsegment parameterSubsegment ) {
				if (string.Equals( parameterName, parameterSubsegment.ParameterName, StringComparison.OrdinalIgnoreCase )) {
					foundParameterSubsegment = parameterSubsegment;
					return false;
				}
				return true;
			} );
			return foundParameterSubsegment;
		}

		private static bool IsParameterRequired( ParameterSubsegment parameterSubsegment, RouteValueDictionary defaultValues, out object defaultValue )
		{
			if (parameterSubsegment.IsCatchAll) {
				defaultValue = null;
				return false;
			}
			return !defaultValues.TryGetValue( parameterSubsegment.ParameterName, out defaultValue );
		}

		private static bool IsRoutePartNonEmpty( object routePart )
		{
			string str = routePart as string;
			if (str != null) {
				return (str.Length > 0);
			}
			return (routePart != null);
		}

		public RouteValueDictionary Match( string virtualPath, RouteValueDictionary defaultValues )
		{
			IList<string> urlParts = RouteParser.SplitUrlToPathSegmentStrings( virtualPath );
			if (defaultValues == null) {
				defaultValues = new RouteValueDictionary();
			}

			RouteValueDictionary matchedValues = new RouteValueDictionary();
			bool hasParts = false;
			bool flag2 = false;

			for (int segmentIndex = 0; segmentIndex < this.PathSegments.Count; segmentIndex++) {
				PathSegment segment = this.PathSegments[segmentIndex];

				// Check for parts left
				if (urlParts.Count <= segmentIndex) {
					hasParts = true;
				}

				// Get the current segment
				string cSeg = hasParts ? null : urlParts[segmentIndex];

				if (segment is SeparatorPathSegment) {

					// Check SeparatorPath Match
					if (!hasParts && !string.Equals( cSeg, "/", StringComparison.Ordinal )) {
						return null;
					}
				} else {
					ContentPathSegment contentPathSegment = segment as ContentPathSegment;

					// Check ContentPathSegment Match
					if (contentPathSegment != null) {

						// If it's a [*] wildcard
						if (contentPathSegment.IsCatchAll) {

							this.MatchCatchAll( contentPathSegment, urlParts.Skip<string>( segmentIndex ), defaultValues, matchedValues );
							flag2 = true;

							// Check if it's a content match
						} else if (!this.MatchContentPathSegment( contentPathSegment, cSeg, defaultValues, matchedValues )) {
							return null;

						}
					}

				}
			}
			if (!flag2 && (this.PathSegments.Count < urlParts.Count)) {
				for (int j = this.PathSegments.Count; j < urlParts.Count; j++) {
					if (!RouteParser.IsSeparator( urlParts[j] )) {
						return null;
					}
				}
			}
			if (defaultValues != null) {
				foreach (KeyValuePair<string, object> pair in defaultValues) {
					if (!matchedValues.ContainsKey( pair.Key )) {
						matchedValues.Add( pair.Key, pair.Value );
					}
				}
			}
			return matchedValues;
		}

		private void MatchCatchAll( ContentPathSegment contentPathSegment, IEnumerable<string> remainingRequestSegments, RouteValueDictionary defaultValues, RouteValueDictionary matchedValues )
		{
			object obj2;
			string str = string.Join( string.Empty, remainingRequestSegments.ToArray<string>() );
			ParameterSubsegment subsegment = contentPathSegment.Subsegments[0] as ParameterSubsegment;
			if (str.Length > 0) {
				obj2 = str;
			} else {
				defaultValues.TryGetValue( subsegment.ParameterName, out obj2 );
			}
			matchedValues.Add( subsegment.ParameterName, obj2 );
		}

		private bool MatchContentPathSegment( ContentPathSegment routeSegment, string requestPathSegment, RouteValueDictionary defaultValues, RouteValueDictionary matchedValues )
		{
			if (string.IsNullOrEmpty( requestPathSegment )) {

				if (routeSegment.Subsegments.Count <= 1) {
					object obj2;
					ParameterSubsegment subsegment = routeSegment.Subsegments[0] as ParameterSubsegment;
					if (subsegment == null) {
						return false;
					}
					if (defaultValues.TryGetValue( subsegment.ParameterName, out obj2 )) {
						matchedValues.Add( subsegment.ParameterName, obj2 );
						return true;
					}
				}
				return false;
			}
			int length = requestPathSegment.Length;
			int num2 = routeSegment.Subsegments.Count - 1;
			ParameterSubsegment subsegment2 = null;
			LiteralSubsegment subsegment3 = null;
			while (num2 >= 0) {
				int num3 = length;
				ParameterSubsegment subsegment4 = routeSegment.Subsegments[num2] as ParameterSubsegment;
				if (subsegment4 != null) {
					subsegment2 = subsegment4;
				} else {
					LiteralSubsegment subsegment5 = routeSegment.Subsegments[num2] as LiteralSubsegment;
					if (subsegment5 != null) {
						subsegment3 = subsegment5;
						int num4 = requestPathSegment.LastIndexOf( subsegment5.Literal, length - 1, StringComparison.OrdinalIgnoreCase );
						if (num4 == -1) {
							return false;
						}
						if ((num2 == (routeSegment.Subsegments.Count - 1)) && ((num4 + subsegment5.Literal.Length) != requestPathSegment.Length)) {
							return false;
						}
						num3 = num4;
					}
				}
				if ((subsegment2 != null) && (((subsegment3 != null) && (subsegment4 == null)) || (num2 == 0))) {
					int num5;
					int num6;
					if (subsegment3 == null) {
						if (num2 == 0) {
							num5 = 0;
						} else {
							num5 = num3 + subsegment3.Literal.Length;
						}
						num6 = length;
					} else if ((num2 == 0) && (subsegment4 != null)) {
						num5 = 0;
						num6 = length;
					} else {
						num5 = num3 + subsegment3.Literal.Length;
						num6 = length - num5;
					}
					string str = requestPathSegment.Substring( num5, num6 );
					if (string.IsNullOrEmpty( str )) {
						return false;
					}
					matchedValues.Add( subsegment2.ParameterName, str );
					subsegment2 = null;
					subsegment3 = null;
				}
				length = num3;
				num2--;
			}
			if (length != 0) {
				return (routeSegment.Subsegments[0] is ParameterSubsegment);
			}
			return true;
		}

		private static bool RoutePartsEqual( object a, object b )
		{
			string str = a as string;
			string str2 = b as string;
			if ((str != null) && (str2 != null)) {
				return string.Equals( str, str2, StringComparison.OrdinalIgnoreCase );
			}
			if ((a != null) && (b != null)) {
				return a.Equals( b );
			}
			return (a == b);
		}


		public IList<PathSegment> PathSegments { get; private set; }
	}
}
