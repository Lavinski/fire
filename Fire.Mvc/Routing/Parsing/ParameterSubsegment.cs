﻿namespace Fire.Mvc.Routing.Parsing
{
	using System;

	public class ParameterSubsegment : PathSubsegment
	{

		public bool IsCatchAll { get; set; }
		public string ParameterName { get; set; }

		// Methods
		public ParameterSubsegment(string parameterName)
		{
			if (parameterName.StartsWith("*", StringComparison.Ordinal))
			{
				this.ParameterName = parameterName.Substring(1);
				this.IsCatchAll = true;
			}
			else
			{
				this.ParameterName = parameterName;
			}
		}

	}

}