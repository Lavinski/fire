﻿namespace Fire.Mvc.Routing.Parsing
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	public class ContentPathSegment : PathSegment
	{
		// Methods
		public ContentPathSegment(IList<PathSubsegment> subsegments)
		{
			this.Subsegments = subsegments;
		}

		// Properties
		public bool IsCatchAll
		{
			get
			{
				return this.Subsegments.Any<PathSubsegment>(delegate (PathSubsegment seg) {
					return ((seg is ParameterSubsegment) && ((ParameterSubsegment) seg).IsCatchAll);
				});
			}
		}

		public IList<PathSubsegment> Subsegments { get; set; }
	}

}