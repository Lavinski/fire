﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire
{
	public class Pair<T1, T2>
	{
		public Pair(T1 itemA, T2 itemB)
		{
			ItemA = itemA;
			ItemB = itemB;
		}

		public T1 ItemA { get; set; }
		public T2 ItemB { get; set; }
	}
}
