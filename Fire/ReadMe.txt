﻿Only add to the Fire if you have a corresponding unit test.
Only commit to this project if you're tests pass.
Please make sure you provide xmldoc comments for at minimum all public method summaries.
