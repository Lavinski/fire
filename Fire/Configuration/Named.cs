﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Configuration
{
	[AttributeUsage( AttributeTargets.Property, AllowMultiple = false )]
	public class Named : Attribute
	{
		public Named(string name)
		{ 
			this.Name = name;
		}

		public string Name { get; set; }
	}
}
