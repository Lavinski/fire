﻿using System;
using System.Linq;
using System.Reflection;
using Fire.Extensions;

namespace Fire.Configuration
{
	public static class Inflate
	{
		public static void Static( Type type, Func<string, string> dictionary )
		{
			Fill( null, type, dictionary );
		}

		public static void Instance( object instance, Func<string, string> dictionary )
		{
			Fill( instance, instance.GetType(), dictionary );
		}


		private static void Fill( object instance, Type type, Func<string, string> dictionary ) 
		{

			PropertyInfo[] properties;
			if (instance == null) {
				
				// Static
				properties = type.GetProperties( BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly );
			} else {
				
				// Instance
				properties = type.GetProperties( BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly );
			}

			// Get app settings and convert
			foreach (PropertyInfo property in properties) {
				var attributes = property.GetCustomAttributes( true );
				if (!attributes.Any( x => x is Ignore )) {

					var named = attributes.FirstOrDefault( x => x is Named ) as Named;

					var value = dictionary((named != null)? named.Name : property.Name);

					object result;
					if (ExtendConversion.ConvertTo(value, property.PropertyType, out result)) {
						property.SetValue( instance, result, null );
					}
				}
			}
		}
	}
}
