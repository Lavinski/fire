﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Fire.Converters
{
	public static class StreamConverter
	{
		public static Stream ToStream(this string value)
		{
			return new MemoryStream(Encoding.Default.GetBytes(value));
		}

		public static Stream ToStream(this string value, Encoding encoding)
		{
			return new MemoryStream(encoding.GetBytes(value));
		}
	}
}
