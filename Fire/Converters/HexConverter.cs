﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Converters
{
	public static class HexConverter
	{
		public static string BytesToHexString( byte[] value, int startIndex, int length )
		{
			if (value == null) {
				throw new ArgumentNullException( "byteArray" );
			}

			int num = value.Length;
			if ((startIndex < 0) || ((startIndex >= num) && (startIndex > 0))) {
				throw new ArgumentOutOfRangeException( "startIndex", "ArgumentOutOfRange_StartIndex" );
			}

			int num2 = length;
			if (num2 < 0) {
				throw new ArgumentOutOfRangeException( "length", "ArgumentOutOfRange_GenericPositive" );
			}

			if (startIndex > (num - num2)) {
				throw new ArgumentException( "Arg_ArrayPlusOffTooSmall" );
			}

			if (num2 == 0) {
				return string.Empty;
			}

			if (num2 > 0x2aaaaaaa) {
				throw new ArgumentOutOfRangeException( "length", "ArgumentOutOfRange_LengthTooLarge" /*, new object[] { 0x2aaaaaaa }*/ );
			}

			int num3 = num2 * 2;
			char[] chArray = new char[num3];
			int index = 0;
			int num5 = startIndex;
			for (index = 0; index < num3; index += 2) {
				byte num6 = value[num5++];
				chArray[index] = GetHexValue( num6 / 0x10 );
				chArray[index + 1] = GetHexValue( num6 % 0x10 );
			}
			return new string( chArray );
		}

		private static char GetHexValue( int i )
		{
			if (i < 10) {
				return (char)(i + 0x30);
			}
			return (char)((i - 10) + 0x41);
		}
	}
}
