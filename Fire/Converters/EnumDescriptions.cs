﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Globalization;

using Fire.Extensions;

namespace Fire.Converters
{
	/// <summary>
	/// Usage: apply the attribute [TypeConverter(typeof(EnumDescriptions))] to the supported enum
	/// </summary>
	public class EnumDescriptions : TypeConverter
	{
		public override bool CanConvertFrom( ITypeDescriptorContext context, Type sourceType )
		{
			return (sourceType.Equals( typeof( Enum ) ) || base.CanConvertFrom( context, sourceType ));
		}

		public override bool CanConvertTo( ITypeDescriptorContext context, Type destinationType )
		{
			return (destinationType.Equals( typeof( String ) ) || base.CanConvertTo( context, destinationType ));
		}

		public override object ConvertFrom( ITypeDescriptorContext context, CultureInfo culture, object value )
		{
			return base.ConvertFrom( context, culture, value );
		}

		public override object ConvertTo( ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType )
		{
			if (destinationType.Equals( typeof( String ) ) &&
				value.GetType().BaseType.Equals( typeof( Enum ) )) {

				string name = value.ToString();
				object[] attrs = value.GetType().GetField( name ).GetCustomAttributes( typeof( DescriptionAttribute ), false );
				return (attrs.Length > 0) ? ((DescriptionAttribute)attrs[0]).Description : name;
			}

			return base.ConvertTo( context, culture, value, destinationType );
		}
	}

}
