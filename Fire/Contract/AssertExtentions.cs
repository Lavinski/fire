﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fire.Extensions;

namespace Fire.Contract
{
	public static class AssertExtentions
	{
		public static void Require(this object item, string name)
		{
			if (item == null) {
				throw new ArgumentException(string.Format(ExceptionMessages.ContractRequired, name));
			}
		}
	}
}
