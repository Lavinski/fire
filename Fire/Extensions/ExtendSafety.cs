﻿namespace Fire.Extensions.Safety
{
	using System;
	using System.Data;
	using System.Configuration;
	using System.Linq;
	using System.Web;

	/// <summary>
	/// Summary description for Safety
	/// </summary>
	public static class ExtendSafety
	{

		public static void ThrowIfNull<T>( this T instance )
		{
			if (instance == null) {
				throw new ArgumentException( string.Format( "Instance of {0} was null", typeof( T ).Name ) );
			}
		}

		/* As : for use with objects */

		public static Result As<T, Result>( this object testType, Func<T, Result> func )
			where T : class
		{
			if (testType is T) {
				return func( testType as T );
			}
			return default( Result );
		}

		public static Result As<T, Result>( this object testType, Func<T, Result> func, Result defaultVal )
			where T : class
		{
			if (testType is T) {
				return func( testType as T );
			}
			return defaultVal;
		}

		public static string As<T, Y>( this object testType, Func<T, string> funcT, Func<Y, string> funcY )
			where T : class
			where Y : class
		{
			if (testType is T) {
				return funcT( testType as T );
			} else if (testType is Y) {
				return funcY( testType as Y );
			}
			return null;
		}

		/* No Null : for use with objects */

		public static bool NotNull<T>( this T target )
			where T : class
		{
			return (target != null);
		}

		public static void NotNull<T>( this T target, Action<T> action )
			where T : class
		{
			if (target != null) {
				action( target );
			}
		}

		public static TResult NotNull<T, TResult>( this T target, Func<T, TResult> func )
			where T : class
		{
			if (target != null) {
				return func( target );
			} else {
				return default( TResult );
			}
		}

		public static TResult NotNull<T, TResult>( this T target, Func<T, TResult> func, Func<TResult> elseFunc )
			where T : class
		{
			if (target != null) {
				return func( target );
			} else {
				return elseFunc();
			}
		}

		public static TResult NotNull<T, TResult>( this T target, Func<T, TResult> func, TResult value )
			where T : class
		{
			if (target != null) {
				return func( target );
			} else {
				return value;
			}
		}

		/* Not Zero : For use with strings */

		public static bool NotZero( this string target )
		{
			return (target != null && target.Length > 0);
		}

		public static void NotZero( this string target, Action<string> work )
		{
			if (target != null && target.Length > 0) {
				work( target );
			}
		}

		public static void NotZero( this string target, Action<string> action, Action elseAction )
		{
			if (target != null && target.Length > 0) {
				action( target );
			} else {
				elseAction();
			}
		}

		public static T NotZero<T>( this string target, Func<string, T> work )
		{
			if (target != null && target.Length > 0) {
				return work( target );
			}
			return default( T );
		}

		public static T NotZero<T>( this string target, Func<string, T> action, Func<T> elseAction )
		{
			if (target != null && target.Length > 0) {
				return action( target );
			} else {
				return elseAction();
			}
		}

		public static T NotZero<T>( this string target, Func<string, T> action, T elseVal )
		{
			if (target != null && target.Length > 0) {
				return action( target );
			} else {
				return elseVal;
			}
		}

		/* Is Safe : For use with Nullable<T> objects */

		public static bool IsSafe<T>( this Nullable<T> target )
			where T : struct
		{
			return (target != null && target.HasValue);
		}

		public static void IsSafe<T>( this Nullable<T> target, Action<T> action )
			where T : struct
		{
			if (target != null && target.HasValue) {
				action( target.Value );
			}
		}

		public static void IsSafe<T>( this Nullable<T> target, Action<T> action, Action notsafe )
			where T : struct
		{
			if (target != null && target.HasValue) {
				action( target.Value );
			} else {
				notsafe();
			}
		}

		public static R IsSafe<T, R>( this Nullable<T> target, Func<T, R> func, Func<R> notsafe )
		where T : struct
		{
			if (target != null && target.HasValue) {
				return func( target.Value );
			} else {
				return notsafe();
			}
		}

		public static R IsSafe<T, R>( this Nullable<T> target, Func<T, R> func, R notsafe )
			where T : struct
		{
			if (target != null && target.HasValue) {
				return func( target.Value );
			} else {
				return notsafe;
			}
		}

		public static R IsSafe<T, R>( this Nullable<T> target, Func<T, R> func )
			where T : struct
		{
			if (target != null && target.HasValue) {
				return func( target.Value );
			} else {
				return default(R);
			}
		}

		/* Nullable */
		public static Nullable<T> Nullable<T>( this T target )
			where T : struct
		{
			return new Nullable<T>( target );
		}

	}
}