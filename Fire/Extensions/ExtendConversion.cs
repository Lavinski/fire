﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Fire.Extensions
{
	public static class ExtendConversion
	{
		/// <summary> Parses a Boolean </summary>
		public static bool ToBool( this string target )
		{
			return (target != null && target.Length == 4 && target.ToLower() == "true");
		}

		/// <summary> Parses an Integer </summary>
		public static int ToInt( this string target )
		{
			int result;
			if (target != null && int.TryParse( target, out result )) {
				return result;
			}
			return 0;
		}

		/// <summary> Parses a DateTime </summary>
		public static DateTime ToDateTime( this string target )
		{
			if (target != null) {
				DateTime date;
				if (DateTime.TryParse( target, out date )) {
					return date;
				}
			}
			return default(DateTime);
		}

		/// <summary> Parses an Enum </summary>
		public static Nullable<TResult> ToEnum<TResult>( this string target )
			where TResult : struct
		{
			Nullable<TResult> result = null;
			try {
				result = new Nullable<TResult>( (TResult)Enum.Parse( typeof( TResult ), target ) );
			} catch (Exception) {
				result = new Nullable<TResult>();
			}

			return result;
		}

		/// <summary> Parses anything </summary>
		public static bool ConvertTo( this string value, Type type, out object result )
		{
			result = null;

			if (type.IsEnum) {
				try {
					result = Convert.ChangeType( value, type );
					return true;

				} catch (Exception) {
					return false;
				}
			}

			TypeConverter tc = TypeDescriptor.GetConverter( type );
			if (tc == null) {
				return false;
			}

			if (tc.CanConvertFrom( typeof( string ) )) {
				try {
					result = tc.ConvertFromString( value );
					return true;
				} catch {
				}
			}
			return false;
		}

		/// <summary> Parses anything </summary>
		public static bool ConvertTo<T>( this string value, out T result )
		{
			result = default( T );

			if (typeof( T ).IsEnum) {
				try {
					result = (T)Convert.ChangeType( value, typeof( T ) );
					return true;

				} catch (Exception) {
					return false;

				}
			}

			TypeConverter tc = TypeDescriptor.GetConverter( typeof( T ) );
			if (tc == null) {
				return false;
			}

			if (tc.CanConvertFrom( typeof( string ) )) {
				try {
					result = (T)tc.ConvertFromString( value );
					return true;
				} catch {
				}
			}
			return false;
		}

		/// <summary> Parses anything </summary>
		public static T ConvertTo<T>( this string value )
		{
			T result;
			if (value.ConvertTo( out result )) {
				return result;
			} else {
				return default( T );
			}
		}

	}
}
