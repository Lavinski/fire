﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Extensions
{
	public class ExtendGenerics
	{
		/// <summary>
		/// Creates a generic type through reflection
		/// </summary>
		/// <typeparam name="T">Return type</typeparam>
		/// <param name="generic">Generic type</param>
		/// <param name="innerType">Inner type</param>
		/// <param name="args">Constructor arguments</param>
		/// <returns>T</returns>
		public static T CreateGeneric<T>( Type generic, Type innerType, params object[] args )
			where T : class
		{
			System.Type specificType = generic.MakeGenericType( new System.Type[] { innerType } );
			return (T)Activator.CreateInstance( specificType, args );
		}

	}
}
