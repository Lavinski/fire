﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using Fire.Cryptography;

namespace Fire.Extensions.Crytography
{

	public static class ExtendCrytography
	{
		//cspp.Flags = CspProviderFlags.UseMachineKeyStore;

		/// <summary>
		/// Encrypts a string using the supplied key. Encoding is done using RSA encryption.
		/// </summary>
		/// <param name="stringToEncrypt">String that must be encrypted.</param>
		/// <param name="key">Encryption key.</param>
		/// <returns>A string representing a byte array separated by a minus sign.</returns>
		/// <exception cref="ArgumentException">Occurs when stringToEncrypt or key is null or empty.</exception>
		public static string Encrypt(this string stringToEncrypt, string key)
		{
			if (string.IsNullOrEmpty(stringToEncrypt)) {
				throw new ArgumentException("An empty string value cannot be encrypted.");
			}

			if (string.IsNullOrEmpty(key)) {
				throw new ArgumentException("Cannot encrypt using an empty key. Please supply an encryption key.");
			}

			return SimpleAES.GenerateEncryptor( key ).Encrypt( stringToEncrypt );
		}

		/// <summary>
		/// Decrypts a string using the supplied key. Decoding is done using RSA encryption.
		/// </summary>
		/// <param name="stringToDecrypt">String that must be decrypted.</param>
		/// <param name="key">Decryption key.</param>
		/// <returns>The decrypted string or null if decryption failed.</returns>
		/// <exception cref="ArgumentException">Occurs when stringToDecrypt or key is null or empty.</exception>
		public static string Decrypt(this string stringToDecrypt, string key)
		{
			if (string.IsNullOrEmpty(stringToDecrypt)) {
				throw new ArgumentException("An empty string value cannot be encrypted.");
			}

			if (string.IsNullOrEmpty(key)) {
				throw new ArgumentException("Cannot decrypt using an empty key. Please supply a decryption key.");
			}

			return SimpleAES.GenerateDecryptor( key ).Decrypt( stringToDecrypt );
		}


	}
}
