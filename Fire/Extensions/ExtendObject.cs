﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;

namespace Fire.Extensions
{
	public static class ExtendObject
	{
		/// <summary>
		/// Tries to create a phrase string from CamelCase text.
		/// </summary>
		public static string FromCamelCase( this object value )
		{
			if (value == null) {
				return null;
			}

			return ExtendString.FromCamelCase( value.ToString() );
		}

		/// <summary> Gets property name </summary>
		public static string GetPropertyName<T, O>( this O obj, Expression<Func<O, T>> expression )
		{
			var member = expression.Body as MemberExpression;
			if (member != null) {

				var property = (member.Member as PropertyInfo);
				if (property != null) {
					return property.Name;
				}
			}
			throw new NullReferenceException("Expression body or body member are null");
		}

		public class PropertyDetails
		{
			public PropertyDetails(Type type, string name)
			{
				Type = type;
				Name = name;
			}

			public Type Type { get; internal set; }
			public string Name { get; internal set; }
		}

		/// <summary> Gets the property details </summary>
		public static PropertyDetails GetPropertyDetail<T, O>( this O obj, Expression<Func<O, T>> expression )
		{
			var member = expression.Body as MemberExpression;
			if (member != null) {

				var property = (member.Member as PropertyInfo);
				if (property != null) {
					return new PropertyDetails( property.PropertyType, property.Name );
				}
			}
			throw new NullReferenceException( "Expression body or body member are null" );
		}
	}
}
