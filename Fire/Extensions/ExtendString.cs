﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RegularEx = System.Text.RegularExpressions.Regex;

namespace Fire.Extensions
{
	public static class ExtendString
	{
		private static readonly RegularEx whiteSpace = new RegularEx( @"\s+" );

		/// <summary>
		/// Enable quick and more natural string.Format calls   
		/// </summary>
		public static string Format( this string value, params object[] args )
		{
			return string.Format( value, args );
		}

		/// <summary>
		/// Matches a whitespace group and replaces it with a single space
		/// </summary>
		public static string StripExcessWhitespace( this string value )
		{
			return whiteSpace.Replace( value, " " );
		}

		/// <summary>
		/// Ensures the first character of the string is uppercase
		/// </summary>
		public static string Capitalise( this string value )
		{
			if (value != null && value.Length > 0) {
				return char.ToUpper( value[0] ) + value.Substring( 1 );
			} else {
				return value;
			}
		}

		/// <summary>
		/// Checks a string for whitespace working from each
		/// end towards the middle.
		/// </summary>
		/// <param name="target">String to test</param>
		/// <returns>True if non whitespace detected</returns>
		public static bool IsWhiteSpace( this string target )
		{
			if (target == null || target.Length == 0) {
				return true;
			}

			var lenBound = target.Length - 1;
			var lenHalf = ((target.Length + 1) / 2);

			// Test from each end
			for (int i = 0; i < lenHalf; i++) {
				if (!char.IsWhiteSpace( target[i] ) ||
					!char.IsWhiteSpace( target[lenBound - i] )) {
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// Tries to create a phrase string from CamelCase text.
		/// Can handle uppercase groups, dates, numbers and spaces
		/// </summary>
		public static string FromCamelCase( this string value )
		{
			if (value != null && value.Length > 1) {

				StringBuilder sb = new StringBuilder( value.Length + 5 );

				var first = true;
				var wasSpace = false;
				var charBefore = '\0';
				var charCurrent = '\0';
				var charAfter = value[0];
				var lengthMinusOne = value.Length - 1;

				int charIndex = 0;
				for (; charIndex < lengthMinusOne; charIndex++) {
					// Pre
					charCurrent = charAfter;
					charAfter = value[charIndex + 1];

					// Action

					if (!first &&
						!wasSpace &&
						!char.IsWhiteSpace( charCurrent ) &&
						(char.IsUpper( charCurrent ) && char.IsLower( charAfter ) || // Start of Word
						 char.IsDigit( charCurrent ) && char.IsLetter( charBefore ) // Letter to Number
						)) {

						sb.Append( ' ' );
					}

					sb.Append( charCurrent );
					wasSpace = char.IsWhiteSpace( charCurrent );
					first = false;

					// Post
					charBefore = charCurrent;
				}

				sb.Append( value[charIndex] );
				return sb.ToString();

			} else {
				return value;
			}
		}

	}
}

