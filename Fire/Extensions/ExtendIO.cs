﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Fire.Extensions.IO
{

	public static class ExtendIO
	{
		/// <summary>Transforms a relative path to an absolute path, also resolves seek(dir)</summary>
		/// <param name="path">The relative path</param>
		/// <param name="basePath">The absolute base path</param>
		/// <returns>Absolute path for the original relative path</returns>
		public static string ResolvePath( this string path, string basePath )
		{
			if (string.IsNullOrEmpty(path)) {
				throw new ArgumentException("Cannot be null or empty", path);

			} else if (string.IsNullOrEmpty(basePath)) {
				throw new ArgumentException("Cannot be null or empty", basePath);

			} else if (!Path.IsPathRooted( basePath )) {
				throw new ArgumentException("Path must be absolute", basePath);
			}

			string absolutePath = null;
			var index = -1;
			var token = "seek(";
			var lowerBasePath = basePath.ToLowerInvariant();
			var lowerPath = path.ToLowerInvariant();
			var hasSeek = (index = lowerPath.IndexOf( token )) != -1;

			if (hasSeek && index != 0) {
				throw new Exception("seek(dir) must be the first part in the path");

			} else if (hasSeek) {

				var end = lowerPath.IndexOf( ')' );
				string relPath = null;

				if (end == -1 ) {
					throw new ArgumentException( string.Format( "Invalid path: {0}", lowerPath ) );

				} else if (end < (lowerPath.Length - 1)) {

					var cChar = lowerPath[end + 1];
					var loc = lowerPath.Substring( end + ((cChar == '/' || cChar == '\\')? 2 : 1) );
					relPath = loc;
				}

				var seek = lowerPath.Substring( index + token.Length, end - token.Length );

				if ((index = lowerBasePath.LastIndexOf( seek )) != -1) {
					absolutePath = basePath.Substring( 0, index + seek.Length );
				} else if (Directory.GetDirectories(basePath).Any( x => (x == seek) ) ) {
					absolutePath = Path.Combine( basePath, seek );
				} else {
					throw new Exception( "Could not find the 'seek' directory" );
				}

				if (relPath != null) {
					absolutePath = Path.Combine( absolutePath, relPath );
				}

			} else {
				absolutePath =  Path.Combine( basePath, path );
			}

			return Path.GetFullPath(absolutePath);
		}

	}
}
