﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Extensions
{
	public static class ExtendEnum
	{
		public static string FromCamelCase( this Enum value )
		{
			return ExtendObject.FromCamelCase( value );
		}

		public static IEnumerable<string> GetFormattedNames( this Enum value )
		{
			var names = Enum.GetNames( value.GetType() );

			for (int index = 0; index < names.Length; index++) {
				names[index] = names[index].FromCamelCase();
			}

			return names;
		}

		public static IEnumerable<T> GetValues<T>()
		{
			var values = Enum.GetValues( typeof(T) );
			return values.Cast<T>();
		}
	}

}
