﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Extensions
{
	public static class ExtendInteger
	{
		public static string Ordinal( this int number )
		{
			int ones = number % 10;
			int tens = (int)Math.Floor( number / 10M ) % 10;

			if (tens == 1) {
				return "th";
			} else {
				switch (ones) {
					case 1:
						return "st";
					case 2:
						return "nd";
					case 3:
						return "rd";
					default:
						return "th";
				}
			}
		}

	}
}
