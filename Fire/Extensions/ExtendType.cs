﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Extensions
{
	public static class ExtendType
	{
		/// <summary>
		/// This method gets the base type of a proxy
		/// </summary>
		public static Type RootType( this Type target )
		{
			var cType = target;

			while (cType.BaseType != typeof( object ) && cType.BaseType != null) {
				cType = cType.BaseType;
			}
			return cType;
		}
	}
}
