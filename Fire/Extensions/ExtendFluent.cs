﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Fire.Extensions
{
	public static class ExtendFluent
	{
		/// <summary>This is a helper for to easily create a new fluent object </summary>
		/// <typeparam name="T">Type of the fluent object</typeparam>
		/// <param name="target">Instance to test</param>
		/// <returns>A Fluent capable instance</returns>
		public static Fluent<T, T> AsFluent<T>( this T target )
		{
			return new Fluent<T, T>( default( T ), target );
		}
	}
}
