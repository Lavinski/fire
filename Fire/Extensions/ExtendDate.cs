﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Extensions
{
	public static class ExtendDate
	{
		/// <summary>
		/// Gets the start of the current day at 0000/12:00AM
		/// </summary>
		public static DateTime GetDayStart( this DateTime time )
		{
			return time.Add( -time.TimeOfDay );
		}

		/// <summary>
		/// Gets the end of the current day at 2359/11:59PM
		/// </summary>
		public static DateTime GetDayEnd( this DateTime time )
		{
			return time.Add( new TimeSpan(0, 23, 59, 59) - time.TimeOfDay );
		}

		/// <summary>
		/// Ajusts the day of the value to match the day of the week and week number in the start date
		/// </summary>
		/// <param name="time">Date to find the day in</param>
		/// <param name="startDate">Date to base the day in month off</param>
		public static DateTime GetWeekdayInMonth( this DateTime time, DateTime startDate )
		{
			var weekdayOfFirst = new DateTime(time.Year, time.Month, 1).DayOfWeek;
			var daysInMonth = DateTime.DaysInMonth(time.Year, time.Month);

			// Wednesday of Week 1 etc.
			var weekday = startDate.DayOfWeek;
			var daysOff = (int)weekdayOfFirst;
			int occurenceWeek = (startDate.Day + daysOff - 1) / 7; // 0 based
			
			// If there is no Xday in the first week add a week
			if (weekdayOfFirst > weekday && occurenceWeek == 0) {
				occurenceWeek++;
			}

			// Find the day given the Week and Weekday

			// Day to check is the default day in the occurrence week
			var dayToCheck = (occurenceWeek * 7) + 1;
			if (dayToCheck > daysInMonth) {
				dayToCheck = daysInMonth;
			}

			// Shift the day
			var dayWeekday = new DateTime(time.Year, time.Month, dayToCheck).DayOfWeek;
			var diff = weekday - dayWeekday;
			var finalDay = dayToCheck + diff;

			// Ensure it occurs in the current month
			if (finalDay > daysInMonth) {
				finalDay -= 7;
			}
			
			DateTime value = new DateTime( time.Year, time.Month, finalDay );
			return value;
		}


		/// <summary>
		/// Gets a list of dates where the day of the month and the day of the week match certain values inside a time range.
		/// </summary>
		public static IList<DateTime> GetDateWhereEqual( Place dayOfMonth, DayOfWeek dayOfWeek, DateTime startRange, DateTime endRange )
		{
			if (endRange < startRange) {
				throw new Exception( string.Format("Range starting value '{0}' must be greater then end of range '{1}'", startRange, endRange ) );
			}

			var results = new List<DateTime>();

			int year = startRange.Year;
			int month = startRange.Month;
			DateTime testMonth;

			do {
				testMonth = new DateTime( year, month, 1 );

				if (dayOfMonth == Place.Last) {
					testMonth = testMonth.AddDays( DateTime.DaysInMonth( year, month ) - 1 );

				} else if (dayOfMonth != Place.First) {
					testMonth = testMonth.AddDays( (int)dayOfMonth );
				}

				if (testMonth.DayOfWeek == dayOfWeek) {
					results.Add( testMonth );
				}

				if (month == 12) {
					year++;
					month = 1;
				} else {
					month++;
				}


			} while (testMonth < endRange);


			return results;
		}

		/// <summary>
		/// Tests if the date is a weekday
		/// </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		public static bool IsWeekday( this DateTime time )
		{
			return (time.DayOfWeek != DayOfWeek.Sunday && time.DayOfWeek != DayOfWeek.Saturday);
		}

		/// <summary>
		/// Gets the closest weekday inside the values month
		/// </summary>
		public static DateTime GetClosestWeekdayInMonth( this DateTime time )
		{
			var isSat = time.DayOfWeek == DayOfWeek.Saturday;
			var isSun = time.DayOfWeek == DayOfWeek.Sunday;

			if (isSat || isSun) {
				if (time.Day == 1) {
					if (isSat) {
						return time.AddDays( 2 );
					} else {
						return time.AddDays( 1 );
					}
				} else if (time.Day == DateTime.DaysInMonth( time.Year, time.Month )) {
					if (isSat) {
						return time.AddDays( -1 );
					} else {
						return time.AddDays( -2 );
					}
				} else {
					if (isSat) {
						return time.AddDays( -1 );
					} else {
						return time.AddDays( 1 );
					}
				}
			}

			return time;
		}

		/// <summary>
		/// Gets the closest weekday without regard for month boundaries
		/// </summary>
		public static DateTime GetClosestWeekday( this DateTime time )
		{
			if (time.DayOfWeek == DayOfWeek.Saturday) {
				return time.AddDays( -1 );

			} else if (time.DayOfWeek == DayOfWeek.Sunday) {
				return time.AddDays( 1 );

			} else {
				return time;
			}
		}

		/// <summary>
		/// Gets the next weekday after the value without regard for month boundaries
		/// </summary>
		public static DateTime GetNextWeekday( this DateTime time )
		{
			//var newTime = time.GetDayStart();
			if (time.DayOfWeek == DayOfWeek.Saturday) {
				return time.AddDays( 2 );

			} else if (time.DayOfWeek == DayOfWeek.Sunday) {
				return time.AddDays( 1 );

			} else {
				return time;
			}
		}

		/// <summary>
		/// Gets the first day of the values month
		/// </summary>
		public static DateTime GetFirstDayOfMonth( this DateTime time )
		{
			return time.GetDayStart().AddDays( 1 - time.Day );
		}

		/// <summary>
		/// Gets the first day of the values month
		/// </summary>
		public static DateTime GetPlaceDayOfMonth( this DateTime time, Place place )
		{
			if (place == Place.Last) {
				return GetLastDayOfMonth( time );
			} else {
				return time.GetDayStart().AddDays( (1 + (int)place) - time.Day );
			}
		}

		/// <summary>
		/// Gets the last day of the values month
		/// </summary>
		public static DateTime GetLastDayOfMonth( this DateTime time )
		{
			return time.GetDayStart().AddDays( DateTime.DaysInMonth(time.Year, time.Month) - time.Day  );
		}

		/// <summary>
		/// Gets the place occurence that matches the weekday
		/// </summary>
		public static DateTime GetPlaceInMonth( this DateTime time, Place place, Weekday weekday )
		{
			DateTime currentDay;
			int step = (place == Place.Last) ? -1 : 1;
			int days = DateTime.DaysInMonth( time.Year, time.Month );

			if (place == Place.Last) {
				currentDay = time.GetLastDayOfMonth();
			} else {
				currentDay = time.GetFirstDayOfMonth();
			}

			int count = 0;
			bool insideMonth = true;
			do {

				var cDOW = currentDay.DayOfWeek;
				if (weekday == Weekday.Day) {
					count++;
				} else if (weekday == Weekday.Weekday) {
					if (cDOW != DayOfWeek.Saturday && cDOW != DayOfWeek.Sunday) {
						count++;
					}
				} else if (weekday == Weekday.WeekendDay) {
					if (cDOW == DayOfWeek.Saturday || cDOW == DayOfWeek.Sunday) {
						count++;
					}

					// Normal days
				} else if ((int)cDOW == (int)weekday) {
					count++;
				}

				if ((place == Place.Last || place == Place.First) && count == 1) {
					return currentDay;
				} else if (count == ((int)place + 1)) {
					return currentDay;
				}

				int nextDay = currentDay.Day + step;
				if ((nextDay > 0) && (nextDay < days + 1)) {
					currentDay = currentDay.AddDays( step );
					insideMonth = true;

				}
			} while (insideMonth);

			return currentDay;
		}

		/// <summary>
		/// Gets the difference in months using the year and month date parts
		/// </summary>
		public static int MonthsDifference( this DateTime startDate, DateTime endDate )
		{
			return (12 * (endDate.Year - startDate.Year) + endDate.Month - startDate.Month);
		}

		/// <summary>
		/// Gets the difference in years using the date year part
		/// </summary>
		public static int YearsDifference( this DateTime startDate, DateTime endDate )
		{
			return (endDate.Year - startDate.Year);
		}

		/// <summary>
		/// Returns True if the date occurs after the expiry (ignores time)
		/// </summary>
		public static bool IsAfterDate( this DateTime date, DateTime expiry )
		{
			return (date.Date > expiry.Date);
		}

		/// <summary>
		/// Returns True if the date occurs after the expiry (ignores time)
		/// </summary>
		public static bool ReachedDate( this DateTime date, DateTime expiry )
		{
			return (date.Date >= expiry.Date);
		}

		/// <summary>
		/// Returns True if the date occurs before the expiry (ignores time)
		/// </summary>
		public static bool IsBeforeDate( this DateTime date, DateTime expiry )
		{
			return (date.Date < expiry.Date);
		}

		/// <summary>
		/// Gets the number of occurrences for each day of the week in a month
		/// </summary>
		public static IDictionary<DayOfWeek, int> DaysOfAMonth( this DateTime date ) {
			var result = new Dictionary<DayOfWeek, int>();

			var day = date.GetFirstDayOfMonth();
			do {

				var dayName = day.DayOfWeek;
				if (result.ContainsKey( dayName )) {
					result[dayName] = result[dayName] + 1;
				} else {
					result.Add( dayName, 1 );
				}

				day = day.AddDays( 1 );
			} while (day.Month == date.Month);

			return result;
		}
	}
}
