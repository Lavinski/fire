﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RegularEx = System.Text.RegularExpressions.Regex;

namespace Fire.Extensions
{
	public static class ExtendStringBuilder
	{
		
		/// <summary>
		/// Removes all text from a string builder
		/// </summary>
		public static void Clear( this StringBuilder target )
		{
			target.Remove( 0, target.Length );
		}

	}
}

