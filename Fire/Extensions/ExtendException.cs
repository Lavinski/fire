﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Extensions
{
	public static class ExtendException
	{
		private const string stack = "<div class='stack-trace'><pre>{0}</pre></div>";
		private const string row = "<div class='row'><span class='label'>{0}</span><span class='value'>{1}</span></div>";
		private const string beginGroup = "<div class='group'>";
		private const string endGroup = "</div>";

		/// <summary> Renders an exception to html </summary>
		public static string ToHtml( this Exception target) {

			return HtmlException( target, new StringBuilder()).ToString();
		}

		private static StringBuilder HtmlException( Exception target, StringBuilder htmlResult )
		{
			htmlResult.AppendLine( "<div class='fire'>" );

			// Message
			if (target.Message != null && target.Message.Length > 0) {
				htmlResult.AppendLine( beginGroup );

					htmlResult.AppendFormat( row, "Message", target.Message );

				htmlResult.AppendLine( endGroup );
			}

			// Link
			if (target.HelpLink != null && target.HelpLink.Length > 0) {
				htmlResult.AppendLine( beginGroup );

				htmlResult.AppendFormat( row, "HelpLink", target.HelpLink );

				htmlResult.AppendLine( endGroup );
			}

			// StackTrace
			if (target.StackTrace != null && target.StackTrace.Length > 0) {
				htmlResult.AppendLine( beginGroup );

				htmlResult.AppendFormat( stack, target.StackTrace );

				htmlResult.AppendLine( endGroup );
			}

			// Source
			if (target.Source != null && target.Source.Length > 0) {
				htmlResult.AppendLine( beginGroup );

				htmlResult.AppendFormat( stack, target.Source );

				htmlResult.AppendLine( endGroup );
			}

			// Data
			if (target.Data != null && target.Data.Count > 0) {
				htmlResult.AppendLine( beginGroup );

				foreach (var item in target.Data.Keys) {
					htmlResult.AppendFormat( row, item, target.Data[item] );
				}

				htmlResult.AppendLine( endGroup );
			}

			// InnerExceptions
			if (target.InnerException != null) {
				htmlResult.AppendLine( beginGroup );

				// Recursive
				HtmlException( target, htmlResult );

				htmlResult.AppendLine( endGroup );
			}

			htmlResult.AppendLine( "</div>" );

			return htmlResult;
		}
	}
}
