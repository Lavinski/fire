﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Extensions.Database
{
	public static class ExtendString
	{

		/// <summary>
		/// Tries to create a phrase string from CamelCase text.
		/// </summary>
		public static string Limit( this string value, int maxLength )
		{
			if (maxLength <= 0) {
				throw new Exception( string.Format( Fire.ExceptionMessages.GreaterThan, maxLength, 0 ) );

			} else if (value == null) {
				return null;

			} else if (value.Length > maxLength) {
				value = value.Substring( 0, maxLength );
			}

			return value;
		}
	}
}
