﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Security;

namespace Fire.Logging
{
	public class WindowsEventLogger : MarshalByRefObject, ILogger
	{

		private string assembilyName;
		private EventLog eventLog = new EventLog();
		private HashSet<LogLevel> enabledLevels;

		public WindowsEventLogger()
		{
			eventLog = new EventLog();
			enabledLevels = new HashSet<LogLevel>();
			assembilyName = this.GetType().Assembly.GetName().Name;

			EnableLevel( LogLevel.Info );
			EnableLevel( LogLevel.Warning );
			EnableLevel( LogLevel.Error );
			EnableLevel( LogLevel.FatalError );
		}

		public void EnableLevel( LogLevel level )
		{
			//if (!enabledLevels.Contains( level )) {
			enabledLevels.Add( level );
			//}
		}


		public void DisableLevel( LogLevel level )
		{
			//if (enabledLevels.Contains( level )) {
			enabledLevels.Remove( level );
			//}
		}

		public bool IsLevelEnabled( LogLevel level )
		{
			return enabledLevels.Contains( level );
		}

		/// <summary>
		/// Selects the lowest log level after debug that is enabled
		/// If none are enabled there is no result
		/// </summary>
		public void Log( object message, params object[] values )
		{
			for (int i = (int)LogLevel.Info; i < (int)LogLevel.FatalError; i++) {
				
				var level = (LogLevel)i;
				if (IsLevelEnabled( level )) {

					Log( level, message, values );	
					break;
				}
			}
		}

		public void Log( LogLevel level, object message, params object[] values )
		{
			var type = Fire.Extensions.ExtendFluent.AsFluent(level)
				.Switch<EventLogEntryType>()
				.Case(LogLevel.Info, EventLogEntryType.Information)
				.Case(LogLevel.Warning, EventLogEntryType.Warning)
				.Case(LogLevel.Error, EventLogEntryType.Error)
				.Case(LogLevel.FatalError, EventLogEntryType.FailureAudit)
				.Default();

			WriteEventLog( assembilyName, string.Format( message.ToString(), values ), type );

		}

		// Security attributes prevent the assembily from being loaded if the requests are not met.
		//[EventLogPermissionAttribute(System.Security.Permissions.SecurityAction.RequestOptional)]
		private void WriteEventLog( string sCallerName, string sLogLine, EventLogEntryType type )
		{
			try {
				//Assert permission
				EventLogPermission eventPerm = new EventLogPermission( EventLogPermissionAccess.Administer, Environment.MachineName );
				eventPerm.Assert();

				if (!System.Diagnostics.EventLog.SourceExists( sCallerName )) {
					System.Diagnostics.EventLog.CreateEventSource( sCallerName, "Application" );
				}

				eventLog.Source = sCallerName;
				eventLog.WriteEntry( sLogLine, type );

			} catch (SecurityException ex) {
				throw new Exception( @"The application does not have permission to perform this action, as HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\EventLog", ex );

			} catch (Exception ex) {
				throw new Exception( "The logger could not use the event log", ex );

			} finally {
				CodeAccessPermission.RevertAssert();
			}
		}

	}
}
