﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Logging
{
	public enum LogLevel : int
	{
		Debug = 0,
		Info = 1,
		Warning = 2,
		Error = 3,
		FatalError = 4
	}

	public interface ILogger
	{
		bool IsLevelEnabled( LogLevel level );
		void Log( object message, params object[] values );
		void Log( LogLevel level, object message, params object[] values );
	}
}
