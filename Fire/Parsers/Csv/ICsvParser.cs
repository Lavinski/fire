﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Fire.Parsers.Csv
{
	public delegate void LineHandler( IList<string> row, int number );

	public interface ICsvParser
	{
		event LineHandler LineParsed;

		int Parse( Stream input );
		int Parse( Stream input, char delimiter, bool trimWhiteSpace,  bool useQuotes, bool validateTable );
	}
}
