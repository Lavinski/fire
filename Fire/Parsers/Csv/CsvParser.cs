﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
namespace Fire.Parsers.Csv
{
	public class CsvParser : ICsvParser
	{
		enum LastCharacter
		{ 
			Normal,
			Delimiter,
			NewLine
		}


		public event LineHandler LineParsed;

		public int Parse( System.IO.Stream input )
		{
			return Parse( input, ',', false, false, false );
		}

		public int Parse( System.IO.Stream input, char delimiter, bool trimWhiteSpace, bool useQuotes, bool validateTable )
		{
			char value;
			int character;
			var reader = new StreamReader( input );
			var cell = new StringBuilder();
			var values = new List<string>();
			var columns = 4;
			var row = 0;
			var firstNewline = 0;
			var isNewline = false;
			var parsing = true;

			while (parsing) {

				character = reader.Read();
				if (character == -1) {

					parsing = false;
					isNewline = true;
					value = '\n';
				} else {
					value = (char)character;
					isNewline = (value == '\n' || value == '\r' || Environment.NewLine.IndexOf( value ) >= 0);
				}

				firstNewline = isNewline ? (firstNewline + 1) : 0;

				if (value == delimiter || isNewline && firstNewline <= 1) {
					// Add the cell value and clear the text

					var text = cell.ToString();
					if (useQuotes) {
						var firstIndex = text.IndexOf( '"' );

						// Validation check
						if (firstIndex >= 0 && firstIndex < text.Length - 1) {
							
							// Get the text between the quotes
							var secondIndex = text.IndexOf( '"', firstIndex + 1 );
							text = text.Substring( firstIndex + 1, secondIndex - firstIndex - 1);

						} else if (validateTable) {

							// Cell is missing a quote so we'll just return the original value
							// However if we're validating throw an error
							throw new Exception( string.Format("The value in the cell ({0}, {1}) is not properly quoted: {2}", values.Count, row, text));
						}
					}

					values.Add( trimWhiteSpace ? text.Trim() : text );
					cell.Remove( 0, cell.Length );

				}

				if (isNewline) {

					if (firstNewline <= 1) {

						if (row == 0) {
							// The first row should set the column width
							columns = values.Count;
						}

						if (validateTable && columns != values.Count) {
							throw new Exception( string.Format( "The row {0} only has {1} columns but should contain {2}", row, values.Count, columns ) );
						}
						
						// Row counter
						row++;

						if (LineParsed != null) {
							LineParsed( values, row );
						}

						// Start a new row
						values = new List<string>(columns);
					}

				} else if (value != delimiter) {
					// Add the next character
					cell.Append(value);
				}

			}

			return row;
		}


	}
}
