﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers
{
	public interface ILocator
	{
		bool Ended { get; }

		bool IsPart(char next);
		void Reset();
	}
}
