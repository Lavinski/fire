﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Fire.Parsers.Locators;
using Fire.Extensions;

namespace Fire.Parsers
{
	public class Parser
	{
		private int intChar;
		private char character;
		private int backLength;
		private bool clear;
		private StreamReader reader;
		private StringBuilder readResult;
		private StringBuilder locatorResult;
		private StringBuilder lastLocatorResult;
		

		public Parser(StreamReader reader)
		{
			this.clear = true;
			this.lastLocatorResult = new StringBuilder();
			this.readResult = new StringBuilder();
			this.locatorResult = new StringBuilder();
			this.reader = reader;
		}

		/// <summary>
		/// Gets the text that Back() reversed over
		/// </summary>
		public string Future { get {
			if (backLength > 0) {
				return lastLocatorResult.ToString( lastLocatorResult.Length - backLength, backLength );
			} else {
				return null;
			}
		} }

		/// <summary>
		/// Gets the result from the read calls
		/// </summary>
		public string Text { get {
			return readResult.ToString();
		} }

		/// <summary>
		/// Gets the length of the text
		/// </summary>
		public int Length { get {
			return readResult.Length;
		} }

		/// <summary>
		/// Emulates seek using the buffer, returns to the state 
		/// before the last match. Also effects the text value.
		/// </summary>
		public void Back()
		{
			lastLocatorResult.Clear();
			lastLocatorResult.Append(locatorResult.ToString());
			backLength = lastLocatorResult.Length;

			var start = readResult.Length - locatorResult.Length;
			if (start >= 0) { 
				readResult.Remove(start, locatorResult.Length);
			}
		}

		/// <summary>
		/// Move forward to the steam head (undo back)
		/// </summary>
		public void Forward()
		{
			backLength = 0;
		}

		public void Concat()
		{
			clear = false;
		}

		/// <summary>
		/// Reads the next character from the stream or buffer
		/// </summary>
		private bool Read(out char character)
		{
			if (backLength > 0) {
				character = lastLocatorResult[lastLocatorResult.Length - backLength];
				backLength -= 1;
				return true;
			}

			if ((intChar = reader.Read()) != -1) {
				character =  (char)intChar;
				return true;
			} else {
				character = '\0';
				return false;
			}
		}

		/// <summary>
		/// Read to adds to the next buffer until the locator matches.
		/// Calling back will move to the start of the locator match.
		/// </summary>
		public bool ReadTo(ILocator locator)
		{
			if (clear) {
				readResult.Clear();
			} else {
				clear = true;
			}
			bool clearLocator = true;

			locator.Reset();
			bool result = false;
			while(Read(out character)) {

				readResult.Append(character);

				// If a match is partial the result will specify
				if (locator.IsPart( character )) {

					if (clearLocator) { locatorResult.Clear(); }
					locatorResult.Append( character );

				} else {
					clearLocator = true;
				}

				if (locator.Ended) {
					return true;
				}

			}
			return result;
		}
		/// <summary>
		/// Read adds to the text buffer while the locator matches.
		/// Calling back will move to the start of the locator miss.
		/// </summary>
		/// <returns>If a full location was found or not</returns>
		public bool Read(ILocator locator)
		{
			if (clear) {
				readResult.Clear();
			} else {
				clear = true;
			}
			locatorResult.Clear();

			locator.Reset();
			bool result = false;
			while(Read(out character)) {

				// If a match is partial the result will specify
				if (locator.IsPart(character)) {
					readResult.Append(character);

				} else {
					result = locator.Ended;
					locatorResult.Append( character );
					break;
				}

				if (locator.Ended) {
					return true;
				}
			}
			return result;
		}

		/// <summary>
		/// Seek to moves through the stream until the locator matches.
		/// Calling back will move to the start of the locator match.
		/// </summary>
		public bool SeekTo(ILocator locator)
		{
			locator.Reset();
			bool clearLocator = true;

			bool result = false;
			while(Read(out character)) {

				// If a match is partial the result will specify
				if (locator.IsPart( character )) {

					if (clearLocator) { 
						locatorResult.Clear();
						clearLocator = false;
					}
					locatorResult.Append( character );

				} else {
					clearLocator = true;
				}

				if (locator.Ended) {
					return true;
				}

			}
			return result;
		}

		/// <summary>
		/// Seek moves through the stream while the locator matches.
		/// Calling back will move to the start of the locator miss.
		/// </summary>
		/// <returns>If a full location was found or not</returns>
		public bool Seek(ILocator locator)
		{
			locator.Reset();
			locatorResult.Clear();

			bool result = false;
			while(Read(out character)) {

				// If a match is partial the result will specify
				if (!locator.IsPart( character )) {

					locatorResult.Clear();
					locatorResult.Append( character );

					result = locator.Ended;
					break;
				}

				if (locator.Ended) {
					return true;
				}
			}
			return result;
		}

	}

}
