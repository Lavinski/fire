﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Sql
{
	public class Identifier
	{
		StringBuilder value;
		string text;

		public Identifier()
		{
			value = new StringBuilder();
			text = null;
		}

		public string Text {
			get {

				return (text == null) ? text = value.ToString() : text;
			}
		}
		public void AddPart(string part)
		{
			if (value.Length > 0) {
				value.Append( '.' );
			}
			value.Append( part );
		}
	}
}
