﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Sql
{
	public class Column
	{
		public string Name { get; set; }
		public DataType Type { get; set; }
		public string Default { get; set; }
		public bool Nullable { get; set; }
	}
}
