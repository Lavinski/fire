﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Sql
{
	public class DataType
	{
		public string Name { get; set; }
		public int? Size { get; set; }
		public int? Scale { get; set; }

			// Types with a size

			// char
			// nchar
			// varchar
			// nvarchar
			// binary
			// varbinary 

	}
}
