﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Sql
{
	public class Table
	{
		public Table()
		{
			Columns = new List<Column>();
			Constraints = new List<Constraint>();
		}

		public Identifier Identifier { get; set; }
		public IList<Column> Columns { get; set; }
		public IList<Constraint> Constraints { get; set; }
	}
}
