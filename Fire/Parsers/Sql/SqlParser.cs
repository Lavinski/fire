﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Fire.Validation;
using Fire.Parsers.Locators;

namespace Fire.Parsers.Sql
{
	public class SqlParser
	{
		protected ILocator whitespace = new WhiteSpace();
		protected ILocator nonWhitespace = new NonWhiteSpace();
		protected ILocator alpha = new AlphaLocator();
		protected ILocator squareLeft = new Character('[');
		protected ILocator squareRight = new Character(']');
		protected ILocator dot = new Character('.');
		protected ILocator comma = new Character(',');
		protected ILocator parenthesisLeft = new Character('(');
		protected ILocator parenthesisRight = new Character(')');
		protected ILocator tableItemName = new WhiteSpaceOrCharacter(')');
		protected ILocator numbers = new Numbers();


		protected Parser parser;

		private const int minNameLength = 3;

		public SqlParser(Stream textStream)
		{ 
			parser = new Parser(new StreamReader(textStream));
		}

		public Database Parse()
		{
			Database database = new Database();

			if (parser.Seek( whitespace )) {
				parser.Back();

				if (parser.Read( alpha )) {

					// Default error handler is to seek next ';'
					switch (parser.Text.ToLowerInvariant()) {
						case "create":
							Create(database);
							break;
					}
				}
			}
			return database;
		}

		public void Create(Database database)
		{
			if (parser.Seek( whitespace )) {
				parser.Back();

				if (parser.Read( alpha )) {
					parser.Back();

					switch (parser.Text.ToLowerInvariant()) {
						case "table":

							database.Tables.Add(ParseTable());
							break;

					}

				}
			}
		}

		/// <summary>
		/// After reading Create Table this method will be invoked
		/// this method will return after reading the
		/// ending semi semicolon (not the closing parentheses) or invalid sql 
		/// </summary>
		/// <returns></returns>
		protected Table ParseTable()
		{
			var result = new ValidationResult();

			var table = new Table();

			parser.Seek( whitespace );
			parser.Back();

			table.Identifier = ParseIdentifier();
			
			parser.Seek( whitespace );
			parser.Back();

			if (!parser.Seek( parenthesisLeft )) {
				result.AddError( ExceptionMessages.Parse_Sql_TableStartParenthesis );
			} else {

				bool readingTable = true;
				bool readingColumns = true;
				while (readingTable) {
				
					parser.Seek( whitespace );
					parser.Back();

					parser.ReadTo(tableItemName); // table item name reads to whitespace or the ) character
					parser.Back();

					if (parser.Text.Equals( "Constraint", StringComparison.InvariantCultureIgnoreCase )) {
						// Read Contraint
						readingColumns = false;

						// [Constraint]
						table.Constraints.Add( ParseConstraint() );

					} else if (parser.Text == "") { // We hit the ')'
						// End Table Create
						readingTable = false;

					} else if (readingColumns) {
						// Read column

						// [Column]
						table.Columns.Add( ParseColumn() );

					} else {
						// Invalid sequence
						result.AddError( ExceptionMessages.Parse_Sql_TableContentOrder );
					}

					parser.SeekTo( new Disjunction( alpha, comma ) );
					parser.Back();

					if (parser.Future == ",") {
						parser.Forward();
					}
				}
			}

			parser.Seek( whitespace );
			parser.Back();

			//if (!parser.Seek( parenthesisRight )) {
			//	result.AddError(ExceptionMessages.Parse_Sql_TableEndParenthesis);
			//}

			result.Validate();
			return table;
		}

		// eg [a].[b]
		protected Identifier ParseIdentifier()
		{
			var result = new ValidationResult();
			var identifier = new Identifier();

			try {

				// When testing mock ParseName.. maybe with moles?
				var parsing = true;
				do {
					identifier.AddPart( ParseName() );
					parsing = parser.Seek( dot );
				} while (parsing);

			} catch (ValidationException val) {

				result.Combine( val );
			}

			result.Validate();
			return identifier;
		}

		// eg [a]
		protected string ParseName()
		{
			var result = new ValidationResult();

			if (!parser.Read( alpha )) {
				parser.Back();
				if (parser.Read( squareLeft )) {
					parser.Concat();

					if (!parser.ReadTo( squareRight )) {
						result.AddError( ExceptionMessages.Parse_Sql_MissingRightBracket );

					} else if (parser.Length < minNameLength) {
						result.AddError( ExceptionMessages.Parse_Sql_InvalidObjectName );
					}

				} else {
					result.AddError(ExceptionMessages.Parse_Sql_MissingObjectName);
				}
			}

			result.Validate();
			return parser.Text;
		}

		protected Column ParseColumn()
		{
			var column = new Column() {
				Name = parser.Text
			};


			parser.Seek( whitespace );
			parser.Back();

			column.Type = ParseType();

			// optional null or not null
			// optional CONSTRAINT or IDENTITY (don't bother with identity)

			parser.SeekTo( nonWhitespace ); // comma or text
			parser.Back();

			return column;
		}

		protected DataType ParseType()
		{
			var dataType = new DataType();

			dataType.Name = ParseName();
			parser.Back();

			parser.Seek( whitespace );
			parser.Back();

			parser.SeekTo( nonWhitespace ); //new Disjunction( alpha, parenthesisLeft )
			parser.Back();

			// ToDo: [(p[ ,s] )]

			if (parser.Future == "(") {
				parser.Forward();

				parser.Seek( whitespace );
				parser.Back();

				parser.Read( numbers );

				dataType.Scale = int.Parse( parser.Text );
				parser.Back();

				parser.SeekTo( parenthesisRight );
			}

			return dataType;
		}

		protected Constraint ParseConstraint()
		{
			var constraint = new Constraint();

			/*
[ CONSTRAINT constraint_name ] 
{ 
	{ PRIMARY KEY | UNIQUE } 
		[ CLUSTERED | NONCLUSTERED ] 
		(column [ ASC | DESC ] [ ,...n ] ) 
		[ 
			WITH FILLFACTOR = fillfactor 
		   |WITH ( <index_option> [ , ...n ] ) 
		]
		[ ON { partition_scheme_name (partition_column_name)
			| filegroup | "default" } ] 
	| FOREIGN KEY 
		( column [ ,...n ] ) 
		REFERENCES referenced_table_name [ ( ref_column [ ,...n ] ) ] 
		[ ON DELETE { NO ACTION | CASCADE | SET NULL | SET DEFAULT } ] 
		[ ON UPDATE { NO ACTION | CASCADE | SET NULL | SET DEFAULT } ] 
		[ NOT FOR REPLICATION ] 
	| CHECK [ NOT FOR REPLICATION ] ( logical_expression ) 
} */
			return constraint;
		}
	}
}
