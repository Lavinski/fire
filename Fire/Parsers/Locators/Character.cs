﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Locators
{
	public class Character : ILocator
	{
		private char token;
		private bool ended;

		public Character(char token)
		{
			this.token = token;
		}

		public bool Ended { 
			get { return ended; }
		}

		public void Reset()
		{
			ended = false;
		}

		public bool IsPart(char next)
		{
			return (ended = (next == token));
		}

	}
}
