﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Locators
{
	public class Word : ILocator
	{
		int index = 0;
		string token;

		public Word(string token)
		{
			this.token = token;

			IgnoreCase = true;
		}

		public bool IgnoreCase { get; set; }

		public bool Ended { 
			get { return (index >= token.Length); }
		}

		public void Reset()
		{
			index = 0;
		}

		public bool IsPart(char next)
		{
			if (index >= token.Length) {
				return false;

			} else if (IgnoreCase?
						(char.ToLowerInvariant(next) == char.ToLowerInvariant(token[index])) : 
						(next == token[index])) {

				if (index == token.Length - 1) {
					index = 0;
					return true;
				} else {
					index += 1;
					return false;
				}
			} else {
				index = 0;
				return false;
			}
		}

	}
}
