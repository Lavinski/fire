﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Locators
{
	public class WhiteSpace : ILocator
	{
		bool reading = true;

		public bool IsPart(char next)
		{
			return (reading = char.IsWhiteSpace(next));
		}

		public bool Ended
		{
			get { return !reading; }
		}

		public void Reset() 
		{
			return;
		}

	}
}
