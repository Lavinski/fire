﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Locators
{
	public class Conjunction : ILocator
	{
		List<ILocator> locators = new List<ILocator>();

		public Conjunction()
		{ }

		public Conjunction(params ILocator[] locators)
		{
			this.locators.AddRange( locators );
		}

		public void Add(ILocator locator)
		{
			locators.Add( locator );
		}

		#region ILocator Members

		public bool Ended
		{
			get { return !locators.Any( x => !x.Ended ); }
		}

		public bool IsPart(char next)
		{
			return !locators.Any( x => !x.IsPart(next) );
		}

		public void Reset()
		{
			foreach (var item in locators) {
				item.Reset();
			}
		}

		#endregion
	}
}
