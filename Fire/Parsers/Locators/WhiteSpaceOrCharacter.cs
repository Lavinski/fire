﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Locators
{
	public class WhiteSpaceOrCharacter : ILocator
	{
		private char token;
		private bool ended;

		public WhiteSpaceOrCharacter(char token)
		{
			this.token = token;
		}

		public bool Ended { 
			get { return ended; }
		}

		public void Reset()
		{
			ended = false;
		}

		public bool IsPart(char next)
		{
			return (ended = 
				(next == token) ||
				(char.IsWhiteSpace(next)));
		}

	}
}
