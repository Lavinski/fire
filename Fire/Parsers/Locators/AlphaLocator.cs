﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Locators
{
	public class AlphaLocator : ILocator
	{
		bool reading = false;
		bool ended = false;

		public bool IsPart(char next)
		{
			var isPart = char.IsLetter(next);
			ended = reading && !isPart;

			return (reading = isPart);
		}

		public bool Ended
		{
			get { return ended; }
		}

		public void Reset() 
		{
			return;
		}

	}
}
