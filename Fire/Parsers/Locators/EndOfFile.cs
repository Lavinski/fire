﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Locators
{
	public class EndOfFile : ILocator
	{

		public bool IsPart(char next)
		{
			return true;
		}

		public bool Ended
		{
			get { return false; }
		}

		public void Reset() 
		{
			return;
		}

	}
}
