﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Locators
{
	public class Disjunction : ILocator
	{
		List<ILocator> locators = new List<ILocator>();

		public Disjunction()
		{ }

		public Disjunction(params ILocator[] locators)
		{
			this.locators.AddRange( locators );
		}

		public void Add(ILocator locator)
		{
			locators.Add( locator );
		}

		#region ILocator Members

		public bool Ended
		{
			//locators.Aggregate(true, (ended, locator) => ended && locator.Ended );
			get { return locators.Any( x => x.Ended ); }
		}

		public bool IsPart(char next)
		{
			return locators.Any( x => x.IsPart(next) );
		}

		public void Reset()
		{
			foreach (var item in locators) {
				item.Reset();
			}
		}

		#endregion
	}
}
