using System;
using System.Xml.Serialization;

namespace Fire.Parsers.Css
{
	/// <summary></summary>
	public class SimpleSelector
	{
		private Combinator? combinator = null;
		private string elementname;
		private string id;
		private string cls;
		private Attribute attribute;
		private string pseudo;
		private Function function;
		private SimpleSelector child;

		public Combinator? Combinator
		{
			get { return combinator; }
			set { combinator = value; }
		}

		public string ElementName
		{
			get { return elementname; }
			set { elementname = value; }
		}

		public string ID
		{
			get { return id; }
			set { id = value; }
		}

		public string Class
		{
			get { return cls; }
			set { cls = value; }
		}

		public string Pseudo
		{
			get { return pseudo; }
			set { pseudo = value; }
		}

		public Attribute Attribute
		{
			get { return attribute; }
			set { attribute = value; }
		}

		public Function Function
		{
			get { return function; }
			set { function = value; }
		}

		public SimpleSelector Child
		{
			get { return child; }
			set { child = value; }
		}

		/// <summary></summary>
		/// <returns></returns>
		public override string ToString()
		{

			System.Text.StringBuilder txt = new System.Text.StringBuilder();
			if (combinator.HasValue) {
				switch (combinator.Value) {
					case Fire.Parsers.Css.Combinator.PrecededImmediatelyBy:
						txt.Append( " + " );
						break;
					case Fire.Parsers.Css.Combinator.ChildOf:
						txt.Append( " > " );
						break;
					case Fire.Parsers.Css.Combinator.PrecededBy:
						txt.Append( " ~ " );
						break;
				}
			}
			if (elementname != null) { txt.Append( elementname ); }
			if (id != null) { txt.AppendFormat( "#{0}", id ); }
			if (cls != null) { txt.AppendFormat( ".{0}", cls ); }
			if (pseudo != null) { txt.AppendFormat( ":{0}", pseudo ); }
			if (attribute != null) { txt.Append( attribute.ToString() ); }
			if (function != null) { txt.Append( function.ToString() ); }
			if (child != null) {
				if (child.ElementName != null) { txt.Append( " " ); }
				txt.Append( child.ToString() );
			}
			return txt.ToString();
		}
	}
}