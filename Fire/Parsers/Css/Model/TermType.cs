using System;

namespace Fire.Parsers.Css 
{

	public enum TermType 
	{
		Number,
		Function,
		String,
		Url,
		Unicode,
		Hex
	}
}