using System;

namespace Fire.Parsers.Css {

	/// <summary>Combinators act on a left and right simple selector</summary>
	public enum Combinator {
		ChildOf,				// >
		PrecededImmediatelyBy,	// +
		PrecededBy				// ~
	}
}