using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Text;

namespace Fire.Parsers.Css
{
	/// <summary>An Expression is a collection of Terms. A Term represents an actual property value.</summary>
	public class Expression
	{
		private List<Term> terms = new List<Term>();

		public List<Term> Terms
		{
			get { return terms; }
			set { terms = value; }
		}

		public override string ToString()
		{
			StringBuilder txt = new StringBuilder();
			
			bool first = true;
			foreach (Term t in terms) {
				if (first) {
					first = false;
				} else {
					txt.AppendFormat( "{0} ", t.Seperator.HasValue ? t.Seperator.Value.ToString() : "" );
				}

				txt.Append( t.ToString() );
			}
			return txt.ToString();
		}
	}
}