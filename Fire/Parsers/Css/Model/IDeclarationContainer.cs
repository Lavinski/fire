using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Fire.Parsers.Css {
	/// <summary></summary>
	public interface IDeclarationContainer {
		/// <summary></summary>
		List<Declaration> Declarations { get; set; }
	}
}