using System;

namespace Fire.Parsers.Css
{
	/// <summary>Measurement unit</summary>
	public enum Unit
	{
		/// <summary>None</summary>
		None,
		/// <summary>The size of the element as a relative percentage percentage</summary>
		Percent,
		/// <summary>The font size of the element (or, to the parent element's font size if set on the 'font-size' property)</summary>
		Em,
		/// <summary>The x-height of the element's font</summary>
		Ex,
		/// <summary>Viewing device</summary>
		Px,
		/// <summary>The grid defined by 'layout-grid' described in the CSS3 Text module [CSS3TEXT]</summary>
		Gd,
		/// <summary>The font size of the root element</summary>
		Rem,
		/// <summary>The viewport's width</summary>
		VW,
		/// <summary>The viewport's height</summary>
		VH,
		/// <summary>The viewport's height or width, whichever is smaller of the two</summary>
		VM,
		/// <summary>The width of the "0" (ZERO, U+0030) glyph found in the font for the font size used to render. 
		/// If the "0" glyph is not found in the font, the average character width may be used. How is the "average character width" found?</summary>
		CH,
		/// <summary>MM</summary>
		MM,
		/// <summary>CM</summary>
		CM,
		/// <summary>Inches</summary>
		In,
		/// <summary>Points</summary>
		PT,
		/// <summary>PC</summary>
		PC,
		/// <summary>Degrees</summary>
		Deg,
		/// <summary>Grads</summary>
		Grad,
		/// <summary>Radians</summary>
		Rad,
		/// <summary>Turns</summary>
		Turn,
		/// <summary>MS</summary>
		MS,
		/// <summary>S</summary>
		S,
		/// <summary>Hertz</summary>
		Hz,
		/// <summary>Kilohertz</summary>
		kHz,
	}

	public static class UnitOutput
	{
		public static string ToString( Unit u )
		{
			if (u == Unit.Percent) {
				return "%";

			} else if (u == Unit.Hz || u == Unit.kHz) {
				return u.ToString();

			} else if (u == Unit.None) {
				return "";
			}
			return u.ToString().ToLower();
		}
	}
}