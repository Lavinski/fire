using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Fire.Parsers.Css {
	/// <summary></summary>
	public interface IRuleSetContainer {
		/// <summary></summary>
		List<RuleSet> RuleSets { get; set; }
	}
}