using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Text;

namespace Fire.Parsers.Css
{
	/// <summary>A selector is a combination of simple selectors</summary>
	public class Selector
	{
		private List<SimpleSelector> simpleselectors = new List<SimpleSelector>();

		public List<SimpleSelector> SimpleSelectors
		{
			get { return simpleselectors; }
			set { simpleselectors = value; }
		}

		/// <summary></summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder txt = new StringBuilder();
			
			bool first = true;
			foreach (SimpleSelector ss in simpleselectors) {
				if (first) { 
					first = false; 
				} else {
					txt.Append( " " );
				}
				txt.Append( ss.ToString() );
			}

			return txt.ToString();
		}
	}
}