using System;
using System.Xml.Serialization;
using System.Text;

namespace Fire.Parsers.Css
{
	/// <summary></summary>
	public class Function
	{
		private string name;
		private Expression expression;

		/// <summary></summary>
		[XmlAttribute( "name" )]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		/// <summary></summary>
		[XmlElement( "Expression" )]
		public Expression Expression
		{
			get { return expression; }
			set { expression = value; }
		}

		/// <summary></summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder txt = new StringBuilder();

			txt.AppendFormat( "{0}(", name );
			if (expression != null) {

				bool first = true;
				foreach (Term t in expression.Terms) {

					if (first) {
						first = false;
					} else if (!t.Value.EndsWith( "=" )) {
						txt.Append( ", " );
					}

					bool quoteMe = (t.Type == TermType.String && !t.Value.EndsWith( "=" ));

					if (quoteMe) { txt.Append( "'" ); }
					txt.Append( t.ToString() );
					if (quoteMe) { txt.Append( "'" ); }
				}
			}
			txt.Append( ")" );
			return txt.ToString();
		}
	}
}