using System;

namespace Fire.Parsers.Css
{
	/// <summary></summary>
	public enum DirectiveType
	{
		Media,
		Import,
		Charset,
		Page,
		FontFace,
		Namespace,
		Other
	}
}