﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Css
{
	public class Style
	{
		public Style()
		{
			Rules = new List<Rule>();
			Properties = new List<Property>();
		}

		public IList<Rule> Rules { get; private set; }
		public IList<Property> Properties { get; private set; }

	}
}
