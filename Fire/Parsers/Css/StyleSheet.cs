﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Css
{
	public class StyleSheet
	{
		public StyleSheet()
		{ 
			Styles = new List<Style>();
			Rules = new List<Rule>();
		}

		public IList<Style> Styles { get; private set; }
		public IList<Rule> Rules { get; private set; }
	}
}
