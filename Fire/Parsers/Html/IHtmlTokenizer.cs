﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Fire.Parsers.Html
{
	public interface IHtmlTokenizer
	{
		/// <summary>
		/// Flat list html tokenizer
		/// </summary>
		/// <param name="htmlStream">Data to tokenise</param>
		/// <returns>Tag list</returns>
		IList<Tag> Parse( Stream htmlStream );

	}
}
