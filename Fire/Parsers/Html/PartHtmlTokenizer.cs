﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Fire.Parsers.Html
{
	public class PartHtmlTokenizer : IHtmlTokenizer
	{
		private IList<Tag> tagList;
		private StreamReader reader;

		private int charInt = 0;
		private char curChar = (char)0;

		private StringBuilder data = null;
		private Attribute curAttrib = null;
		private Tag curTag = null;
		private ReadState state;
		private ReadingText textState;

		private char cValChar = '"';

		const string cdata = "![cdata[";
		const string comment = "!--";
		const string doctype = "!doctype";
		const string script = "script";

		/// <summary>
		/// Flat list html tokenizer
		/// </summary>
		/// <param name="htmlStream">Data to tokenise</param>
		/// <returns>Tag list</returns>
		public IList<Tag> Parse( Stream htmlStream )
		{
			var seek = SeekState.Name;
			var isWhitespace = false;
			var inside = false;

			tagList = new List<Tag>();
			reader = new StreamReader( htmlStream );
			data = new StringBuilder();

			state = ReadState.ReadingToTag;
			textState = ReadingText.Normal;

			/* Parser notes:
			 * Script tags are parsed using CDATA
			 * Attribute values that contain spaces must be inside quotes
			 */

			// For each char in the stream
			while ((charInt = reader.Read()) > 0) {

				// Get the character
				curChar = (char)charInt;
				isWhitespace = char.IsWhiteSpace( curChar );

				var isTagClosing = (curChar == '/');
				var isTagClosed = (curChar == '>');

				switch (state) {
					// Read to Tag
					case ReadState.ReadingToTag:
						// Reading to a tag build a html string with the contents
						// leading up to the start of the next html tag. This will
						// support script and CDATA parsing.

						char[] buffer;
						string endChars = string.Empty;

						// Only perform a back get of it's a possible data block end
						if (curChar == '>' && (textState == ReadingText.Script || textState == ReadingText.CData)) {
							buffer = new char[8];
							data.CopyTo( Math.Max( data.Length - 8, 0 ), buffer, 0, Math.Min( 8, data.Length ) );
							endChars = new string( buffer ).ToLower();
						}

						if (textState == ReadingText.Normal && curChar == '<') {
							// This is the end of a text block
							if (data.Length > 0) {
								tagList.Add( new Tag() {
									Type = TagType.Text,
									Value = data.ToString()
								} );
								data = new StringBuilder();
							}
							// [return] Switch State
							state = ReadState.ReadingTag;
							curTag = new Tag() {
								Type = TagType.Begin
							};
							tagList.Add( curTag );

						} else if (textState == ReadingText.Script && curChar == '>' && endChars.EndsWith( "</script" )) {

							data.Remove( data.Length - 8, 8 );
							textState = ReadingText.Normal;

						} else if (textState == ReadingText.CData && curChar == '>' && endChars.EndsWith( "]]" )) {

							data.Remove( data.Length - 2, 2 );
							textState = ReadingText.Normal;

							curTag.Type = TagType.CData;
							curTag.Value = data.ToString();
							data = new StringBuilder();

						} else {

							data.Append( curChar );
						}


						// return string tag
						break;

					// Read tag
					case ReadState.ReadingTag:

						// read until it reaches text, and then to the next whitespace

						if (inside) {
							
							if (BuilderStartsWith( data, cdata )) {
								textState = ReadingText.CData;
								inside = false;

								data = new StringBuilder();
								state = ReadState.ReadingToTag;
								break;

							} else if (BuilderStartsWith( data, comment )) {
								curTag.Type = TagType.Comment;
								inside = false;
								
								data = new StringBuilder();
								state = ReadState.ReadingComment;
								break;

							} else if (BuilderStartsWith( data, doctype )) {
								curTag.Type = TagType.Doctype;

							}
						}

						// Lower the char for the tag name
						curChar = char.ToLower( curChar );

						if (isTagClosing) {
							curTag.Type = curTag.Value == null ? TagType.End : TagType.SelfClosing;
						}

						if (isWhitespace && inside ||
							isTagClosing || isTagClosed) {
							// End Reading

							if (inside) {
								inside = false;

								curTag.Value = data.ToString();
								data = new StringBuilder();

								if (isTagClosed) {
									if (curTag.Type == TagType.Begin && curTag.Value == script) {
										textState = ReadingText.Script;
									}
									state = ReadState.ReadingToTag;
								} else {
									state = ReadState.ReadingAttribute;
								}
							}

						} else if (!isWhitespace && !inside) {
							// Begin reading

							inside = true;
						}

						if (inside && !isWhitespace) {
							// Read
							data.Append( curChar );
						}

						// return tag
						break;

					// Read attribute
					case ReadState.ReadingAttribute:

						var isQuote = (curChar == '"' || curChar == '\'');

						// Strings cannot be exited
						if (seek != SeekState.String) { // Base case

							// This exits the attribute code
							if (isTagClosing || isTagClosed) {

								if (seek == SeekState.Name) {
									if (data.Length > 0) {

										curAttrib = new Attribute() {
											Name = data.ToString()
										};

										curTag.Attributes.Add( curAttrib );
										data = new StringBuilder();
										inside = false;
									}
								} else { // Value
									if (data.Length > 0) {

										curAttrib.Value = data.ToString();
										data = new StringBuilder();
										inside = false;
									}
								}

								// Abort
								if (isTagClosed) {
									state = ReadState.ReadingToTag;
								} else {
									state = ReadState.EndingTag;
								}
								inside = false;
								break;
							}
						}

						if (seek == SeekState.String) {
							// Parses a string (already started)

							if (curChar == cValChar) {
								// End the string
								if (curAttrib != null) {
									curAttrib.Value = data.ToString();
								} else {
									curAttrib = new Attribute() {
										Name = data.ToString()
									};
									curTag.Attributes.Add( curAttrib );
								}
								data = new StringBuilder();
								seek = SeekState.Name;
								inside = false;
								break;
							} else {
								// Add to the string
								inside = true;
							}

						} else if (seek == SeekState.Value) {
							// Parses the data after an equals sign

							if (inside && isWhitespace) {
								// If not a string value and it ends
								curAttrib.Value = data.ToString();
								data = new StringBuilder();
								seek = SeekState.Name;
								inside = false;

							}
							if (!inside && isQuote) {
								// Start parsing a string
								seek = SeekState.String;
								cValChar = curChar;
								break;
							} else if (!inside && !isWhitespace) {
								// Add to the value
								inside = true;
							}


						} else if (seek == SeekState.Name) {
							// This parses data before an equals sign

							var isEquals = (curChar == '=');

							if (isEquals || inside && isWhitespace) {

								// There should always be data
								if (data.Length == 0) {
									state = ReadState.ReadingToTag;
									break;
									//throw new Exception( "Sanity Check" );
								}

								curAttrib = new Attribute() {
									Name = data.ToString()
								};

								curTag.Attributes.Add( curAttrib );
								data = new StringBuilder();
								inside = false;

								// Equals then go to value
								if (isEquals) {
									seek = SeekState.Value;
								}

							} else if (!inside && isQuote) {
								// Start parsing a string
								curAttrib = null;
								seek = SeekState.String;
								cValChar = curChar;
								break;

							} else if (!isWhitespace) {
								// Add more Name Data
								inside = true;

							}
						}

						if (inside) {
							data.Append( curChar );
						}

						// return attribute
						break;
					// Read string
					case ReadState.EndingTag:

						if (isTagClosed) {
							// End Reading

							state = ReadState.ReadingToTag;
						}

						break;

					// Read string
					case ReadState.ReadingComment:

						if (isTagClosed) {
							bool end = true;
							for (int i = data.Length - 2; i < data.Length; i++) {
								end &= (data[i] == '-');
							}

							if (end) {

								data.Remove( data.Length - 2, 2 );
								curTag.Value = data.ToString();
								data = new StringBuilder();

								state = ReadState.ReadingToTag;
								break;
							}
						}

						data.Append( curChar );

						// return string
						break;

					default:
						break;
				}
			}

			return tagList;
		}

		private bool BuilderStartsWith( StringBuilder builder, string prefix )
		{
			if (builder.Length >= prefix.Length) {
				for (int i = 0; i < prefix.Length; i++) {
					if (builder[i] != prefix[i]) {
						return false;
					}
				}
				return true;
			}
			return false;
		}

		private enum ReadState
		{
			ReadingToTag,
			ReadingTag,
			EndingTag,
			ReadingAttribute,
			ReadingComment
		}

		private enum SeekState
		{
			Name,
			Value,
			String
		}

		private enum ReadingText
		{
			Normal,
			CData,
			Script
		}

	}
}
