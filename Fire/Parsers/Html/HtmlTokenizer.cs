﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Fire.Parsers.Html
{
	public class HtmlTokenizer : IHtmlTokenizer
	{
		enum State
		{ 
			Document,
			TagBegin,
			TagName,
			AttribName,
			AttribBegin,
			AttribValue,
			AttribEnd,
			TagEnd,
			Text,
			CData,
			TagData
		}

		/// <summary>
		/// Flat list html tokenizer
		/// </summary>
		/// <param name="htmlStream">Data to tokenise</param>
		/// <returns>Tag list</returns>
		public IList<Tag> Parse( Stream htmlStream )
		{
			var tagList = new List<Tag>();
			var reader = new StreamReader( htmlStream );

			var charInt = 0;
			var curChar = (char)charInt;
			var data = new StringBuilder();
			var cState = State.Document;
			var cTag = new Tag();
			var cAttrib = (Attribute)null;
			var cValChar = '"';
			var insideAttrib = false;

			/* Parser notes:
			 * Script tags are parsed using CDATA
			 * Attribute values that contain spaces must be inside quotes
			 */

			// For each char in the stream
			while ((charInt = reader.Read()) > 0) {
				
				var handled = false;
				
				// Get the character
				curChar = (char)charInt;

				// Begin tag
				if (insideAttrib) {
					if (curChar == cValChar) {
						// If the attribute is well formed it can be added
						if (cAttrib != null) {
							cTag.Attributes.Add(cAttrib);
						}
						// Tell the parser it's outside a string
						insideAttrib = false;
					} else {
						data.Append(curChar);
					}

				} else if (curChar == '<') {
					if (cState == State.Text) {
						
						// Add a text tag
						if (data.Length > 0) {
							tagList.Add( new Tag() {
								Type = TagType.Text,
								Value = data.ToString()
							} );
							data.Remove( 0, data.Length );
						}
					}

					if (cState == State.TagEnd || cState == State.Document || cState == State.Text) {
						// A new tag is beginning
						cState = State.TagBegin;
						cTag.Type = TagType.Begin;

						handled = true;
					}

					// Tag endings
				} else if (curChar == '/') {
					handled = true;

					if (cState == State.TagBegin) {
						// This is a closing tag
						cTag.Type = TagType.End;

					} else if (cState == State.TagName || cState == State.AttribName || cState == State.AttribValue) {
						// This is a self closing tag
						cTag.Type = TagType.SelfClosing;

					} else {
						handled = false;

					}
					
					// End tag
				} else if (curChar == '>') {
					if ( cState == State.AttribName || cState == State.TagData || cState == State.AttribValue || cState == State.TagBegin || cState == State.AttribEnd) {
						
						// The tag is ending
						cState = State.TagEnd;

						// Add this tag to the list
						tagList.Add( cTag );
						cTag = new Tag();


						handled = true;
					}
					
					// Starting a string
				} else if (curChar == '"' || curChar == '\'') {
					if (cState == State.AttribName) {
						cValChar = curChar;

						cAttrib = new Attribute() {
							Name = data.ToString()
						};

						insideAttrib = true;
						data.Remove( 0, data.Length );

						handled = true;
					}

					// Not in a trivial state
				} else if (curChar == '=') {
					if (cState == State.AttribName) {
						
						handled = true;
					}

					// Tag Text
				} else {
					if (cState == State.TagBegin && !char.IsWhiteSpace(curChar)) {
						cState = State.TagData;
					}

					if (cState == State.TagData && char.IsWhiteSpace(curChar)) {
						cState = State.AttribName;
						cTag.Value = data.ToString();

						data.Remove( 0, data.Length );
						handled = true;
					}
					
					if (cState == State.TagData || cState == State.AttribName || cState == State.AttribValue || cState == State.AttribBegin) {
						 data.Append( curChar );
						 handled = true;
					}
				}

				// Free text
				if (!handled) {
					if (cState == State.TagEnd || cState == State.Text) {
						cState = State.Text;
						data.Append( curChar );
					}
				}

				//<div/> <div></div> 
			}

			return tagList;
		}

	}
}
