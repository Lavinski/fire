﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Html
{
	public enum TagType
	{
		CData,
		Text,
		Comment,
		Doctype,
		Begin,
		End,
		SelfClosing
	}
}
