﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Html
{
	public class Tag
	{
		public Tag()
		{
			Attributes = new List<Attribute>();
		}

		public string Value { get; set; }
		public IList<Attribute> Attributes { get; protected set; }

		public TagType Type { get; set; }

		public override string ToString()
		{
			if (Type == TagType.Begin) {
				return string.Format( "<{0}> ({1})", Value, Attributes.Count );
			
			} else if (Type == TagType.SelfClosing) {
				return string.Format( "<{0} /> ({1})", Value, Attributes.Count );

			} else if (Type == TagType.End) {
				return string.Format( "</{0}>", Value );

			} else {
				return Value;
			}
		}
	}
}
