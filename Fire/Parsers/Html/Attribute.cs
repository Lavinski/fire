﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Html
{
	public class Attribute
	{
		public string Name { get; set; }
		public string Value { get; set; }

		public override string ToString()
		{
			if (Value != null) {
				return string.Format( "{0}: {1}", Name, Value );
			} else {
				return Name;
			}
		}
	}
}
