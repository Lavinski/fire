﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Token
{
	public class TextTokenizer : StringTokenizer
	{
		public TextTokenizer(string value) : base(value) { }
		
		public override bool IsTokenPart(bool recording, char cchar)
		{
			return char.IsLetter(cchar) || cchar == '_' || cchar == '-';
		}
	}
}
