﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Token
{
	public abstract class StringTokenizer
	{
		int currentIndex = 0;
		bool recording = false;
		StringBuilder word = new StringBuilder();
		string phase = string.Empty;

		public StringTokenizer(string value)
		{
			this.phase = value;
		}

		/// <summary>
		/// Returns false when there are no more words
		/// </summary>
		public bool Done
		{
			get
			{
				if (currentIndex >= phase.Length) {
					return true;
				} else {
					for (int i = currentIndex; i < phase.Length; i++)
					{
						if (IsTokenPart(false, phase[i])) {
							currentIndex = i;
							return false;
						}
					}
				}
				return true;
			}
		}

		/// <summary>
		/// Gets the next word or an empty string
		/// </summary>
		public string Next() {

			char char1 = '\0';
			while (currentIndex < phase.Length) {
				char1 = phase[currentIndex++];

				if (IsTokenPart(recording, char1)) {
					recording = true;
					word.Append( char1 );

				} else if (recording) {

					//if (!IsTokenPart(true, phase[currentIndex])) {
					//	currentIndex += 1;
					//}

					break;
				}
			}

			var result = word.ToString();
			word.Remove( 0, word.Length );

			recording = false;
			return result;
		}

		public abstract bool IsTokenPart(bool recording, char cchar);

	}
}
