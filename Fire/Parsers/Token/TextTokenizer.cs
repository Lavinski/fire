﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Token
{
	public class WordTokenizer : StringTokenizer
	{
		public WordTokenizer(string value) : base(value) { }
		
		public override bool IsTokenPart(bool recording, char cchar)
		{
			return char.IsLetter(cchar);
		}
	}
}
