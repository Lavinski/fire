﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Parsers.Token
{
	public class NumberTokenizer : StringTokenizer
	{
		public NumberTokenizer(string value) : base(value) { }

		private bool signed = false;
		private bool deci = false;

		public override bool IsTokenPart(bool recording, char cchar)
		{
			var digit = char.IsDigit(cchar);

			if ((signed || deci) && !digit) {
				signed = deci = false;
			}

			return
			(!signed && cchar == '-') ||
			(!deci && cchar == '.') ||
			digit;
	
		}

		public float NextFloat()
		{
			float result;
			string number = Next();
			if (float.TryParse(number, out result)) {
				
				return result;
			} else {
				return 0;
			}
		}
	}
}
