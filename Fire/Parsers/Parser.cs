﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Fire.Parsers
{
	public enum Gap
	{
		/// <summary>Must not have whitespace</summary>
		None,
		/// <summary>Must have whitespace</summary>
		WhiteSpace,
		/// <summary>Whitespace doesn't matter</summary>
		Optional,
	}

	public interface Locator
	{
		bool Failed { get; }
		bool Found { get; }

		void Reset();
		bool IsLocated(char next);
	}

	public class StringLocator : Locator
	{
		int index = 0;
		string token;
		bool failed = false;

		public StringLocator(string token)
		{
			this.token = token;

			IgnoreCase = true;
		}

		public bool IgnoreCase { get; set; }

		public bool Failed { 
			get { return failed; }
		}

		public bool Found { 
			get { return (index >= token.Length); }
		}

		public void Reset()
		{
			index = 0;
			failed = false;
		}

		public bool IsLocated(char next)
		{
			if (index >= token.Length) {
				return false;

			} else if (IgnoreCase?
						(char.ToLowerInvariant(next) == char.ToLowerInvariant(token[index])) : 
						(next == token[index])) {

				if (index == token.Length - 1) {
					index = 0;
					return true;
				} else {
					index += 1;
					return false;
				}
			} else {
				index = 0;
				failed = true;
				return false;
			}
		}

	}

	public class Section 
	{
		public Locator Locator { get; set; }
		public Action Action { get; set; }

		public Section(string location) :
			this( new StringLocator( location ) ) { }

		public Section(string location, Action action) :
			this( new StringLocator( location ), action ) { }

		public Section(Locator locator)
		{
			Locator = locator;
		}

		public Section(Locator locator, Action action)
		{
			Locator = locator;
			Action = action;
		}
	}

	public class Parser
	{
		// ReadTo (locator) 
		// Read (locator)

		// SeekTo (locator)
		// Seek (locator)

		// 

		public StreamReader Stream { get; set; }

		public bool Seek(string location)
		{
			return Seek( Gap.Optional, new[] { new Section(location) } );
		}

		public bool Seek(string location, Action action)
		{
			return Seek( Gap.Optional, new[] { new Section(location, action) }, null );
		}

		public bool Seek(string location, Action action, Action noMatch)
		{
			return Seek( Gap.Optional, new[] { new Section(location, action) }, noMatch );
		}

		public bool Seek(IEnumerable<Section> sections)
		{
			return Seek( Gap.Optional, sections, null );
		}

		public bool Seek(Gap preGap, IEnumerable<Section> sections)
		{
			return Seek( preGap, sections, null );
		}

		public bool Seek(Gap preGap, IEnumerable<Section> sections, Action noMatch)
		{
			bool seeking = true;
			bool hadWhitespace = false;
			do {
				var intChar = Stream.Read();
				if (intChar != -1) {
					
					var character = (char)intChar;
					var isWhitespace = char.IsWhiteSpace( character );

					if (preGap == Gap.None) {
						if (isWhitespace) {
							
							// End
							seeking = false;
							if (noMatch != null) {
								noMatch();
							}
							break;
						}
					} else if (preGap == Gap.WhiteSpace) {
						if (!hadWhitespace && !isWhitespace) {
							
							// End
							seeking = false;
							if (noMatch != null) {
								noMatch();
							}
							break;
						} else {
							hadWhitespace = true;
						}
					}

					// Locators
					if (preGap == Gap.None || !isWhitespace) {
						foreach (var section in sections) {

							// Check if located
							if (!section.Locator.Failed &&
								section.Locator.IsLocated( character )) {

								if (section.Action != null) {
									section.Action();
								}

								seeking = false;
								return true;
							}

						}
					}

				} else {
					// End
					seeking = false;
					if (noMatch != null) {
						noMatch();
					}
				}

			} while (seeking);
			return false;
		}

	}
}
