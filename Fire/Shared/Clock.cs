﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fire.Extensions;

namespace Fire.Shared
{
	public class Clock : IClock
	{
		static Clock()
		{
			Current = new Clock();
		}

		public static IClock Current { get; set; }

		private TimeSpan offset;

		public DateTime Now {
			get { return DateTime.Now + offset; }
			set { offset = value - DateTime.Now; }
		}

		public DateTime Today { 
			get { return (DateTime.Now + offset).GetDayStart(); }
		}
	}
}
