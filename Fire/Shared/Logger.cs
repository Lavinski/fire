﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fire.Extensions;
using Fire.Logging;

namespace Fire.Shared
{
	/// <summary>
	/// Logger shell
	/// </summary>
	public class Logger : ILogger
	{
		static Logger()
		{
			Current = new Logger();
		}

		public static ILogger Current { get; set; }

		public bool IsLevelEnabled( LogLevel level )
		{
			return Current.IsLevelEnabled(level);
		}

		public void Log( object message, params object[] values )
		{
			Current.Log( message, values );
		}

		public void Log( LogLevel level, object message, params object[] values )
		{
			Current.Log( level, message, values );
		}

	}
}
