﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Expression = System.Text.RegularExpressions.Regex;

namespace Fire.Regex
{
	public static class Validatiors
	{
		public const string isGuid = @"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$";

		/// <summary> Regular expression usage example </summary>
		public static bool Validate( string value, string expression )
		{
			return new Expression( expression ).IsMatch( value );
		}
	}
}
