﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Services
{
	public class JobAttribute : Attribute
	{
		public JobAttribute() { }

		public JobAttribute( int hours, int minutes, int seconds )
		{
			Interval = new TimeSpan( hours, minutes, seconds );
		}

		public TimeSpan Interval { get; set; }
	}
}
