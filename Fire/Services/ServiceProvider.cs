﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Fire.Logging;
using Fire.Shared;

namespace Fire.Services
{
	// This class is incomplete
	/// <summary> Provides a service that periodically calls job methods </summary>
	public class ServiceProvider : IInitializable
	{
		class JobDetail
		{
			public DateTime LastRunTime { get; set; }
			public TimeSpan Interval { get; set; }
			public ThreadStart Method { get; set; }
		}

		private Thread thread = null;
		private IList<JobDetail> jobs = new List<JobDetail>();
		private TimeSpan defaultTick = new TimeSpan( 0, 5, 0);
		private ILogger logger = Logger.Current;

		public ServiceProvider()
		{
			Running = true;
		}

		public void Initialize()
		{
			if (TickInterval == default(TimeSpan)) {
				TickInterval = new TimeSpan( 0, 1, 0 );
			}

			foreach (var job in Jobs) {
				Add( job );
			}

			// ToDo: Search for jobs with the CronJobAttribute

			if (Debug) {
				ServiceThread();
			} else {
				thread = new Thread( new ThreadStart( ServiceThread ) );
				thread.IsBackground = true;
				thread.Name = "Service Provider";
				thread.Start();
			}
			
		}

		public void ServiceThread()
		{
			while (Running) {

				try {

					long start_t = Clock.Current.Now.Ticks;

					foreach (var job in jobs) {

						// If the next run time is less than or equal to now
						if (job.LastRunTime + job.Interval <= Clock.Current.Now) {

							// Set the last run time
							job.LastRunTime = Clock.Current.Now;

							if (logger.IsLevelEnabled( LogLevel.Info )) {
								logger.Log( LogLevel.Info, "Service job: {0}", job.Method.Method.Name );
							}

							// Invoke the job last
							job.Method();
						}

					}
					long delta_t = (Clock.Current.Now.Ticks - start_t) / 10000;
					if (logger.IsLevelEnabled( LogLevel.Info ) && delta_t > 6) {
						logger.Log( LogLevel.Info, "Service tick. processing took over 6ms ({0}ms)", delta_t );
					}

				} catch (Exception ex) {
					logger.Log( ex );

				} finally {
					// End
				}

				Thread.Sleep( TickInterval );
			}
		}

		public void Add( ThreadStart method )
		{
			var attributes = method.Method.GetCustomAttributes( typeof( JobAttribute ), true );

			TimeSpan interval;
			var attribute = attributes.FirstOrDefault() as JobAttribute;
			if (attribute != null) {
				interval = attribute.Interval;
			} else {
				interval = defaultTick;
			}

			jobs.Add( new JobDetail() {
				Interval = interval,
				Method = method
			} );
		}

		public void Remove( ThreadStart method )
		{
			try {
				var job = jobs.Where( j => j.Method == method ).First();
				jobs.Remove( job );

			} catch (Exception) {
				throw new Exception( "Job could not be removed" );
			}
			
		}
		
		public IList<ThreadStart> Jobs
		{ set; private get; }

		public TimeSpan TickInterval
		{ set; get; }

		public bool Debug 
		{ get; set; }

		public bool Running 
		{ get; set; }
	}
}
