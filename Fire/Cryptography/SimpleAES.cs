﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Fire.Cryptography
{

	public class SimpleAES : IEncryptor, IDecryptor
	{
		// I'll just use a constant vector for now
		private readonly static byte[] vector = { 16, 27, 42, 11, 213, 13, 1, 122, 131, 121, 221, 112, 79, 32, 114, 156 };

		public static IEncryptor GenerateEncryptor(string key) 
		{
			var hasher = MD5.Create();

			var bytes = hasher.ComputeHash(Encoding.ASCII.GetBytes(key));
			return GenerateEncryptor(bytes);
		}

		public static IEncryptor GenerateEncryptor(byte[] key) 
		{
			var rm = new RijndaelManaged();
			var transform = rm.CreateEncryptor(key, vector);

			return new SimpleAES(transform);
		}


		public static IDecryptor GenerateDecryptor(string key) 
		{
			var hasher = MD5.Create();

			var bytes = hasher.ComputeHash(Encoding.ASCII.GetBytes(key));
			return GenerateDecryptor(bytes);
		}

		public static IDecryptor GenerateDecryptor(byte[] key) 
		{
			var rm = new RijndaelManaged();
			var transform = rm.CreateDecryptor(key, vector);

			return new SimpleAES(transform);
		}


		private ICryptoTransform transformer;
		private UTF8Encoding UTFEncoder;

		private SimpleAES(ICryptoTransform transformer)
		{
			this.transformer = transformer;
			this.UTFEncoder = new System.Text.UTF8Encoding();
		}


		public string Encrypt(string data)
		{
			return Convert.ToBase64String(Encrypt(UTFEncoder.GetBytes(data)));
		}

		public byte[] Encrypt(byte[] data)
		{

			//Used to stream the data in and out of the CryptoStream.
			MemoryStream memoryStream = new MemoryStream();

			/*
			 * We will have to write the unencrypted bytes to the stream,
			 * then read the encrypted result back from the stream.
			 */
			#region Write the decrypted value to the encryption stream
			CryptoStream cs = new CryptoStream(memoryStream, transformer, CryptoStreamMode.Write);
			cs.Write(data, 0, data.Length);
			cs.FlushFinalBlock();
			#endregion

			#region Read encrypted value back out of the stream
			memoryStream.Position = 0;
			byte[] encrypted = new byte[memoryStream.Length];
			memoryStream.Read(encrypted, 0, encrypted.Length);
			#endregion

			//Clean up.
			cs.Close();
			memoryStream.Close();

			return encrypted;
		}

		/// The other side: Decryption methods
		public string Decrypt(string EncryptedString)
		{
			return UTFEncoder.GetString(Decrypt(Convert.FromBase64String(EncryptedString)));
		}

		/// Decryption when working with byte arrays.    
		public byte[] Decrypt(byte[] EncryptedValue)
		{
			#region Write the encrypted value to the decryption stream
			MemoryStream encryptedStream = new MemoryStream();
			CryptoStream decryptStream = new CryptoStream(encryptedStream, transformer, CryptoStreamMode.Write);
			decryptStream.Write(EncryptedValue, 0, EncryptedValue.Length);
			decryptStream.FlushFinalBlock();
			#endregion

			#region Read the decrypted value from the stream.
			encryptedStream.Position = 0;
			Byte[] decryptedBytes = new Byte[encryptedStream.Length];
			encryptedStream.Read(decryptedBytes, 0, decryptedBytes.Length);
			encryptedStream.Close();
			#endregion
			return decryptedBytes;
		}

	}


}
