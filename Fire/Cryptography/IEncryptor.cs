﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Cryptography
{
	public interface IEncryptor
	{
		string Encrypt(string data);
		byte[] Encrypt(byte[] data);
	}
}
