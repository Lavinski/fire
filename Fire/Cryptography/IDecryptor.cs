﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Cryptography
{
	public interface IDecryptor
	{
		string Decrypt(string data);
		byte[] Decrypt(byte[] data);
	}
}
