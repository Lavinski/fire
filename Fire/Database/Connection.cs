﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database
{
	/// <summary>
	/// Wraps a database connection string
	/// Note: keys are all lowercase, values are preserved
	/// </summary>
	public class Connection
	{
		private bool isDirty = false;
		private string value = null;

		private Dictionary<string, string> parts = new Dictionary<string, string>();

		public Connection() { }
		public Connection( string value ) {
			Value = value;
		}

		public string Username
		{
			get { return this["user"]; }
			set { 
				this["user"] = value;
				this.isDirty = true;
			}
		}
		public string Password
		{
			get { return this["password"]; }
			set { 
				this["password"] = value;
				this.isDirty = true;
			}
		}

		public string Location
		{
			get { return this["database"]; }
			set { 
				this["database"] = value;
				this.isDirty = true;
			}
		}

		public string Value
		{
			get {
				if (isDirty) {
					StringBuilder builder = new StringBuilder();
					foreach (var item in parts) {
						builder.AppendFormat( "{0}={1};", item.Key, item.Value );
					}
					value = builder.ToString();
				}
				return value;
			}
			set {
				this.value = value;
				this.isDirty = false;

				var partList = value.Split( new[] { ';' }, StringSplitOptions.RemoveEmptyEntries );
				foreach (var item in partList) {
					var pair = item.Split( new[] { '=' }, StringSplitOptions.RemoveEmptyEntries );
					if (pair.Length > 1) {

						var key = pair[0].Trim().ToLowerInvariant();
						var val = pair[1].Trim();

						if (parts.ContainsKey(key)) {
							parts[key] = val;
						} else {
							parts.Add(key, val);
						}
						
					}
				}
			}
		}

		public string this[string key]
		{
			get {
				key = key.ToLowerInvariant();

				if (parts.ContainsKey( key )) {
					return parts[key];
				} else {
					return null;
				}
			}
			set {
				key = key.ToLowerInvariant();

				if (parts.ContainsKey( key )) {
					parts[key] = value;
				} else {
					parts.Add(key, value);
				}
				this.isDirty = true;
			}
		}

		public override string ToString()
		{
			return Value;
		}
	}
}
