﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database
{
	public interface IUser
	{
		string Username { get; set; }
		string Password { get; set; }

		bool Equals( IUser user);
		T GetAs<T>();
	}
}
