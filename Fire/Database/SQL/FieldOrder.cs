﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database.SQL
{
	public class FieldOrder
	{
		public FieldOrder( Field field )
		{
			this.Order = Order.Asc;
			this.Field = field;
		}

		public FieldOrder( Order order, Field field )
		{
			this.Order = order;
			this.Field = field;
		}

		public Order Order { get; set; }
		public Field Field { get; set; }
	}
}
