﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database.SQL
{
	public enum Order
	{
		Asc,
		Desc
	}

	public enum JoinSide
	{
		Left,
		Right
	}

	public enum JoinType
	{
		Inner,
		Outer
	}
}
