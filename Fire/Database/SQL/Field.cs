﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database.SQL
{
	public class Field
	{
		public Field( string name )
		{
			Name = name;
			Type = typeof( object );
		}

		public Field( Table table, string name )
		{
			Table = table;
			Name = name;
			Type = typeof( object );
		}

		/* Order */

		public FieldOrder Order()
		{
			return new FieldOrder( this );
		}

		public FieldOrder Order(Order order) 
		{
			return new FieldOrder( order, this );
		}

		/* Properties */ 

		public Table Table { get; protected set; }
		public string Name { get; set; }
		public Type Type { get; set; }

		public override string ToString()
		{
			return ((Table == null) ? Name : string.Format("{0}.{1}", Table.ToString(), Name));
		}
	}
}
