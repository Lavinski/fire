﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database.SQL
{
	public class Predicate
	{
		private string expression = null;
		private Table[] tables = null;

		public Predicate Apply( string expression, params Table[] tables )
		{
			this.expression = expression;
			this.tables = tables;
			return this;
		}

		public override string ToString()
		{
			if (tables != null && tables.Length > 0) {
				return string.Format( expression, tables.Select(x => x.ToString()).ToArray() );

			} else {
				return expression;
			}
		}

		/* Predicate Examples */

		public static Predicate EqualTo( Field left, Field right )
		{ 
			return new Predicate().Apply( string.Concat( left.ToString(), " = ", right.ToString() ) );
		}

		public static Predicate EqualTo( Field left, object value )
		{
			return EqualTo(left, value, false);
		}

		public static Predicate EqualTo( Field left, object value, bool literal )
		{
			StringBuilder builder = new StringBuilder()
				.Append( left )
				.Append( '=' );

			if (literal || 
				value is int ||
				value is long || 
				value is short ||
				value is byte ||
				value is bool) {

				builder.Append( value );

			} else {
				builder.Append( '\'' ).Append( value.ToString().Replace("'", "''") ).Append( '\'' );

			}

			return new Predicate() { expression = builder.ToString() };
		}

	}
}
