﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database.SQL
{
	public class Join
	{
		public Join()
		{
			Side = JoinSide.Left;
			Type = JoinType.Inner;
		}

		public JoinSide Side { get; set; }
		public JoinType Type { get; set; }

		public Table Table { get; set; }
		public Field LeftField { get; set; }
		public Field RightField { get; set; }

		public void Assert()
		{

			if (LeftField.Table != null && RightField.Table != null &&
					(Table != LeftField.Table || Table != RightField.Table)) {

				throw new Exception( string.Format( "The table {0} just be used in the join", Table.Name ) );
			}

		}
	}


}
