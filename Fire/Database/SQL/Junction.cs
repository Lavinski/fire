﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database.SQL
{
	public class Disjunction : Junction
	{
		public Disjunction()
		{
			link = "Or";
		}
	}

	public class Conjunction : Junction
	{
		public Conjunction()
		{
			link = "And";
		}
	}

	public abstract class Junction : Predicate
	{
		protected List<Predicate> predicates;
		protected string link = "logicalOp";

		public Junction()
		{
			predicates = new List<Predicate>();
		}

		public Junction Add( Predicate predicate )
		{
			predicates.Add(predicate);
			return this;
		}

		public int Count
		{
			get {
				return predicates.Count;
			} 
		}

		public bool Any
		{
			get {
				return predicates.Count > 0;
			}
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			var isFirst = true;
			foreach (var item in predicates) {
				if (isFirst) {
					isFirst = false;
				} else {
					builder.Append( ' ' ).Append( link ).Append( ' ' );
				}
				builder.Append('(').Append( item ).Append(')');
			}

			return builder.ToString();
		}
	}
}
