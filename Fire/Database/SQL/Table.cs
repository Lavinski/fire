﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database.SQL
{
	public class Table
	{
		public Table( string name ) : this(name, null)
		{ 
			
		}

		public Table( string name, string alias )
		{
			if (string.IsNullOrEmpty( name )) {
				throw new ArgumentException(string.Format( "Table name is empty" ));
			}
			Name = name;
			Alias = alias;
		}

		public string Name { get; set; }
		public string Alias { get; set; }
		
		public string Selector {
			get {
				return string.Format( "{0}.*", Alias ?? Name );
			}
		}

		public override string ToString()
		{
			return Alias ?? Name;
		}
	}
}
