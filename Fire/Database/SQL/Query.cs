﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database.SQL
{

	public class Query
	{
		private Table mainTable;

		private List<Join> joins;
		private Conjunction conjunction;
		private List<Table> tableSelect;
		private List<Field> fieldSelect;
		private List<FieldOrder> fieldOrder;
		private List<Field> fieldGroup;

		private Query( Table table )
		{
			this.mainTable = table;
			this.joins = new List<Join>();
			this.conjunction = new Conjunction();
			this.tableSelect = new List<Table>();
			this.fieldSelect = new List<Field>();
			this.fieldOrder = new List<FieldOrder>();
			this.fieldGroup = new List<Field>();
		}

		/* Properties */

		public Table FromTable
		{
			get { return mainTable; }
		}

		/* From */

		public static Query From( string table )
		{
			return new Query( new Table( table ) );
		}

		public static Query From( Table table )
		{
			return new Query( table );
		}

		/* Where */

		public Query Where( Predicate predicate )
		{
			conjunction.Add( predicate );

			return this;
		}

		// public void Where( Conjunction predicate )
		// public void Where( Disjunction predicate )

		/* Join */

		public Query Join( string table, string fieldLeft, string fieldRight )
		{
			var jTable = new Table( table );

			Join( new Join() {
				Table = jTable,
				LeftField = new Field(jTable, fieldLeft),
				RightField = new Field(mainTable, fieldRight)
			} );

			return this;
		}

		public Query Join( Table table, string fieldLeft, string fieldRight )
		{
			Join( new Join() {
				Table = table,
				LeftField = new Field(table, fieldLeft),
				RightField = new Field(mainTable, fieldRight)
			} );

			return this;
		}

		public Query Join( Table table, Field fieldLeft, Field fieldRight )
		{
			Join( new Join() {
				Table = table,
				LeftField = fieldLeft,
				RightField = fieldRight
			} );

			return this;
		}

		public Query Join( JoinSide side, JoinType type, Table table, Field fieldLeft, Field fieldRight )
		{
			Join( new Join() {
				Side = side,
				Type = type,
				Table = table,
				LeftField = fieldLeft,
				RightField = fieldRight
			} );

			return this;
		}

		public Query Join( Join join )
		{
			join.Assert();
			joins.Add( join );

			return this;
		}

		/* Select */
		public Query Select( params string[] fields )
		{
			foreach (var item in fields) {
				fieldSelect.Add( new Field(item) );
			}

			return this;
		}
		public Query Select( IEnumerable<string> fields )
		{
			foreach (var item in fields) {
				fieldSelect.Add( new Field(item) );
			}

			return this;
		}

		public Query Select( params Field[] fields )
		{
			fieldSelect.AddRange( fields );

			return this;
		}
		public Query Select( IEnumerable<Field> fields )
		{
			fieldSelect.AddRange( fields );

			return this;
		}

		public Query Select( params Table[] tables )
		{
			tableSelect.AddRange( tables );

			return this;
		}
		public Query Select( IEnumerable<Table> tables )
		{
			tableSelect.AddRange( tables );

			return this;
		}

		/* Order By */

		public Query OrderBy( params FieldOrder[] fields )
		{
			fieldOrder.AddRange( fields );

			return this;
		}

		/* Group By */

		public Query GroupBy( params Field[] fields )
		{
			fieldGroup.AddRange( fields );

			return this;
		}


		public override string ToString()
		{
			var isFirst = true;
			var builder = new StringBuilder();

			builder.Append( "Select " );

			// Select
			isFirst = true;
			if (fieldSelect.Count == 0 && tableSelect.Count == 0) {
				builder.Append( '*' );
			} else {
				foreach (var selections in fieldSelect) {
					if (!isFirst) {
						builder.Append( ", " );
					} else {
						isFirst = false;
					}
					builder.Append( selections.ToString() );
				}
				foreach (var selections in tableSelect) {
					if (!isFirst) {
						builder.Append( ", " );
					} else {
						isFirst = false;
					}
					builder.Append( selections.Selector );
				}
			}
			builder.AppendLine();

			// From 
			builder.Append( "From " ).Append( mainTable ).AppendLine();

			// Joins
			if (joins.Count > 0) {
				foreach (var join in joins) {
					builder.Append( join.Side ).Append( ' ' ).Append( join.Type )
						.Append( " Join " )
						.Append( join.Table.Name );

					if (!string.IsNullOrEmpty( join.Table.Alias )) {
						builder.Append( ' ' ).Append( join.Table.Alias );
					}

					builder
						.Append( " On " )
						.Append( join.LeftField )
						.Append( " = " )
						.Append( join.RightField );
				}
				builder.AppendLine();
			}

			// Where
			if (conjunction.Any) {
				builder
					.Append("Where ")
					.Append( conjunction )
					.AppendLine();
			}

			// Order By
			isFirst = true;
			if (fieldOrder.Count > 0) {
				builder.Append( "Order By " );
				foreach (var field in fieldOrder) {
					if (!isFirst) {
						builder.Append( ", " );
					} else {
						isFirst = false;
					}
					builder.Append( field.Field ).Append( ' ' ).Append( field.Order );
				}
				builder.AppendLine();
			}

			// Group By
			isFirst = true;
			if (fieldGroup.Count > 0) {
				builder.Append( "Group By " );
				foreach (var field in fieldGroup) {
					if (!isFirst) {
						builder.Append( ", " );
					} else {
						isFirst = false;
					}
					builder.Append( field.ToString() );
				}
				builder.AppendLine();
			}

			return builder.ToString();
		}

	}

}
