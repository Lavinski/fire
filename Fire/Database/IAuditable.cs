﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Database
{
	public interface IAuditable
	{
		IUser Creator { get; set; }
		IUser Modifier { get; set; }
		DateTime Created { get; set; }
		DateTime Modified { get; set; }
	}
}
