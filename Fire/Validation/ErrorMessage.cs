﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Fire.Extensions;

namespace Fire.Validation
{
	[DebuggerDisplay( "{Message}" )]
	public class ValidationMessage
	{
		public ValidationMessage()
		{ }

		public ValidationMessage(string message)
		{
			Message = message;
		}
			
		public ValidationMessage( string message, params object[] args )
		{
			Message = message.Format( args );
		}

		public ValidationMessage(string location, string message)
		{
			Location = location;
			Message = message;
		}

		public ValidationMessage( string location, string message, params object[] args )
		{
			Location = location;
			Message = message.Format( args );
		}

		public string Location { get; set; }
		public string Message { get; set; }
	}
}
