﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Validation.MessageResults
{
	[Obsolete]
	public class NaturalResult : ResultBase
	{
		public override bool IsValid { 
			get { 
				return (Errors.Count == 0);
			} 
		}
	}
}
