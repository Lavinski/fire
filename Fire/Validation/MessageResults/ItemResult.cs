﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Validation.MessageResults
{
	public class ItemResult<T>
	{
		public IResult Result { get; set; }
		public T Item { get; set; }
	}
}
