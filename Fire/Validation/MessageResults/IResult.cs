﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Validation.MessageResults
{
	[Obsolete]
	public interface IResult
	{
		bool IsValid { get; }
		IList<ValidationMessage> Warnings { get; }
		IList<ValidationMessage> Errors { get; }

		void AddWarning(string message);
		void AddWarning(ValidationMessage message);

		void AddError(string message);
		void AddError(ValidationMessage message);

		void Append(IResult result);
	}
}
