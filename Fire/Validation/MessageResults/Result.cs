﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Validation.MessageResults
{
	[Obsolete]
	public abstract class ResultBase : IResult
	{
		public ResultBase()
		{
			Warnings = new List<ValidationMessage>();
			Errors = new List<ValidationMessage>();
		}

		public ResultBase(StrictResult result)
		{
			Warnings = result.Warnings;
			Errors = result.Errors;
		}

		public ResultBase(string error)
		{
			Warnings = new List<ValidationMessage>();
			Errors = new List<ValidationMessage>();

			AddError(error);
		}

		public abstract bool IsValid { get; }
		public IList<ValidationMessage> Warnings { get; private set; }
		public IList<ValidationMessage> Errors { get; private set; }

		public void AddWarning(string message)
		{
			Warnings.Add(new ValidationMessage(message));
		}
		public void AddWarning(ValidationMessage message)
		{
			Warnings.Add(message);
		}

		public void AddError(string message)
		{
			Errors.Add( new ValidationMessage( message ) );
		}
		public void AddError(ValidationMessage message)
		{
			Errors.Add(message);
		}

		public void Append(IResult result)
		{
			foreach (var item in result.Errors) {
				Errors.Add(item);
			}

			foreach (var item in result.Warnings) {
				Warnings.Add(item);
			}
		}

	}
}
