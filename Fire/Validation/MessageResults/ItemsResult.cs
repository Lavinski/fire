﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Validation.MessageResults
{
	public class ItemsResult<T>
	{
		public IResult Result { get; set; }
		public IEnumerable<T> Item { get; set; }
	}
}
