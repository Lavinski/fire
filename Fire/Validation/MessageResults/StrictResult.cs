﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Validation.MessageResults
{
	[Obsolete]
	public class StrictResult : ResultBase
	{
		public override bool IsValid
		{
			get { 
				return (Warnings.Count == 0 && Errors.Count == 0);
			}
		}
	}
}
