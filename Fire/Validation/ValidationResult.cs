﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Fire.Validation
{
	[DebuggerDisplay( "IsValid = {IsValid}" )]
	public class ValidationResult : IEnumerable<ValidationMessage>, IDisposable
	{
		private List<ValidationMessage> messages;

		public ValidationResult() { 
			messages = new List<ValidationMessage>();
		}

		public void AddError(string message)
		{
			messages.Add(new ValidationMessage(message));
		}

		public void AddError(string location, string message)
		{
			messages.Add(new ValidationMessage(location, message));
		}

		public void AddError(ValidationMessage message)
		{
			messages.Add(message);
		}

		public void Combine(ValidationException validation)
		{
			messages.AddRange(validation.Result.messages);
		}

		public void Combine(ValidationResult validation)
		{
			messages.AddRange(validation.messages);
		}

		public bool IsValid
		{
			get { return messages.Count == 0; }
		}

		public void Validate()
		{
			if (!IsValid) {
				throw new ValidationException( this );
			}
		}

		public IEnumerator<ValidationMessage> GetEnumerator()
		{
			foreach (var message in messages)
			{
				yield return message;
			}
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void Dispose()
		{
			Validate();
		}
	}
}
