﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fire.Contract;

namespace Fire.Validation
{
	public class ValidationException : Exception
	{
		public ValidationException(ValidationResult result)
		{
			result.Require( "result" );

			Result = result;
		}

		public ValidationResult Result { get; protected set; }
	}
}
