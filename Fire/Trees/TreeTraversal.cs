﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Fire.Trees
{
	public class TreeTraversal
	{
		struct ParentChild<TParent, TChild>
		{
			public TParent Parent;
			public TChild Child;

			public ParentChild( TParent parent, TChild child )
			{
				Parent = parent;
				Child = child;
			}
		}

		/// <summary>
		/// Transforms a list of lists into a node tree
		/// </summary>
		public TDest TransformTree<TSource, TDest>( TSource inputRoot, Func<TSource, TDest> transformNode )
			where TSource : class, IList
			where TDest : class, INode
		{
			var nodes = new Queue<ParentChild<TDest, TSource>>();
			TDest root = null;

			nodes.Enqueue( new ParentChild<TDest, TSource>( null, inputRoot ) );

			while (nodes.Count > 0) {
				var node = nodes.Dequeue();
				var newParent = transformNode( node.Child );

				if (node.Parent != null) {
					node.Parent.AddChild( newParent );
				} else {
					root = newParent;
				}

				foreach (TSource child in node.Child) {
					nodes.Enqueue( new ParentChild<TDest, TSource>( newParent, child ) );
				}
			}

			return root;
		}

	}
}
