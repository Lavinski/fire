﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Trees
{
	public interface INode
	{
		bool HasChildren { get; }
		void AddChild( INode node );
		IEnumerable<INode> NodeChildren { get; }
	}
}
