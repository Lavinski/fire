﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Fire.Trees
{
	[DebuggerDisplay( "{Content}" )]
	public class Node<T> : INode
	{
		public Node()
		{
			Children = new List<Node<T>>();	
		}

		public T Content { get; set; }
		public bool HasChildren { get { return (Children.Count > 0); } }
		public IList<Node<T>> Children { get; set; }

		public IEnumerable<INode> NodeChildren {
			get { return Children.Cast<INode>(); } 
		}

		public void AddChild( INode node )
		{
			Children.Add((Node<T>)node);
		}
	}
}
