﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire.Collections
{
	public static class Combinators
	{

		public static void Merge<T>( this IEnumerable<T> master, IEnumerable<T> outofdate, Action<T> Insert, Action<T> Delete )
		{
			var deletions = new List<T>();
			var additions = new List<T>();

			// Delete
			foreach (var item in outofdate) {
				if (!master.Contains( item )) {
					deletions.Add( item );
				}
			}

			// Add
			foreach (var item in master) {
				if (!outofdate.Contains( item )) {
					additions.Add( item );
				}
			}


			// Modifiers are run to avoid editing the list while walking through them
			foreach (var item in deletions) {
				Delete( item );
			}

			foreach (var item in additions) {
				Insert( item );
			}
		}

	}
}
