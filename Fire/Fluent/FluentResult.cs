﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Fire
{
	/// <summary> 
	/// A fluent object is best created From the fluent extension methods,
	/// It wraps an input and a result for use in the fluent safety extension methods.
	/// </summary>
	public class Fluent<T, R>
	{
		/// <summary>
		/// Creates a new fluent object
		/// </summary>
		/// <param name="core">The initial input</param>
		public Fluent( T core )
		{
			Core = core;
			Result = default(R);
			Complete = true;
			CompleteCase = false;
		}

		/// <summary>
		/// Internally creates a new fluent result
		/// </summary>
		/// <param name="core">Previous input</param>
		/// <param name="result">New result</param>
		internal Fluent( T core, R result )
		{
			Core = core;
			Result = result;
			Complete = true;
			CompleteCase = false;
		}

		/// <summary>Determines if the fluent is still able to complete it's processing</summary>
		protected bool Complete { get; set; }
		/// <summary>Determines if the fluent has found a matching case in a switch</summary>
		protected bool CompleteCase { get; set; }

		/// <summary>The input object</summary>
		protected T Core { get; set; }
		/// <summary>The resulting object</summary>
		protected R Result { get; set; }



		/// <summary>Checks a fluent object for a null reference</summary>
		public Fluent<R, Out> NotNull<Out>( Func<R, Out> func )
		{
			if (this.Complete) {
				return new Fluent<R, Out>( this.Result, func( this.Result ) ) {
					Complete = true
				};
			}

			// If the tests failed return the default
			return new Fluent<R, Out>( this.Result, default( Out ) );
		}

		/// <summary>Checks a fluent object for a null reference or empty string or list</summary>
		public Fluent<string, string> NotEmpty( Func<string, string> func )
		{
			// Make sure the fluent reference is valid
			if (this.Complete) {

				// Do the conversion checks
				var result = this.Result as string;
				var list = this.Result as IList;

				// If it's not a string or is not an empty string or empty list
				if (result == null || result.Length > 0 || (list != null && list.Count > 0)) {

					// Returns the resulting fluent object
					return new Fluent<string, string>( this.Result as string, func( result ) ) {
						Complete = true
					};
				}
			}

			// If the tests failed return the default
			return new Fluent<string, string>( this.Result as string, (string)null );
		}

		/// <summary>Starts a switch section</summary>
		/// <returns>Switch Fluent with no real core</returns>
		public Fluent<R, Out> Switch<Out>()
		{
			// Return a shifted fluent with the complete case option reset
			return new Fluent<R, Out>( this.Result ) {
				Complete = this.Complete,
				CompleteCase = false
			};
		}

		/// <summary>Fluent case checks that the core type matches the In type and if so executes the value function</summary>
		public Fluent<T, R> Case( T equal, R val )
		{

			if (this.CompleteCase) {
				return this;

			} else {

				// Do the test (type and condition)
				if (this.Core.Equals(equal)) {

					// Returns the resulting fluent object
					return new Fluent<T, R>( this.Core, val ) {
						Complete = true,
						CompleteCase = true
					};

				} else {
					// If the tests failed return the default
					return new Fluent<T, R>( this.Core, default( R ) ) {
						Complete = false,
						CompleteCase = this.CompleteCase
					};
				}

			}

		}

		/// <summary>Fluent case checks that the core type matches the In type and if so executes the value function</summary>
		public Fluent<T, R> Case( Func<T, bool> func, Func<T, R> val )
		{

			if (this.CompleteCase) {
				return this;

			} else {

				// Do the test (type and condition)
				if (func( this.Core )) {

					// Returns the resulting fluent object
					return new Fluent<T, R>( this.Core, val( this.Core ) ) {
						Complete = true,
						CompleteCase = true
					};

				} else {
					// If the tests failed return the default
					return new Fluent<T, R>( this.Core, default( R ) ) {
						Complete = false,
						CompleteCase = this.CompleteCase
					};
				}

			}

		}

		/// <summary>Fluent case checks that the core type matches the In type and if so executes the value function</summary>
		public Fluent<T, R> Case<In>( Func<In, bool> when, Func<In, R> val )
			where In : class
		{

			if (this.CompleteCase) {
				return this;

			} else {

				// Do the test (type)
				if (this.Core is In && when( this.Core as In )) {

					// Returns the resulting fluent object
					return new Fluent<T, R>( this.Core, val( this.Core as In ) ) {
						Complete = true,
						CompleteCase = true
					};

				} else {
					// If the tests failed return the default
					return new Fluent<T, R>( this.Core, default( R ) ) {
						Complete = false,
						CompleteCase = this.CompleteCase
					};
				}

			}

		}

		/// <summary>Fluent case checks that the core type matches the In type and if so executes the value function</summary>
		public Fluent<T, R> Case<In>( Func<In, R> val )
			where In : class
		{

			if (this.CompleteCase) {
				return this;

			} else {

				// Do the test (type and condition)
				if (this.Core is In) {

					// Returns the resulting fluent object
					return new Fluent<T, R>( this.Core, val( this.Core as In ) ) {
						Complete = true,
						CompleteCase = true
					};

				} else {
					// If the tests failed return the default
					return new Fluent<T, R>( this.Core, default( R ) ) {
						Complete = false,
						CompleteCase = this.CompleteCase
					};
				}

			}

		}

		/// <summary>Default returns either the result of a fluent expression or it's own value if the fluent failed</summary>
		public R Default( R defaultValue )
		{
			return (this.Complete ? (R)this.Result : defaultValue);
		}

		/// <summary>Default returns either the result of a fluent expression or it's own value if the fluent failed</summary>
		public R Default( Func<R> defaultFunc )
		{
			return (this.Complete ? (R)this.Result : defaultFunc());
		}

		/// <summary>Default returns either the result of a fluent expression or it's own value if the fluent failed</summary>
		public R Default()
		{
			return (this.Complete ? (R)this.Result : default(R));
		}
	}

}
