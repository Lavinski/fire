﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire
{
	public interface IClock
	{
		DateTime Now { get; set; }
		DateTime Today { get; }
	}
}
