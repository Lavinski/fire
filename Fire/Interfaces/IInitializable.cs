﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fire
{
	public interface IInitializable
	{
		void Initialize();
	}
}
