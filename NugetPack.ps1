$ScriptPath = split-path -parent $MyInvocation.MyCommand.Definition

$SolutionDirectory = "$ScriptPath"

$msBuildDirectory = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319"

$PackagesFolder = "$SolutionDirectory\NuGet"

$ProjectPath = "Fire\Fire.csproj"

$NuspecPath = "Fire\Fire.nuspec"

$NuGetPath = "$SolutionDirectory\Tools\NuGet"

#Setup Path
$env:path += ";$NuGetPath;"

function Ensure-Directory($Path) {
	if (!(Test-Path $Path)) {
		New-Item $Path -Type Directory
	}
}

Write-Host "Building the project"
. "$msBuildDirectory\msbuild" $ProjectPath /p:Configuration=Release

Ensure-Directory $PackagesFolder

Write-Host "Package the project"
nuget pack $ProjectPath -Prop Configuration=Release -OutputDirectory $PackagesFolder #-Verbosity detailed








#$key = Read-Host "API Key"
#nuget setApiKey $key

#$package = Read-Host "Package file name"
#nuget push "$package.nupkg"